/**
 * Author: Roman Panov
 * Date:   2/28/13
 */

package org.roman.application.texteditor;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayDeque;
import java.util.EventObject;
import java.util.Queue;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.roman.ui.text.editor.CaretEvent;
import org.roman.ui.text.editor.CaretListener;
import org.roman.ui.text.editor.Document;
import org.roman.ui.text.editor.EditorComponent;
import org.roman.ui.text.editor.EditorFactory;
import org.roman.ui.text.editor.MultiLineDocument;
import org.roman.ui.text.editor.TextHighlighter;
import org.roman.ui.text.editor.TextListener;
import org.roman.ui.text.editor.TextPosition;
import org.roman.util.TreeList;
import org.roman.util.text.TextUtil;
import org.roman.util.text.lexer.LexerException;
import org.roman.util.text.lexer.ResettableLexer;
import org.roman.util.text.lexer.ResettableLexerFactory;
import org.roman.util.text.lexer.java.JavaLexer;

final class EditorMainFrame extends JFrame {
    private static final String MAIN_TITLE = "Guano Editor 0.0.2";
    private static final String FILE_LOADING_TITLE = "Loading file";
    private static final String FILE_SAVING_TITLE = "Saving file";
    private static final String DOCUMENT_CLOSING_TITLE = "Closing document";
    private static final String UNTITLED_DOCUMENT_TITLE = "Untitled Document";
    private static final String INSERT_MODE_LABEL = "INS";
    private static final String OVERWRITE_MODE_LABEL = "OVR";
    private static final String DEFAULT_LINE_SEPARATOR = "\n";

    private static final int OPTION_SAVE         = 0;
    private static final int OPTION_DISCARD      = 1;
    private static final int OPTION_SAVE_ALL     = 2;
    private static final int OPTION_DISCARD_ALL  = 3;
    private static final int OPTION_DO_NOT_CLOSE = 4;

    private int defaultCloseOperation = HIDE_ON_CLOSE;
    private final ResettableLexerFactory<Reader> lexerFactory = new LexerFactory();
    private final JavaColorMapper javaColorMapper = new JavaColorMapper();
    private final JFileChooser fileChooser = new JFileChooser();
    private final TabbedPane tabbedPane = new TabbedPane();
    private final JLabel statusLabel = new JLabel(TextUtil.EMPTY_STRING);
    private final EditorComponentListener editorComponentListener = new EditorComponentListener();
    private final TextPosition caretPosition = new TextPosition();
    private final Font editorFont;
    private final Menu menu;
    private final StringBuilder statusTextBuilder = new StringBuilder();
    private final String charsetName = Charset.defaultCharset().name();
    private final Queue<Integer> poolOdUntitledIds = new ArrayDeque<Integer>();
    private int untitledDocumentCount = 0;

    @Override
    public int getDefaultCloseOperation() {
        return defaultCloseOperation;
    }

    @Override
    public void setDefaultCloseOperation(int operation) {
        if (operation != DO_NOTHING_ON_CLOSE && operation != HIDE_ON_CLOSE &&
            operation != DISPOSE_ON_CLOSE && operation != EXIT_ON_CLOSE) {
            throw new IllegalArgumentException("Illegal value for default close operation: " +
                                                   operation + ".");
        }

        defaultCloseOperation = operation;
    }

    EditorMainFrame(Font editorFont) {
        super(MAIN_TITLE);
        this.editorFont = editorFont;
        enableEvents(AWTEvent.KEY_EVENT_MASK);
        final Container mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(tabbedPane, BorderLayout.CENTER);
        setContentPane(mainPanel);
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Java source files",
                                                                       "java"));

        super.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                processWindowClosing();
            }
        });

        menu = new Menu();
        setJMenuBar(menu.menuBar);

        // Create status bar.
        final JPanel statusBar = new JPanel();
        statusLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        statusBar.add(statusLabel);
        mainPanel.add(statusLabel, BorderLayout.SOUTH);

        createNewDocument();
    }

    private void openDocument() {
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            openFile(fileChooser.getSelectedFile());
        }
    }

    private void createNewDocument() {
        final MultiLineDocument document = new MultiLineDocument();
        openInTab(document, createEditorComponent(document), null);
    }

    private void saveActiveDocument() {
        if (tabbedPane.selectedDocInfoEntry != null && tabbedPane.getTabCount() > 0) {
            DocumentInfo docInfo = tabbedPane.selectedDocInfoEntry.value();
            if (docInfo != null) {
                if (saveToFile(docInfo)) {
                    tabbedPane.updateTitles();
                    // If this file is opened in another tab, close it.
                    tabbedPane.closeDocumentWithFile(docInfo.file);
                }
            }
        }
    }

    private void saveActiveDocumentAs() {
        if (tabbedPane.selectedDocInfoEntry != null && tabbedPane.getTabCount() > 0) {
            final DocumentInfo docInfo = tabbedPane.selectedDocInfoEntry.value();
            if (docInfo != null) {
                final File formerFile = docInfo.file;
                docInfo.file = null;

                if (saveToFile(docInfo)) {
                    tabbedPane.updateTitles();
                    // If this file is opened in another tab, close it.
                    tabbedPane.closeDocumentWithFile(docInfo.file);
                } else {
                    // Failed to save this document to another file. Fallback to the original file.
                    docInfo.file = formerFile;
                }
            }
        }
    }

    private void closeActiveDocument() {
        if (tabbedPane.selectedDocInfoEntry != null && tabbedPane.getTabCount() > 0) {
            final DocumentInfo docInfo = tabbedPane.selectedDocInfoEntry.value();
            if (docInfo != null) {
                if (docInfo.isUnsaved) {
                    final int choice = showSaveOptionDialog(docInfo.shortName, false);
                    switch (choice) {
                        case OPTION_SAVE:
                            if (!saveToFile(docInfo)) {
                                return;
                            }
                            break;
                        case OPTION_DISCARD:
                            break;
                        default:
                            return;
                    }
                }

                closeDocument(tabbedPane.selectedDocInfoEntry);
            }
        }
    }

    private boolean closeAllDocuments() {
        if (tabbedPane.getTabCount() > 0) {
            int numberOfUnsavedDocs = 0;
            for (TreeList.Entry<DocumentInfo> docInfoEntry = tabbedPane.docInfoList.first();
                 docInfoEntry != null; docInfoEntry = docInfoEntry.next()) {
                final DocumentInfo docInfo = docInfoEntry.value();
                if (docInfo != null && docInfo.isUnsaved) {
                    ++numberOfUnsavedDocs;
                }
            }

            boolean saveAll = false;
            boolean discardAll = false;
            TreeList.Entry<DocumentInfo> docInfoEntry = tabbedPane.docInfoList.first();
            while (docInfoEntry != null) {
                final TreeList.Entry<DocumentInfo> nextEntry = docInfoEntry.next();
                final DocumentInfo docInfo = docInfoEntry.value();

                if (docInfo != null) {
                    if (docInfo.isUnsaved) {
                        int choice;
                        if (saveAll) {
                            choice = OPTION_SAVE;
                        } else if (discardAll) {
                            choice = OPTION_DISCARD;
                        } else {
                            choice = showSaveOptionDialog(docInfo.shortName,
                                                          numberOfUnsavedDocs > 1);
                        }

                        switch (choice) {
                            case OPTION_SAVE_ALL:
                                saveAll = true;
                                choice = OPTION_SAVE;
                                break;
                            case OPTION_DISCARD_ALL:
                                discardAll = true;
                                choice = OPTION_DISCARD;
                                break;
                        }

                        switch (choice) {
                            case OPTION_SAVE:
                                if (!saveToFile(docInfo)) {
                                    return false;
                                }
                                break;
                            case OPTION_DISCARD:
                                --numberOfUnsavedDocs;
                                break;
                            default:
                                return false;
                        }
                    }

                    closeDocument(docInfoEntry);
                }

                docInfoEntry = nextEntry;
            }
        }

        return true;
    }

    private void openInTab(MultiLineDocument document, EditorComponent editorComponent, File file) {
        final DocumentInfo newDocInfo = new DocumentInfo(document, editorComponent, file);

        final TreeList.Entry<DocumentInfo> selectedDocInfoEntry = tabbedPane.selectedDocInfoEntry;
        if (file != null && selectedDocInfoEntry != null) {
            final DocumentInfo selectedDocInfo = selectedDocInfoEntry.value();
            if (!selectedDocInfo.isUnsaved && selectedDocInfo.isUntitled()) {
                // The currently selected tab contains a virgin document. Let's use this tab
                // instead of creating a new one.
                tabbedPane.setTab(selectedDocInfoEntry, newDocInfo);
                return;
            }
        }

        final int newTabIndex = tabbedPane.getTabCount();
        tabbedPane.addTab(newDocInfo);

        if (tabbedPane.getTabCount() == 1) {
            processFirstDocumentOpened();
        }

        tabbedPane.setSelectedIndex(newTabIndex);
    }

    private void openFile(File file) {
        final String path = filePath(file);

        if (!file.exists()) {
            final StringBuilder message = new StringBuilder("File ");
            message.append(path).append(" does not exist.");
            JOptionPane.showMessageDialog(this, message, FILE_LOADING_TITLE,
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Don't open a file that is already opened in the editor. Activate its tab instead.
        if (tabbedPane.activateTabForFile(file)) {
            return;
        }

        if (!file.canRead()) {
            final StringBuilder message = new StringBuilder("File ");
            message.append(path).append(" is not readable.");
            JOptionPane.showMessageDialog(this, message, FILE_LOADING_TITLE,
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!file.canWrite()) {
            final StringBuilder message = new StringBuilder("File ");
            message.append(path).append(" is read-only.");
            message.append("\nYou won't be able to save your changes to this file.");
            JOptionPane.showMessageDialog(this, message, FILE_LOADING_TITLE,
                                          JOptionPane.WARNING_MESSAGE);
        }

        final MultiLineDocument document;
        Reader fileReader = null;
        try {
            fileReader = new InputStreamReader(new BufferedInputStream(new FileInputStream(file)));
            document = new MultiLineDocument(fileReader);
        } catch (IOException exception) {
            exception.printStackTrace();
            final StringBuilder message = new StringBuilder("Failed to open file ");
            message.append(path).append('.');
            JOptionPane.showMessageDialog(this, message, FILE_LOADING_TITLE,
                                          JOptionPane.ERROR_MESSAGE);
            return;
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        }

        openInTab(document, createEditorComponent(document), file);
    }

    private boolean saveToFile(DocumentInfo docInfo) {
        if (docInfo.isUnsaved || docInfo.file == null) {
            File file = docInfo.file;
            if (file == null) {
                if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                    file = fileChooser.getSelectedFile();
                } else {
                    return false;
                }
            }

            while (file.exists() && !file.canWrite()) {
                final String path = filePath(file);
                final StringBuilder message = new StringBuilder("File ");
                message.append(path).append(" is not writable.");
                final Object[] options = new Object[] {
                    "Choose another file", "Add write permission", "Do not save"
                };
                final int CHOOSE_ANOTHER_FILE = 0;
                final int ADD_WRITE_PERMISSION = 1;

                final int choice = JOptionPane.showOptionDialog(this, message,
                                                                FILE_SAVING_TITLE,
                                                                JOptionPane.DEFAULT_OPTION,
                                                                JOptionPane.WARNING_MESSAGE, null,
                                                                options,
                                                                options[CHOOSE_ANOTHER_FILE]);
                switch (choice) {
                    case CHOOSE_ANOTHER_FILE:
                        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                            file = fileChooser.getSelectedFile();
                        } else {
                            return false;
                        }
                        break;

                    case ADD_WRITE_PERMISSION:
                        if (!file.setWritable(true)) {
                            message.setLength(0);
                            message.append("Failed to add write permission for file ").append(path);
                            message.append('.');
                            JOptionPane.showMessageDialog(this, message, FILE_SAVING_TITLE,
                                                          JOptionPane.WARNING_MESSAGE);
                        }
                        break;

                    default:
                        return false;
                }
            }

            String lineSeparator = System.getProperty("line.separator");
            if (lineSeparator == null) {
                lineSeparator = DEFAULT_LINE_SEPARATOR;
            }

            final String path = filePath(file);
            Writer fileWriter = null;
            try {
                fileWriter =
                    new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(file)));
                docInfo.document.store(fileWriter, lineSeparator);
            } catch (IOException exception) {
                exception.printStackTrace();
                final StringBuilder message = new StringBuilder("Failed to write file ");
                message.append(path).append('.');
                JOptionPane.showMessageDialog(this, message, FILE_SAVING_TITLE,
                                              JOptionPane.ERROR_MESSAGE);
                return false;
            } finally {
                if (fileWriter != null) {
                    try {
                        fileWriter.close();
                    } catch (IOException exception) {
                        exception.printStackTrace();
                    }
                }
            }

            docInfo.setFile(file);
            return true;
        }

        return true;
    }

    private void closeDocument(TreeList.Entry<DocumentInfo> docInfoEntry) {
        final DocumentInfo docInfo = docInfoEntry.value();
        if (docInfo != null) {
            docInfo.dispose();
            tabbedPane.removeEntry(docInfoEntry);

            if (tabbedPane.getTabCount() == 0) {
                processLastDocumentClosed();
            }
        }
    }

    private TextHighlighter<Color> createTextTinter(Document document) {
        final TextHighlighter<Color> textTinter;

        try {
            textTinter = EditorFactory.createTextHighlighter(document,
                                                             lexerFactory,
                                                             javaColorMapper);
        } catch (LexerException exception) {
            // Must never happen.
            throw new RuntimeException("Sad... This must not have happened.", exception);
        }

        return textTinter;
    }

    private int showSaveOptionDialog(String docName, boolean hasMultipleDocs) {
        final StringBuilder message = new StringBuilder(docName);
        message.append(" contains unsaved changes.\n");
        message.append("Save them before closing?");

        final Object[] options = new Object[hasMultipleDocs ? 5 : 3];
        int save, discard, saveAll, discardAll, doNotClose;
        options[0] = "Save";
        save = 0;
        options[1] = "Discard";
        discard = 1;
        if (hasMultipleDocs) {
            options[2] = "Save all";
            saveAll = 2;
            options[3] = "Discard all";
            discardAll = 3;
            options[4] = "Do not close";
            doNotClose = 4;
        } else {
            discardAll = saveAll = Integer.MIN_VALUE;
            options[2] =  "Do not close";
            doNotClose = 2;
        }

        final int choice = JOptionPane.showOptionDialog(this, message,
                                                        DOCUMENT_CLOSING_TITLE,
                                                        JOptionPane.DEFAULT_OPTION,
                                                        JOptionPane.QUESTION_MESSAGE,
                                                        null, options, options[save]);
        if (choice == save) {
            return OPTION_SAVE;
        }
        if (choice == discard) {
            return OPTION_DISCARD;
        }
        if (choice == saveAll) {
            return OPTION_SAVE_ALL;
        }
        if (choice == discardAll) {
            return OPTION_DISCARD_ALL;
        }
        if (choice == doNotClose) {
            return OPTION_DO_NOT_CLOSE;
        }

        return choice;
    }

    private EditorComponent createEditorComponent(Document document) {
        final EditorComponent editorComponent = new EditorComponent(document, editorFont,
                                                                    createTextTinter(document));
        editorComponent.addChangeListener(editorComponentListener);
        editorComponent.addCaretListener(editorComponentListener);
        return editorComponent;
    }

    private void updateStatusText(int caretLine, int caretColumn, boolean isOverwriteModeOn) {
        statusTextBuilder.setLength(0);
        statusTextBuilder.append(caretLine + 1).append(':').append(caretColumn + 1).append(" | ");
        statusTextBuilder.append(isOverwriteModeOn ? OVERWRITE_MODE_LABEL : INSERT_MODE_LABEL);
        statusTextBuilder.append(" | ").append(charsetName).append(" ");
        statusLabel.setText(statusTextBuilder.toString());
    }

    private void processWindowClosing() {
        switch (defaultCloseOperation) {
            case HIDE_ON_CLOSE:
                setVisible(false);
                break;
            case DISPOSE_ON_CLOSE:
                if (closeAllDocuments()) {
                    dispose();
                }
                break;
            case EXIT_ON_CLOSE:
                if (closeAllDocuments()) {
                    System.exit(0);
                }
                break;
        }
    }

    private void processFirstDocumentOpened() {
        menu.menuItemSave.setEnabled(true);
        menu.menuItemSaveAs.setEnabled(true);
        menu.menuItemClose.setEnabled(true);
    }

    private void processLastDocumentClosed() {
        setTitle(MAIN_TITLE);
        menu.menuItemSave.setEnabled(false);
        menu.menuItemSaveAs.setEnabled(false);
        menu.menuItemClose.setEnabled(false);
        statusLabel.setText(TextUtil.EMPTY_STRING);
    }

    private void processEditorComponentStateChanged(EventObject event) {
        if (tabbedPane.selectedDocInfoEntry != null) {
            final DocumentInfo selectedDocInfo = tabbedPane.selectedDocInfoEntry.value();
            if (selectedDocInfo != null) {
                final EditorComponent activeEditorComponent = selectedDocInfo.editorComponent;
                if (activeEditorComponent == event.getSource()) {
                    activeEditorComponent.computeCaretPosition(caretPosition);
                    updateStatusText(caretPosition.line, caretPosition.column,
                                     activeEditorComponent.getOverwriteMode());
                }
            }
        }
    }

    private void processCaretMoved(CaretEvent event) {
        if (tabbedPane.selectedDocInfoEntry != null) {
            final DocumentInfo selectedDocInfo = tabbedPane.selectedDocInfoEntry.value();
            if (selectedDocInfo != null) {
                final EditorComponent activeEditorComponent = selectedDocInfo.editorComponent;
                if (activeEditorComponent == event.getSource()) {
                    updateStatusText(event.getCaretLine(), event.getCaretColumn(),
                                     activeEditorComponent.getOverwriteMode());
                }
            }
        }
    }

    private static String filePath(File file) {
        String path;
        try {
            path = file.getCanonicalPath();
        } catch (IOException exception) {
            exception.printStackTrace();
            path = file.getAbsolutePath();
        }
        return path;
    }

    private int takeUntitledId() {
        final Integer id = poolOdUntitledIds.poll();
        ++untitledDocumentCount;
        return id == null ? untitledDocumentCount : id;
    }

    private void returnUntitledId(int id) {
        --untitledDocumentCount;
        poolOdUntitledIds.offer(id);
    }

    private final class DocumentInfo {
        private final MultiLineDocument document;
        private final EditorComponent editorComponent;
        private File file;
        private String shortName;
        private String longName;
        private boolean isUnsaved = false;
        private int untitledId = -1;

        private DocumentInfo(MultiLineDocument document,
                             EditorComponent editorComponent,
                             File file) {
            this.document = document;
            this.editorComponent = editorComponent;
            setFile(file);
        }

        private String shortTitle() {
            if (isUnsaved) {
                final StringBuilder titleBld = new StringBuilder("*");
                titleBld.append(shortName);
                return titleBld.toString();
            }
            return shortName;
        }

        private String longTitle() {
            final StringBuilder titleBld = new StringBuilder();
            if (isUnsaved) {
                titleBld.append('*');
            }
            titleBld.append(longName).append(" - ").append(MAIN_TITLE);
            return titleBld.toString();
        }

        private void setFile(File file) {
            this.file = file;

            if (file == null) {
                untitledId = takeUntitledId();
                final StringBuilder titleBld = new StringBuilder(UNTITLED_DOCUMENT_TITLE);
                titleBld.append(" ").append(untitledId);
                longName = shortName = titleBld.toString();
            } else {
                if (isUntitled()) {
                    returnUntitledId(untitledId);
                    untitledId = -1;
                }
                shortName = file.getName();
                longName = filePath(file);
                isUnsaved = false;
            }
        }

        private void dispose() {
            if (isUntitled()) {
                returnUntitledId(untitledId);
            }
        }

        private boolean isUntitled() {
            return untitledId > 0;
        }
    }

    private final class Menu {
        private final JMenuBar menuBar;
        private final JMenuItem menuItemSave;
        private final JMenuItem menuItemSaveAs;
        private final JMenuItem menuItemClose;

        private Menu() {
            menuBar = new JMenuBar();
            final JMenu fileMenu = new JMenu("File");
            fileMenu.setMnemonic(KeyEvent.VK_F);
            menuBar.add(fileMenu);

            final JMenuItem menuItemNew = new JMenuItem("New", KeyEvent.VK_N);
            menuItemNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                                                              InputEvent.CTRL_DOWN_MASK));
            menuItemNew.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    createNewDocument();
                }
            });

            final JMenuItem menuItemOpen = new JMenuItem("Open...", KeyEvent.VK_O);
            menuItemOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
                                                               InputEvent.CTRL_DOWN_MASK));
            menuItemOpen.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    openDocument();
                }
            });

            menuItemSave = new JMenuItem("Save", KeyEvent.VK_S);
            menuItemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                                               InputEvent.CTRL_DOWN_MASK));
            menuItemSave.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveActiveDocument();
                }
            });

            menuItemSaveAs = new JMenuItem("Save As...", KeyEvent.VK_A);
            menuItemSaveAs.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                       InputEvent.SHIFT_DOWN_MASK |InputEvent.CTRL_DOWN_MASK));
            menuItemSaveAs.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveActiveDocumentAs();
                }
            });

            menuItemClose = new JMenuItem("Close", KeyEvent.VK_C);
            menuItemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4,
                                                                InputEvent.CTRL_DOWN_MASK));
            menuItemClose.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    closeActiveDocument();
                }
            });

            final JMenuItem menuItemQuit = new JMenuItem("Quit", KeyEvent.VK_Q);
            menuItemQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
                                                               InputEvent.CTRL_DOWN_MASK));
            menuItemQuit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (closeAllDocuments()) {
                        System.exit(0);
                    }
                }
            });

            fileMenu.add(menuItemNew);
            fileMenu.add(menuItemOpen);
            fileMenu.addSeparator();
            fileMenu.add(menuItemSave);
            fileMenu.add(menuItemSaveAs);
            fileMenu.addSeparator();
            fileMenu.add(menuItemClose);
            fileMenu.add(menuItemQuit);
        }
    }

    private final class TabbedPane extends JTabbedPane {
        private final TreeList<DocumentInfo> docInfoList = new TreeList<DocumentInfo>();
        private TreeList.Entry<DocumentInfo> selectedDocInfoEntry = null;

        @Override
        protected void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);

            if (selectedDocInfoEntry != null) {
                selectedDocInfoEntry.value().editorComponent.requestFocusInWindow();
            }
        }

        private TabbedPane() {
            getModel().addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent event) {
                    processTabSelected(tabbedPane.getSelectedIndex());
                }
            });
        }

        private void addTab(final DocumentInfo docInfo) {
            final TreeList.Entry<DocumentInfo> docInfoEntry = docInfoList.append(docInfo);
            tabbedPane.addTab(docInfo.shortTitle(), new JScrollPane(docInfo.editorComponent));
            attachListener(docInfoEntry);
        }

        private void setTab(TreeList.Entry<DocumentInfo> docInfoEntry, DocumentInfo newDocInfo) {
            final DocumentInfo oldDocInfo = docInfoEntry.value();
            if (oldDocInfo != null) {
                oldDocInfo.dispose();
            }

            docInfoEntry.setValue(newDocInfo);
            final int tabIndex = docInfoEntry.index();
            setComponentAt(tabIndex, new JScrollPane(newDocInfo.editorComponent));
            attachListener(docInfoEntry);
            updateTitles(docInfoEntry);
            newDocInfo.editorComponent.computeCaretPosition(caretPosition);
            updateStatusText(caretPosition.line, caretPosition.column,
                             newDocInfo.editorComponent.getOverwriteMode());
        }

        private void updateTitles() {
            if (selectedDocInfoEntry != null) {
                final DocumentInfo selectedDocInfo = selectedDocInfoEntry.value();
                if (selectedDocInfo != null) {
                    setTitleAt(selectedDocInfoEntry.index(), selectedDocInfo.shortTitle());
                    EditorMainFrame.this.setTitle(selectedDocInfo.longTitle());
                }
            }
        }

        private void updateTitles(TreeList.Entry<DocumentInfo> docInfoEntry) {
            final DocumentInfo docInfo = docInfoEntry.value();
            if (docInfo != null) {
                setTitleAt(docInfoEntry.index(), docInfo.shortTitle());
                if (docInfoEntry == selectedDocInfoEntry) {
                    EditorMainFrame.this.setTitle(docInfo.longTitle());
                }
            }
        }

        private boolean activateTabForFile(File file) {
            if (file == null) {
                return false;
            }

            TreeList.Entry<DocumentInfo> docInfoEntry = docInfoList.first();
            for (int tabIdx = 0; docInfoEntry != null;
                 ++tabIdx, docInfoEntry = docInfoEntry.next()) {
                if (file.equals(docInfoEntry.value().file)) {
                    setSelectedIndex(tabIdx);
                    return true;
                }
            }

            return false;
        }

        private void closeDocumentWithFile(File file) {
            if (file != null) {
                for (TreeList.Entry<DocumentInfo> docInfoEntry = tabbedPane.docInfoList.first();
                     docInfoEntry != null; docInfoEntry = docInfoEntry.next()) {
                    if (docInfoEntry != tabbedPane.selectedDocInfoEntry) {
                        final DocumentInfo docInfo = docInfoEntry.value();
                        if (docInfo != null && file.equals(docInfo.file)) {
                            docInfo.dispose();
                            tabbedPane.removeEntry(docInfoEntry);
                        }
                    }
                }
            }
        }

        private void removeEntry(TreeList.Entry<DocumentInfo> docInfoEntry) {
            final int tabIndex = docInfoEntry.index();
            removeTabAt(tabIndex);
            docInfoList.remove(docInfoEntry);
            selectedDocInfoEntry = docInfoList.get(getSelectedIndex());
        }

        private void processTabSelected(int tabIndex) {
            selectedDocInfoEntry = docInfoList.get(tabIndex);
            if (selectedDocInfoEntry != null) {
                final DocumentInfo selectedDocInfo = selectedDocInfoEntry.value();
                setTitle(selectedDocInfo.longTitle());
                selectedDocInfo.editorComponent.computeCaretPosition(caretPosition);
                updateStatusText(caretPosition.line, caretPosition.column,
                                 selectedDocInfo.editorComponent.getOverwriteMode());
            }
        }

        private void attachListener(final TreeList.Entry<DocumentInfo> docInfoEntry) {
            final DocumentInfo docInfo = docInfoEntry.value();
            if (docInfo != null) {
                docInfo.document.addListener(new TextListener() {
                    @Override
                    public void textChanging(int offset, int length, int newLength) {
                    }

                    @Override
                    public void textChanged() {
                        if (!docInfo.isUnsaved) {
                            docInfo.isUnsaved = true;
                            updateTitles(docInfoEntry);
                        }
                    }
                });
            }
        }
    }

    private final class EditorComponentListener implements ChangeListener, CaretListener {
        @Override
        public void stateChanged(ChangeEvent event) {
            processEditorComponentStateChanged(event);
        }

        @Override
        public void caretMoved(CaretEvent event) {
            processCaretMoved(event);
        }
    }

    private static final class LexerFactory implements ResettableLexerFactory<Reader> {
        @Override
        public ResettableLexer<Reader> createLexer(Reader reader) {
            return JavaLexer.create(reader);
        }
    }
}
