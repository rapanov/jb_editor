/**
 * Author: Roman Panov
 * Date:   2/28/13
 */

package org.roman.application.texteditor;

import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

public final class EditorMainApp implements Runnable{
    private static final String DEFAULT_FONT_NAME = Font.MONOSPACED;
    private static final int DEFAULT_FONT_STYLE = Font.BOLD;
    private static final int DEFAULT_FONT_SIZE = 14;
    private static final int FRAME_X = 100;
    private static final int FRAME_Y = 100;
    private static final int FRAME_WIDTH = 800;
    private static final int FRAME_HEIGHT = 600;

    private final Font defaultFont;

    public static void main(String[] args) {
        final String osName = System.getProperty("os.name");
        if (osName != null && osName.toLowerCase().startsWith("windows")) {
            // Use native look&feel on Windows.
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        new EditorMainApp().launch();
    }

    @Override
    public void run() {
        final JFrame editorFrame = new EditorMainFrame(defaultFont);
        editorFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        editorFrame.setLocation(FRAME_X, FRAME_Y);
        editorFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        editorFrame.setVisible(true);
    }

    private EditorMainApp() {
        defaultFont = new Font(DEFAULT_FONT_NAME, DEFAULT_FONT_STYLE, DEFAULT_FONT_SIZE);
        System.out.format("Using font \"%s\"\n", fontToString(defaultFont));
    }

    private void launch() {
        SwingUtilities.invokeLater(this);
    }

    private static Object fontToString(Font font) {
        if (font == null) {
            return "null";
        }

        final StringBuilder strBld = new StringBuilder(font.getName());
        strBld.append(" ").append(font.getSize());

        if (font.isBold()) {
            strBld.append(" bold");
        }

        if (font.isItalic()) {
            strBld.append(" italic");
        }

        return strBld;
    }
}
