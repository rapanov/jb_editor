/**
 * Author: Roman Panov
 * Date:   3/2/13
 */

package org.roman.application.texteditor;

import java.awt.Color;
import org.roman.util.IntegerMapperAdapter;
import org.roman.util.text.lexer.java.JavaLexer;

public final class JavaColorMapper extends IntegerMapperAdapter<Color> {
    private static final Color COLOR_KEYWORD = new Color(0x0, 0x0, 0xae);
    private static final Color COLOR_STRING = new Color(0, 0x80, 0);
    private static final Color COLOR_NUMERIC = new Color(0x0, 0x0, 0xff);
    private static final Color COLOR_COMMENT = new Color(0x80, 0x80, 0x80);

    @Override
    public Color map(int tokenType) {
        if (JavaLexer.isKeyword(tokenType)) {
            return COLOR_KEYWORD;
        }

        if (JavaLexer.isCharLiteral(tokenType) || JavaLexer.isStringLiteral(tokenType)) {
            return COLOR_STRING;
        }

        if (tokenType == JavaLexer.TOKEN_TYPE_BOOLEAN_LITERAL) {
            return COLOR_KEYWORD;
        }

        if (JavaLexer.isNumericLiteral(tokenType)) {
            return COLOR_NUMERIC;
        }

        if (JavaLexer.isComment(tokenType)) {
            return COLOR_COMMENT;
        }

        return null;
    }
}
