/**
 * Author: Roman Panov
 * Date:   11/16/12
 */

package org.roman.ui.text.editor;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.CharacterIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.HashSet;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.roman.util.OffsetList;
import org.roman.util.Segment;
import org.roman.util.SegmentIterator;
import org.roman.util.text.Characters;
import org.roman.util.text.IterableCharSequence;
import org.roman.util.text.LineIterator;
import org.roman.util.text.LineReader;

public final class MultiLineDocument implements Document {

    private static final char NEW_LINE = Characters.LINE_FEED;

    private int textLength;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final OffsetList<LineChunk> chunkList = new OffsetList<LineChunk>();
    private OffsetList.EntryEx<LineChunk> chunkEntryEx = new OffsetList.EntryEx<LineChunk>();
    private LineIterator lineIterator = new LineIterator();
    private final Set<TextListener> listeners = new HashSet<TextListener>();
    private final Segment lineSegment = new Segment();

    public MultiLineDocument() {
        chunkList.put(0, new LineChunk(0));
        textLength = 0;
    }

    public MultiLineDocument(CharIterator text) {
        this(text, Integer.MAX_VALUE);
    }

    public MultiLineDocument(CharacterIterator text, int maxChars) {
        lineIterator.reset(text, maxChars);
        int offset = 0;

        while (lineIterator.next(lineSegment)) {
            final int lineLength = lineSegment.length;
            final LineChunk chunk = new LineChunk(lineLength);
            copyChars(text, lineSegment.offset, chunk.text, 0, lineLength);
            final int chunkSize = lineLength + 1;
            chunkList.put(offset, chunk, chunkSize);
            offset += chunkSize;
        }

        if (offset > 0) {
            // Subtract one, as the last line doesn't have the new line character at its end.
            textLength = offset - 1;
        } else {
            chunkList.put(0, new LineChunk(0));
            textLength = 0;
        }
    }

    public MultiLineDocument(Reader reader) throws IOException {
        final LineReader lineReader = new LineReader(reader);
        StringBuilder textLine = new StringBuilder();
        int lineLength;
        int offset = 0;

        while ((lineLength = lineReader.readLine(textLine)) >= 0) {
            final LineChunk chunk = new LineChunk(lineLength);
            textLine.getChars(0, lineLength, chunk.text, 0);
            final int chunkSize = lineLength + 1;
            chunkList.put(offset, chunk, chunkSize);
            offset += chunkSize;
            textLine.setLength(0);
        }

        if (offset > 0) {
            // Subtract one, as the last line doesn't have the new line character at its end.
            textLength = offset - 1;
        } else {
            chunkList.put(0, new LineChunk(0));
            textLength = 0;
        }
    }

    @Override
    public char charAt(int index) {
        chunkList.lowerBound(index, chunkEntryEx);
        final OffsetList.Entry<LineChunk> chunkEntry = chunkEntryEx.entry;

        if (chunkEntry != null) {
            final LineChunk chunk = chunkEntryEx.entry.value();
            final int offsetInChunk = index - chunkEntryEx.offset;

            if (offsetInChunk < chunk.lineLength) {
                return chunk.text[offsetInChunk];
            }

            if (offsetInChunk == chunk.lineLength && chunkEntry.next() != null) {
                return NEW_LINE;
            }
        }

        throw new StringIndexOutOfBoundsException(index);
    }

    @Override
    public int length() {
        return textLength;
    }

    @Override
    public IterableCharSequence subSequence(int start, int end) {
        if (start < 0) {
            throw new StringIndexOutOfBoundsException(start);
        }

        if (end > textLength) {
            throw new StringIndexOutOfBoundsException(end);
        }

        final int subSequenceLength = end - start;
        if (subSequenceLength < 0) {
            throw new StringIndexOutOfBoundsException(subSequenceLength);
        }

        if (start == 0 && end == textLength) {
            return this;
        }

        final CharacterIterator charIterator = iterator(null);
        charIterator.setIndex(start);
        return new MultiLineDocument(charIterator, subSequenceLength);
    }

    @Override
    public String toString() {
        final StringBuilder strBld = new StringBuilder(textLength);
        OffsetList.Entry<LineChunk> chunkEntry = chunkList.first();

        if (chunkEntry != null) {
            LineChunk chunk = chunkEntry.value();
            strBld.append(chunk.text, 0, chunk.lineLength);

            for (chunkEntry = chunkEntry.next(); chunkEntry != null;
                 chunkEntry = chunkEntry.next()) {
                chunk = chunkEntry.value();
                strBld.append(NEW_LINE);
                strBld.append(chunk.text, 0, chunk.lineLength);
            }
        }

        return strBld.toString();
    }

    @Override
    public CharacterIterator iterator(CharacterIterator target) {
        return target instanceof CharIterator ? ((CharIterator)target).reset() : new CharIterator();
    }

    @Override
    public int lineCount() {
        return chunkList.size();
    }

    @Override
    public int lineIndex(int offset) {
        int lineIdx = -1;
        chunkList.lowerBound(offset, chunkEntryEx);

        if (chunkEntryEx.entry != null) {
            if (chunkEntryEx.offset + chunkEntryEx.entry.value().lineLength >= offset) {
                lineIdx = chunkEntryEx.index;
            }
        }

        return lineIdx;
    }

    @Override
    public int lineOffset(int lineIndex) {
        validateLineIndex(lineIndex);
        chunkList.get(lineIndex, chunkEntryEx);
        return chunkEntryEx.offset;
    }

    @Override
    public int lineLength(int lineIndex) {
        validateLineIndex(lineIndex);
        chunkList.get(lineIndex, chunkEntryEx);
        return chunkEntryEx.entry.value().lineLength;
    }

    @Override
    public void line(int lineIndex, Segment target) {
        validateLineIndex(lineIndex);
        chunkList.get(lineIndex, chunkEntryEx);
        target.offset = chunkEntryEx.offset;
        target.length = chunkEntryEx.entry.value().lineLength;
    }

    @Override
    public int lineByOffset(int offset, Segment target) {
        chunkList.lowerBound(offset, chunkEntryEx);

        if (chunkEntryEx.entry == null) {
            return -1;
        }

        target.offset = chunkEntryEx.offset;
        target.length = chunkEntryEx.entry.value().lineLength;
        return chunkEntryEx.index;
    }

    @Override
    public SegmentIterator lineIterator(int lineIndex, SegmentIterator target) {
        validateLineIndex(lineIndex);
        chunkList.get(lineIndex, chunkEntryEx);
        return target instanceof ChunkIterator ? ((ChunkIterator)target).reset(chunkEntryEx)
                                               : new ChunkIterator(chunkEntryEx);
    }

    @Override
    public void readLock() {
        readWriteLock.readLock().lock();
    }

    @Override
    public void readUnlock() {
        readWriteLock.readLock().unlock();
    }

    @Override
    public void writeLock() {
        readWriteLock.writeLock().lock();
    }

    @Override
    public void writeUnlock() {
        readWriteLock.writeLock().unlock();
    }

    @Override
    public void insertChar(int offset, char ch) {
        chunkList.lowerBound(offset, chunkEntryEx);
        final OffsetList.Entry<LineChunk> chunkEntry = chunkEntryEx.entry;

        if (chunkEntry == null) {
            throw new StringIndexOutOfBoundsException(offset);
        }

        final LineChunk chunk = chunkEntry.value();
        final int offsetInChunk = offset - chunkEntryEx.offset;

        if (offsetInChunk <= chunk.lineLength) {
            fireDocumentChanging(offset, 0, 1);
            final char[] srcText = chunk.text;

            if (isNewLine(ch)) {
                final int newCapacity = chunk.newCapacity(offsetInChunk);
                if (newCapacity != chunk.text.length) {
                    chunk.text = new char[newCapacity];
                    System.arraycopy(srcText, 0, chunk.text, 0, offsetInChunk);
                }

                final LineChunk newChunk = new LineChunk(chunk.lineLength - offsetInChunk);
                System.arraycopy(srcText, offsetInChunk,
                                 newChunk.text, 0,
                                 newChunk.lineLength);
                chunk.lineLength = offsetInChunk;
                chunkEntry.shiftAllAfterThis(1);
                chunkList.put(offset + 1, newChunk);
            } else {
                final int newCapacity = chunk.newCapacity(chunk.lineLength + 1);
                if (newCapacity != chunk.text.length) {
                    chunk.text = new char[newCapacity];
                    System.arraycopy(srcText, 0, chunk.text, 0, offsetInChunk);
                }

                System.arraycopy(srcText, offsetInChunk,
                                 chunk.text, offsetInChunk + 1,
                                 chunk.lineLength - offsetInChunk);
                chunk.text[offsetInChunk] = ch;
                ++chunk.lineLength;
                chunkEntry.shiftAllAfterThis(1);
            }

            ++textLength;
            fireDocumentChanged();
        } else {
            throw new StringIndexOutOfBoundsException(offset);
        }
    }

    @Override
    public void deleteChar(int offset) {
        chunkList.lowerBound(offset, chunkEntryEx);
        final OffsetList.Entry<LineChunk> chunkEntry = chunkEntryEx.entry;

        if (chunkEntry == null) {
            throw new StringIndexOutOfBoundsException(offset);
        }

        final LineChunk chunk = chunkEntry.value();
        final int offsetInChunk = offset - chunkEntryEx.offset;

        if (offsetInChunk < chunk.lineLength) {
            fireDocumentChanging(offset, 1, 0);
            final char[] srcText = chunk.text;
            final int newCapacity = chunk.newCapacity(chunk.lineLength - 1);

            if (newCapacity != chunk.text.length) {
                chunk.text = new char[newCapacity];
                System.arraycopy(srcText, 0, chunk.text, 0, offsetInChunk);
            }

            System.arraycopy(srcText, offsetInChunk + 1,
                             chunk.text, offsetInChunk,
                             chunk.lineLength - offsetInChunk - 1);
            --chunk.lineLength;
            chunkEntry.shiftAllAfterThis(-1);
            --textLength;
            fireDocumentChanged();
        } else if (offsetInChunk == chunk.lineLength) {
            final OffsetList.Entry<LineChunk> nextChunkEntry = chunkEntry.next();

            if (nextChunkEntry == null) {
                throw new StringIndexOutOfBoundsException(offset);
            } else {
                // The lines are to be merged.
                fireDocumentChanging(offset, 1, 0);
                final LineChunk nextChunk = nextChunkEntry.value();
                final int mergedLineLength = chunk.lineLength + nextChunk.lineLength;
                final int newCapacity = chunk.newCapacity(mergedLineLength);

                if (newCapacity != chunk.text.length) {
                    final char[] srcText = chunk.text;
                    chunk.text = new char[newCapacity];
                    System.arraycopy(srcText, 0, chunk.text, 0, chunk.lineLength);
                }

                System.arraycopy(nextChunk.text, 0,
                                 chunk.text, chunk.lineLength,
                                 nextChunk.lineLength);
                chunk.lineLength = mergedLineLength;
                chunkList.remove(nextChunkEntry, true);
                chunkEntry.shiftAllAfterThis(nextChunk.lineLength);
                --textLength;
                fireDocumentChanged();
            }
        } else {
            throw new StringIndexOutOfBoundsException(offset);
        }
    }

    @Override
    public void changeText(int offset, int length, CharacterIterator newText) {
        chunkList.lowerBound(offset, chunkEntryEx);
        OffsetList.Entry<LineChunk> startChunkEntry = chunkEntryEx.entry;

        if (startChunkEntry == null) {
            throw new StringIndexOutOfBoundsException(offset);
        }

        final int startChunkOffset = chunkEntryEx.offset;
        final LineChunk startChunk = startChunkEntry.value();
        final int offsetInStartChunk = offset - startChunkOffset;

        if (offsetInStartChunk > startChunk.lineLength) {
            throw new StringIndexOutOfBoundsException(offset);
        }

        OffsetList.Entry<LineChunk> endChunkEntry;
        LineChunk endChunk;
        final int endOffset = offset + length;
        int offsetInEndChunk = endOffset - startChunkOffset;

        if (offsetInEndChunk <= startChunk.lineLength)  {
            endChunkEntry = startChunkEntry;
            endChunk = startChunk;
        } else {
            chunkList.lowerBound(endOffset, chunkEntryEx);
            endChunkEntry = chunkEntryEx.entry;
            endChunk = endChunkEntry.value();
            offsetInEndChunk = endOffset - chunkEntryEx.offset;

            if (offsetInEndChunk > endChunk.lineLength) {
                throw new StringIndexOutOfBoundsException(endOffset);
            }
        }

        final int newLength = newText.getEndIndex() - newText.getBeginIndex();
        lineIterator.reset(newText);
        fireDocumentChanging(offset, length, newLength);
        char[] srcText = startChunk.text;

        if (startChunk == endChunk) {
            if (lineIterator.next(lineSegment)) {
                final int shift = newLength - length;
                int insertedLineOffset = lineSegment.offset;
                int insertedLineLength = lineSegment.length;

                if (lineIterator.next(lineSegment)) {
                    final int firstInsertedLineOffset = insertedLineOffset;
                    final int firstInsertedLineLength = insertedLineLength;
                    int insertedChunkOffset = startChunkOffset + startChunk.lineLength + 1;

                    for (;;) {
                        insertedLineOffset = lineSegment.offset;
                        insertedLineLength = lineSegment.length;

                        if (!lineIterator.hasNext()) {
                            break;
                        }

                        final int insertedChunkSize = insertedLineLength + 1;
                        final LineChunk insertedChunk = new LineChunk(insertedLineLength);
                        chunkList.put(insertedChunkOffset, insertedChunk, insertedChunkSize);
                        insertedChunkOffset += insertedChunkSize;
                        copyChars(newText, insertedLineOffset, insertedChunk.text, 0,
                                  insertedLineLength);
                        lineIterator.next(lineSegment);
                    }

                    final int reminderLength = endChunk.lineLength - offsetInEndChunk;
                    final LineChunk lastInsertedChunk =
                        new LineChunk(insertedLineLength + reminderLength);
                    copyChars(newText, insertedLineOffset, lastInsertedChunk.text, 0,
                              insertedLineLength);
                    System.arraycopy(endChunk.text, offsetInEndChunk,
                                     lastInsertedChunk.text, insertedLineLength,
                                     reminderLength);
                    chunkList.put(insertedChunkOffset,
                                  lastInsertedChunk,
                                  lastInsertedChunk.lineLength + 1);

                    final int newLineLength = offsetInStartChunk + firstInsertedLineLength;
                    final int newCapacity = startChunk.newCapacity(newLineLength);

                    if (newCapacity != startChunk.text.length) {
                        startChunk.text = new char[newCapacity];
                        System.arraycopy(srcText, 0, startChunk.text, 0, offsetInStartChunk);
                    }

                    copyChars(newText, firstInsertedLineOffset, startChunk.text, offsetInStartChunk,
                              firstInsertedLineLength);
                    startChunkEntry.shiftAllAfterThis(newLineLength - startChunk.lineLength);
                    startChunk.lineLength = newLineLength;
                } else {
                    final int newCapacity = startChunk.newCapacity(startChunk.lineLength + shift);
                    if (newCapacity != startChunk.text.length) {
                        startChunk.text = new char[newCapacity];
                        System.arraycopy(srcText, 0, startChunk.text, 0, offsetInStartChunk);
                    }

                    copyChars(newText, insertedLineOffset, startChunk.text, offsetInStartChunk,
                              insertedLineLength);
                    System.arraycopy(srcText, offsetInEndChunk,
                                     startChunk.text, offsetInEndChunk + shift,
                                     startChunk.lineLength - offsetInEndChunk);
                    startChunk.lineLength += shift;
                    startChunkEntry.shiftAllAfterThis(shift);
                }

                textLength += shift;
            }
        } else {
            if (lineIterator.next(lineSegment)) {
                int insertedLineOffset = lineSegment.offset;
                int insertedLineLength = lineSegment.length;

                if (lineIterator.next(lineSegment)) {
                    final int firstInsertedLineOffset = insertedLineOffset;
                    final int firstInsertedLineLength = insertedLineLength;
                    int newLineLength = offsetInStartChunk + firstInsertedLineLength;
                    int shift = newLineLength - startChunk.lineLength;
                    int newCapacity = startChunk.newCapacity(newLineLength);

                    if (newCapacity != startChunk.text.length) {
                        startChunk.text = new char[newCapacity];
                        System.arraycopy(srcText, 0, startChunk.text, 0, offsetInStartChunk);
                    }

                    copyChars(newText, firstInsertedLineOffset, startChunk.text, offsetInStartChunk,
                              firstInsertedLineLength);
                    startChunk.lineLength = newLineLength;

                    // All chunks between 'startChunk' and 'endChunk' are to be removed.
                    OffsetList.Entry<LineChunk> chunkEntry = startChunkEntry.next();
                    while (chunkEntry != endChunkEntry) {
                        final OffsetList.Entry<LineChunk> nextChunkEntry = chunkEntry.next();
                        chunkList.remove(chunkEntry, true);
                        chunkEntry = nextChunkEntry;
                    }

                    startChunkEntry.shiftAllAfterThis(shift);
                    int insertedChunkOffset = startChunkOffset + startChunk.lineLength + 1;

                    for (;;) {
                        insertedLineOffset = lineSegment.offset;
                        insertedLineLength = lineSegment.length;

                        if (!lineIterator.hasNext()) {
                            break;
                        }

                        final int insertedChunkSize = insertedLineLength + 1;
                        final LineChunk insertedChunk = new LineChunk(insertedLineLength);
                        chunkList.put(insertedChunkOffset, insertedChunk, insertedChunkSize);
                        insertedChunkOffset += insertedChunkSize;
                        copyChars(newText, insertedLineOffset, insertedChunk.text, 0,
                                  insertedLineLength);
                        lineIterator.next(lineSegment);
                    }

                    final int reminderLength = endChunk.lineLength - offsetInEndChunk;
                    newLineLength = insertedLineLength + reminderLength;
                    shift = newLineLength - endChunk.lineLength;
                    newCapacity = endChunk.newCapacity(newLineLength);
                    srcText = endChunk.text;

                    if (newCapacity != endChunk.text.length) {
                        endChunk.text = new char[newCapacity];
                    }

                    if (shift < 0) {
                        for (int srcIdx = offsetInEndChunk, dstIdx = insertedLineLength;
                             srcIdx < endChunk.lineLength; ++srcIdx,  ++dstIdx) {
                            endChunk.text[dstIdx] = srcText[srcIdx];
                        }
                    } else if (shift > 0) {
                        for (int srcIdx = endChunk.lineLength - 1, dstIdx = srcIdx + shift;
                             srcIdx >= offsetInEndChunk; --srcIdx,  --dstIdx) {
                            endChunk.text[dstIdx] = srcText[srcIdx];
                        }
                    }

                    copyChars(newText, insertedLineOffset, endChunk.text, 0, insertedLineLength);
                    endChunk.lineLength = newLineLength;
                    endChunkEntry.shiftAllAfterThis(shift);
                    textLength += newLength - length;
                } else {
                    // All chunks between 'startChunk' and 'endChunk' are to be removed.
                    OffsetList.Entry<LineChunk> chunkEntry = startChunkEntry.next();
                    while (chunkEntry != endChunkEntry) {
                        final OffsetList.Entry<LineChunk> nextChunkEntry = chunkEntry.next();
                        chunkList.remove(chunkEntry, false);
                        chunkEntry = nextChunkEntry;
                    }

                    final int endChunkReminderLength = endChunk.lineLength - offsetInEndChunk;
                    final int newLineLength = offsetInStartChunk + insertedLineLength +
                                              endChunkReminderLength;
                    final int newCapacity = startChunk.newCapacity(newLineLength);

                    if (newCapacity != startChunk.text.length) {
                        startChunk.text = new char[newCapacity];
                        System.arraycopy(srcText, 0, startChunk.text, 0, offsetInStartChunk);
                    }

                    final int chunkIdx = copyChars(newText, insertedLineOffset,
                                                   startChunk.text, offsetInStartChunk,
                                                   insertedLineLength);
                    System.arraycopy(endChunk.text, offsetInEndChunk,
                                     startChunk.text, chunkIdx,
                                     endChunkReminderLength);
                    chunkList.remove(endChunkEntry, false);
                    startChunk.lineLength = newLineLength;
                    final int shift = newLength - length;
                    startChunkEntry.shiftAllAfterThis(shift);
                    textLength += shift;
                }
            }
        }

        fireDocumentChanged();
    }

    @Override
    public boolean addListener(TextListener listener) {
        return listeners.add(listener);
    }

    @Override
    public boolean removeListener(TextListener listener) {
        return listeners.remove(listener);
    }

    public void store(Writer storageWriter, String lineSeparator) throws IOException {
        OffsetList.Entry<LineChunk> chunkEntry = chunkList.first();
        if (chunkEntry != null) {
            LineChunk chunk = chunkEntry.value();
            storageWriter.write(chunk.text, 0, chunk.lineLength);
            for (chunkEntry = chunkEntry.next(); chunkEntry != null;
                 chunkEntry = chunkEntry.next()) {
                storageWriter.write(lineSeparator);
                chunk = chunkEntry.value();
                storageWriter.write(chunk.text, 0, chunk.lineLength);
            }
        }
    }

    private void fireDocumentChanging(int offset, int length, int newLength) {
        for (final TextListener listener: listeners) {
            listener.textChanging(offset, length, newLength);
        }
    }

    private void fireDocumentChanged() {
        for (final TextListener listener: listeners) {
            listener.textChanged();
        }
    }

    private static int copyChars(CharacterIterator src, int srcPos,
                                 char[] dst, int dstPos, int length) {
        final int formerIndex = src.getIndex(); // Retain the index, as we'll have to restore it.
        char ch = src.setIndex(srcPos);
        for (final int dstEndPos = dstPos + length; dstPos < dstEndPos; ++dstPos, ch = src.next()) {
            dst[dstPos] = ch;
        }
        src.setIndex(formerIndex);
        return dstPos;
    }

    private void validateLineIndex(int lineIndex) {
        if (lineIndex < 0 || lineIndex >= chunkList.size()) {
            throw new IndexOutOfBoundsException("Line index is out of bounds: " + lineIndex + ".");
        }
    }

    private static boolean isNewLine(char ch) {
        return (ch == Characters.LINE_FEED || ch == Characters.CARRIAGE_RETURN);
    }

    private static final class LineChunk {
        private int lineLength;
        char[] text;

        @Override
        public String toString() {
            final StringBuilder strBld = new StringBuilder(lineLength + 7);
            strBld.append('\"').append(text,  0, lineLength).append("\"(");
            strBld.append(lineLength).append(")");
            return strBld.toString();
        }

        private LineChunk(int lineLength) {
            this.lineLength = lineLength;
            text = new char[lineLength];
        }

        private int newCapacity(int newLineLength) {
            int capacity = text.length;

            if (newLineLength > capacity) {
                // Increase the capacity.
                if (capacity <= 0x10) {
                    capacity += 4;
                } else {
                    capacity *= 5;
                    capacity >>>= 2;
                }

                if (capacity < newLineLength) {
                    capacity = newLineLength;
                }
            } else if (capacity > 4) {
                for (int halfCapacity = capacity >>> 1;
                     capacity > 0 && newLineLength <= halfCapacity;
                     halfCapacity >>>= 1) {
                    capacity = halfCapacity;
                }
            }

            return capacity;
        }
    }

    private final class CharIterator implements CharacterIterator  {
        private final OffsetList.EntryEx<LineChunk> chunkEntryEx;
        private int charIndex;

        @Override
        public Object clone() {
            return new CharIterator(chunkEntryEx, charIndex);
        }

        @Override
        public char current() {
            if (charIndex >= 0 && charIndex < textLength) {
                final LineChunk chunk = chunkEntryEx.entry.value();
                final int offsetInChunk = charIndex - chunkEntryEx.offset;
                return (offsetInChunk < chunk.lineLength ? chunk.text[offsetInChunk] : NEW_LINE);
            }

            return DONE;
        }

        @Override
        public char first() {
            chunkList.first(chunkEntryEx);
            charIndex = 0;
            return current();
        }

        @Override
        public int getBeginIndex() {
            return 0;
        }

        @Override
        public int getEndIndex() {
            return textLength;
        }

        @Override
        public int getIndex() {
            return charIndex;
        }

        @Override
        public char last() {
            if (textLength > 0) {
                charIndex = textLength - 1;
                chunkList.last(chunkEntryEx);
                LineChunk chunk = chunkEntryEx.entry.value();

                if (chunk.lineLength > 0) {
                    return chunk.text[chunk.lineLength - 1];
                }

                // We need to retain this, as previous() will provide us with the relative
                // (not absolute) offset.
                final int offsetBase = chunkEntryEx.offset;
                chunkEntryEx.entry.previous(chunkEntryEx);
                chunkEntryEx.offset += offsetBase;
                return NEW_LINE;
            } else {
                charIndex = 0;
            }

            return DONE;
        }

        @Override
        public char next() {
            if (++charIndex == 0) {
                return first();
            }

            if (charIndex < textLength) {
                OffsetList.Entry<LineChunk> chunkEntry = chunkEntryEx.entry;
                if (chunkEntry != null) {
                    LineChunk chunk = chunkEntry.value();
                    int offsetInChunk = charIndex - chunkEntryEx.offset;
                    if (offsetInChunk < 0) {
                        System.out.println("Fuck-up!!!");
                    }

                    if (offsetInChunk < chunk.lineLength) {
                        return chunk.text[offsetInChunk];
                    }

                    if (offsetInChunk == chunk.lineLength) {
                        return NEW_LINE;
                    }

                    // We need to retain this, as next() will provide us with the relative
                    // (not absolute) offset.
                    final int offsetBase = chunkEntryEx.offset;
                    chunkEntry.next(chunkEntryEx);
                    chunkEntry = chunkEntryEx.entry;
                    chunkEntryEx.offset += offsetBase;
                    chunk = chunkEntry.value();
                    return (chunk.lineLength > 0 ? chunk.text[0] : NEW_LINE);
                }
            } else {
                chunkEntryEx.entry = null;
            }

            return DONE;
        }

        @Override
        public char previous() {
            --charIndex;
            if (textLength > 0 && charIndex == textLength - 1) {
                return last();
            }

            if (charIndex >= 0) {
                OffsetList.Entry<LineChunk> chunkEntry = chunkEntryEx.entry;
                if (chunkEntry != null) {
                    LineChunk chunk = chunkEntry.value();
                    int offsetInChunk = charIndex - chunkEntryEx.offset;

                    if (chunk.lineLength > 0 && offsetInChunk >= 0) {
                        return chunk.text[offsetInChunk];
                    }

                    // We need to retain this, as previous() will provide us with the relative
                    // (not absolute) offset.
                    final int offsetBase = chunkEntryEx.offset;
                    chunkEntry.previous(chunkEntryEx);

                    if (chunkEntryEx.entry != null) {
                        chunkEntryEx.offset += offsetBase;
                        return NEW_LINE;
                    }
                }
            } else {
                chunkEntryEx.entry = null;
            }

            return DONE;
        }

        @Override
        public char setIndex(int index) {
            charIndex = index;

            if (index >= 0 && index < textLength) {
                chunkList.lowerBound(index, chunkEntryEx);
                final LineChunk chunk = chunkEntryEx.entry.value();
                final int offsetInChunk = index - chunkEntryEx.offset;
                return (offsetInChunk < chunk.lineLength ? chunk.text[offsetInChunk] : NEW_LINE);
            }

            chunkEntryEx.entry = null;
            return DONE;
        }

        private CharIterator() {
            chunkEntryEx = new OffsetList.EntryEx<LineChunk>();
            reset();
        }

        private CharIterator(OffsetList.EntryEx<LineChunk> chunkEntryEx, int charIndex) {
            this.chunkEntryEx = new OffsetList.EntryEx<LineChunk>();
            this.chunkEntryEx.assign(chunkEntryEx);
            this.charIndex = charIndex;
        }

        private CharIterator reset() {
            chunkEntryEx.entry = null;
            charIndex = -1;
            return this;
        }
    }

    private static final class ChunkIterator implements SegmentIterator {
        private final OffsetList.EntryEx<LineChunk> chunkEntryEx;
        boolean hasStarted = false;

        @Override
        public boolean hasNext() {
            return
                (chunkEntryEx.entry != null && (!hasStarted || chunkEntryEx.entry.next() != null));
        }

        @Override
        public Segment next() {
            if (chunkEntryEx.entry == null) {
                throw new NoSuchElementException();
            }

            if (hasStarted) {
                // We need to retain this, as next() will provide us with the relative
                // (not absolute) offset.
                final int offsetBase = chunkEntryEx.offset;
                chunkEntryEx.entry.next(chunkEntryEx);

                if (chunkEntryEx.entry == null) {
                    throw new NoSuchElementException();
                }

                chunkEntryEx.offset += offsetBase;
            } else {
                hasStarted = true;
            }

            return new Segment(chunkEntryEx.offset, chunkEntryEx.entry.value().lineLength);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean next(Segment target) {
            if (chunkEntryEx.entry == null) {
                return false;
            }

            if (hasStarted) {
                // We need to retain this, as next() will provide us with the relative
                // (not absolute) offset.
                final int offsetBase = chunkEntryEx.offset;
                chunkEntryEx.entry.next(chunkEntryEx);

                if (chunkEntryEx.entry == null) {
                    return false;
                }

                chunkEntryEx.offset += offsetBase;
            } else {
                hasStarted = true;
            }

            target.offset = chunkEntryEx.offset;
            target.length = chunkEntryEx.entry.value().lineLength;
            return true;
        }

        private ChunkIterator(OffsetList.EntryEx<LineChunk> chunkEntryEx) {
            this.chunkEntryEx = new OffsetList.EntryEx<LineChunk>();
            reset(chunkEntryEx);
        }

        private SegmentIterator reset(OffsetList.EntryEx<LineChunk> chunkEntryEx) {
            this.chunkEntryEx.assign(chunkEntryEx);
            hasStarted = false;
            return this;
        }
    }
}