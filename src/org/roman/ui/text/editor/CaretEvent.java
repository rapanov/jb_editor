/**
 * Author: Roman Panov
 * Date:   3/8/13
 */

package org.roman.ui.text.editor;

import java.util.EventObject;

public class CaretEvent extends EventObject {
    private final int caretLine;
    private final int caretColumn;

    public CaretEvent(Object source, int caretLine, int caretColumn) {
        super(source);
        this.caretLine = caretLine;
        this.caretColumn = caretColumn;
    }

    public int getCaretLine() {
        return caretLine;
    }

    public int getCaretColumn() {
        return caretColumn;
    }
}
