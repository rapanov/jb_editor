/**
 * Author: Roman Panov
 * Date:   3/8/13
 */

package org.roman.ui.text.editor;

public interface CaretListener {
    void caretMoved(CaretEvent event);
}
