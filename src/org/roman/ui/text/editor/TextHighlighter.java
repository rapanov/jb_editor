/**
 * Author: Roman Panov
 * Date:   2/17/13
 */

package org.roman.ui.text.editor;

import org.roman.util.DataSegmentIterator;

public interface TextHighlighter<P> extends TextListener {
    DataSegmentIterator<P> iterator(int offset, DataSegmentIterator<P> target);
}
