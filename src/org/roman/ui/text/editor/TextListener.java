/**
 * Author: Roman Panov
 * Date:   2/17/13
 */

package org.roman.ui.text.editor;

public interface TextListener {
    void textChanging(int offset, int length, int newLength);
    void textChanged();
}
