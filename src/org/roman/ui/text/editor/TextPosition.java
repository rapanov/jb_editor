/**
 * Author: Roman Panov
 * Date:   3/7/13
 */

package org.roman.ui.text.editor;

public final class TextPosition implements Comparable<TextPosition> {
    public int line;
    public int column;

    public TextPosition() {
        line = 0;
        column = 0;
    }

    public TextPosition(int line, int column) {
        this.line = line;
        this.column = column;
    }

    public TextPosition(TextPosition anotherPosition) {
        line = anotherPosition.line;
        column = anotherPosition.column;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TextPosition) {
            final TextPosition anotherPosition = (TextPosition)obj;
            return (line == anotherPosition.line && column == anotherPosition.column);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (line << 10) + column;
    }

    @Override
    public int compareTo(TextPosition anotherPosition) {
        if (line > anotherPosition.line) {
            return 1;
        }
        if (line < anotherPosition.line) {
            return -1;
        }
        if (column > anotherPosition.column) {
            return 1;
        }
        if (column < anotherPosition.column) {
            return -1;
        }
        return 0;
    }

    public TextPosition assign(int line, int column) {
        this.line = line;
        this.column = column;
        return this;
    }

    public TextPosition assign(TextPosition anotherPosition) {
        return assign(anotherPosition.line, anotherPosition.column);
    }

    public static void swap(TextPosition pos0, TextPosition pos1) {
        int tmp = pos0.line;
        pos0.line = pos1.line;
        pos1.line = tmp;
        tmp = pos0.column;
        pos0.column = pos1.column;
        pos1.column = tmp;
    }
}
