/**
 * Author: Roman Panov
 * Date:   12/13/12
 */

package org.roman.ui.text.editor;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.Reader;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import javax.swing.JComponent;
import org.roman.util.DataSegment;
import org.roman.util.DataSegmentIterator;
import org.roman.util.Segment;
import org.roman.util.SegmentIterator;
import org.roman.util.TreeList;
import org.roman.util.text.CharArrayIterator;
import org.roman.util.text.CharSequenceIterator;
import org.roman.util.text.Characters;
import org.roman.util.text.CompositeCharIterator;
import org.roman.util.text.EmptyCharIterator;
import org.roman.util.text.RepeatingCharIterator;
import org.roman.util.text.SingleCharIterator;
import org.roman.util.text.TextUtil;

final class EditorCore {

    private static final Color BACKGROUND_COLOR = Color.WHITE;
    private static final Color DEFAULT_TEXT_COLOR = Color.BLACK;
    private static final Color SELECTED_TEXT_COLOR = Color.WHITE;
    private static final Color SELECTION_COLOR = new Color(0x52, 0x6d, 0xa5);
    private static final Color CARET_COLOR = Color.BLACK;

    private static final int MOUSE_LEFT_BUTTON = MouseEvent.BUTTON1;
    private static final int MOUSE_RIGHT_BUTTON = MouseEvent.BUTTON3;
    private static final char NEW_LINE = Characters.LINE_FEED;

    private static final ScheduledExecutorService SCHEDULER =
        new ScheduledThreadPoolExecutor(1, new ThreadFactoryImpl());
    private static final Integer[] INTEGER_POOL = new Integer[0x4000];

    private final Document document;
    private CharacterIterator charIterator = null;
    private SegmentIterator lineIterator = null;
    private LineIterator defaultLineIterator = null;
    private final Segment lineSegment = new Segment();
    private final TextHighlighter<Color> textTinter;
    private DataSegmentIterator<Color> tokenIterator = null;
    private final DataSegment<Color> tokenSegment = new DataSegment<Color>();
    private final JComponent editorComponent;
    private final Executor uiCommandExecutor;
    private final Listener listener;
    private final TreeList<Integer> lineWidthList = new TreeList<Integer>();
    // This maps line width value to count of lines of this width.
    private final SortedMap<Integer, Integer> lineWidthToCountMap = new TreeMap<Integer, Integer>();
    private int textWidth;
    private int textHeight;
    private final Rectangle visibleRect = new Rectangle();
    private final Rectangle contentRect = new Rectangle();
    private TextMetrics textMetrics;
    private int tabSize;
    private final CharBuffer charBuffer = new CharBuffer(0x100);
    private final IntBuffer charXBuffer = new IntBuffer(0x100);
    private char[] textArray = new char[0x100];
    private boolean isOverwriteModeOn = false;
    private final Caret caret;
    private final TextPosition caretOldPosition = new TextPosition();
    private final TextPosition caretNewPosition = new TextPosition();
    private final Selection selection = new Selection();
    private final MouseState mouseState = new MouseState();
    private final DocumentChangeData documentChangeData = new DocumentChangeData();
    private final EmptyCharIterator emptyCharIterator = new EmptyCharIterator();
    private final SingleCharIterator singleCharIterator = new SingleCharIterator('\0');
    private final RepeatingCharIterator repeatingCharIterator = new RepeatingCharIterator('\0', 0);
    private final CompositeCharIterator compositeCharIterator =
        new CompositeCharIterator(emptyCharIterator, emptyCharIterator);
    private final CharArrayIterator charArrayIterator =
        new CharArrayIterator(TextUtil.EMPTY_CHAR_ARRAY);
    private final CharSequenceIterator charSequenceIterator =
        new CharSequenceIterator(TextUtil.EMPTY_CHAR_SEQUENCE);
    private final StringBuilder textBuilder = new StringBuilder(0x100);
    private final Clipboard clipboard;

    EditorCore(Document document,
               int tabSize,
               TextHighlighter<Color> textTinter,
               JComponent editorComponent,
               Executor uiCommandExecutor,
               Listener listener,
               Clipboard clipboard) {
        if (document == null || editorComponent == null) {
            throw new NullPointerException();
        }
        if (tabSize < 0) {
            throw new IllegalArgumentException("Illegal tab size value: " + tabSize + ".");
        }

        this.document = document;
        this.tabSize = tabSize;
        this.textTinter = textTinter;
        this.editorComponent = editorComponent;
        this.uiCommandExecutor = uiCommandExecutor;
        this.listener = listener;
        textMetrics = new TextMetrics(editorComponent.getFontMetrics(editorComponent.getFont()));
        this.clipboard = clipboard;

        document.readLock();
        try {
            caret = new Caret();
            computeTextWidth();
            computeTextHeight();
        } finally {
            document.readUnlock();
        }
    }

    Document document() {
        return document;
    }

    int preferredWidth() {
        return textWidth + Caret.WIDTH;
    }

    int preferredHeight() {
        return textHeight;
    }

    int columnWidth() {
        return textMetrics.spaceWidth;
    }

    int lineHeight() {
        return textMetrics.lineHeight;
    }

    Color backgroundColor() {
        return BACKGROUND_COLOR;
    }

    void setFontMetrics(FontMetrics fontMetrics) {
        textMetrics = new TextMetrics(fontMetrics);
        caret.updateWithNewTextMetrics();
        final int oldTextWidth = textWidth;
        final int oldTextHeight = textHeight;
        computeTextWidth();
        computeTextHeight();
        if (listener != null && (textWidth != oldTextWidth || textHeight != oldTextHeight)) {
            listener.preferredSizeChanged(preferredWidth(), preferredHeight());
        }
    }

    int tabSize() {
        return tabSize;
    }

    boolean isOverwriteMOdeOn() {
        return isOverwriteModeOn;
    }

    void caretPosition(TextPosition target) {
        target.assign(caret.position);
    }

    boolean setTabSize(int tabSize) {
        if (tabSize == this.tabSize) {
            return false;
        }

        this.tabSize = tabSize;
        caret.invalidateVisualColumnAndX();
        final int oldTextWidth = textWidth;
        computeTextHeight();
        if (listener != null && textWidth != oldTextWidth) {
            listener.preferredSizeChanged(preferredWidth(),  preferredHeight());
        }
        return true;
    }

    void paint(Graphics graphics) {
        final Rectangle clipRect = graphics.getClipBounds();

        if (clipRect.equals(caret.visualRect) && isCaretVisible()) {
            drawCaretOnly(graphics);
        } else {
            graphics.setColor(BACKGROUND_COLOR);
            graphics.fillRect(clipRect.x, clipRect.y, clipRect.width, clipRect.height);
            document.readLock();

            try {
                drawTextAndCaret(graphics, clipRect.x, clipRect.y, clipRect.width, clipRect.height);
            } finally {
                document.readUnlock();
            }
        }
    }

    void processFocusGained(FocusEvent event) {
        /*if (!caret.areVisualColumnAndXValid()) {
            document.readLock();
            try {
                caret.computeVisualColumnAndX();
            } finally {
                document.readUnlock();
            }
        }
        caret.isVisible = true;
        final Rectangle caretRect = caret.visualRect;
        editorComponent.repaint(caretRect.x, caretRect.y, caretRect.width,caretRect.height);*/
    }

    void processFocusLost(FocusEvent event) {
        if (!caret.areVisualColumnAndXValid()) {
            document.readLock();
            try {
                caret.computeVisualColumnAndX();
            } finally {
                document.readUnlock();
            }
        }
        final Rectangle caretRect = caret.visualRect;
        editorComponent.repaint(caretRect.x, caretRect.y, caretRect.width,caretRect.height);
    }

    void processKeyPressed(KeyEvent event) {
        final int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_UP:
                moveCaretUp(event);
                event.consume();
                break;
            case KeyEvent.VK_DOWN:
                moveCaretDown(event);
                event.consume();
                break;
            case KeyEvent.VK_LEFT:
                moveCaretLeft(event);
                event.consume();
                break;
            case KeyEvent.VK_RIGHT:
                moveCaretRight(event);
                event.consume();
                break;
            case KeyEvent.VK_PAGE_UP:
                moveCaretOnePageUp(event);
                event.consume();
                break;
            case KeyEvent.VK_PAGE_DOWN:
                moveCaretOnePageDown(event);
                event.consume();
                break;
            case KeyEvent.VK_HOME:
                moveCaretHome(event);
                event.consume();
                break;
            case KeyEvent.VK_END:
                moveCaretEnd(event);
                event.consume();
                break;
            case KeyEvent.VK_ESCAPE:
                // Abolish selection.
                selection.isValid = false;
                editorComponent.repaint();
                event.consume();
                break;
            case KeyEvent.VK_INSERT:
                invertOverwriteMode();
                event.consume();
                break;
            default:
                processControlKey(event);
        }
    }

    void processKeyTyped(KeyEvent event) {
        if (!event.isAltDown() &&
            !event.isAltGraphDown() &&
            !event.isControlDown()) {
            event.consume();
            final char keyChar = event.getKeyCode() == KeyEvent.VK_ENTER ? NEW_LINE
                                                                         : event.getKeyChar();
            switch (keyChar) {
                case KeyEvent.CHAR_UNDEFINED:
                case Characters.ESCAPE:
                    break;
                case Characters.BACKSPACE:
                    deletePreviousChar();
                    break;
                case Characters.DELETE :
                    deleteCurrentChar();
                    break;
                default:
                    insertChar(keyChar);
                    break;
            }
        }
    }

    void processMousePressed(MouseEvent event) {
        if (event.getButton() == MouseEvent.BUTTON1) {
            if (!mouseState.isLeftButtonDown) {
                mouseState.isLeftButtonDown = true;
                mouseState.hasLeftButtonStateChanged = true;
            }
        }
    }

    void processMouseReleased(MouseEvent event) {
        if (event.getButton() == MouseEvent.BUTTON1) {
            if (mouseState.isLeftButtonDown) {
                mouseState.isLeftButtonDown = false;
                mouseState.hasLeftButtonStateChanged = true;
            }
        }
    }

    void processMouseClicked(MouseEvent event) {
        final int mouseButton = event.getButton();
        switch (mouseButton) {
            case MOUSE_LEFT_BUTTON:
                if (event.getClickCount() > 1) {
                    selectWordUnderXY(event.getX(), event.getY());
                } else {
                    moveCaretWithMouse(event);
                }
                break;

            case MOUSE_RIGHT_BUTTON:
                // TODO: Implement better behavior.
                // Abolish selection.
                selection.isValid = false;
                editorComponent.repaint();
                break;
        }
    }

    void processMouseMoved(MouseEvent event) {
    }

    void processMouseDragged(MouseEvent event) {
        if (mouseState.isLeftButtonDown) {
            moveCaretWithMouse(event);
            scrollToCaret();
            mouseState.hasLeftButtonStateChanged = false;
        }
    }

    void processDocumentChanging(int offset, int length, int newLength) {
        if (documentChangeData.isChanging) {
            throw new IllegalStateException();
        }

        documentChangeData.isChanging = true;
        documentChangeData.offset = offset;
        documentChangeData.length = length;
        documentChangeData.newLength = newLength;
        document.readLock();

        try {
            if (document.lineCount() > 0) {
                final Segment lineSegment = documentLine(caret.position.line);
                documentChangeData.caretOffset =
                    lineSegment.offset + Math.min(caret.position.column, lineSegment.length);
            } else {
                documentChangeData.caretOffset = 0;
            }

            documentChangeData.firstChangedLine = document.lineByOffset(offset, lineSegment);
            final int changingRegionEnd = offset + length;
            if (changingRegionEnd <= lineSegment.offset + lineSegment.length) {
                documentChangeData.lineCount = 1;
            } else {
                documentChangeData.lineCount =
                    document.lineIndex(changingRegionEnd) - documentChangeData.firstChangedLine + 1;
            }
        } finally {
            document.readUnlock();
        }
    }

    void processDocumentChanged() {
        if (!documentChangeData.isChanging) {
            throw new IllegalStateException();
        }

        documentChangeData.isChanging = false;
        selection.isValid = false;
        document.readLock();

        try {
            if (documentChangeData.caretOffset >= documentChangeData.offset) {
                int caretOffset = documentChangeData.caretOffset;
                final int changedRegionEnd = documentChangeData.offset + documentChangeData.length;

                if (documentChangeData.caretOffset >= changedRegionEnd) {
                    // The caret stayed after the changing area, so we might need to shift it.
                    final int caretShift = documentChangeData.newLength - documentChangeData.length;
                    caretOffset += caretShift;
                }

                caretOffset = Math.min(caretOffset, document.length());
                if (caretOffset < 0) {
                    caretOffset = 0;
                }

                int caretLine = 0;
                int caretColumn = 0;

                if (document.lineCount() > 0) {
                    caretLine = document.lineByOffset(caretOffset, lineSegment);
                    caretColumn = caretOffset - lineSegment.offset;
                }

                caret.moveTo(caretLine, caretColumn);
            }
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();

        final int oldTextWidth = textWidth;
        final int oldTextHeight = textHeight;
        document.readLock();
        try {
            updateTextWidthAndHeight();
        } finally {
            document.readUnlock();
        }
        if (listener != null && (textWidth != oldTextWidth || textHeight != oldTextHeight)) {
            listener.preferredSizeChanged(preferredWidth(), preferredHeight());
        }
    }

    private void processControlKey(KeyEvent event) {
        if (event.isControlDown()) {
            final int keyCode = event.getKeyCode();
            switch (keyCode) {
                case KeyEvent.VK_A:
                    selectAll();
                    event.consume();
                    break;
                case KeyEvent.VK_C:
                    copyToClipboard();
                    event.consume();
                    break;
                case KeyEvent.VK_V:
                    pasteFromClipboard();
                    event.consume();
                    break;
                case KeyEvent.VK_X:
                    cutToClipboard();
                    event.consume();
                    break;
            }
        }
    }

    private void moveCaretUp(InputEvent event) {
        caretOldPosition.assign(caret.position);
        document.readLock();

        try {
            caret.stepUp();
            caretNewPosition.assign(caret.position);
            updateSelectionAfterCaretMove(event);
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();
        scrollToCaret();
    }

    private void moveCaretDown(InputEvent event) {
        caretOldPosition.assign(caret.position);
        document.readLock();

        try {
            caret.stepDown();
            caretNewPosition.assign(caret.position);
            updateSelectionAfterCaretMove(event);
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();
        scrollToCaret();
    }

    private void moveCaretLeft(InputEvent event) {
        boolean hasMoved = false;
        caretOldPosition.assign(caret.position);
        document.readLock();

        try {
            hasMoved = event.isControlDown() ? caret.moveOneWordLeft() : caret.stepLeft();
            caretNewPosition.assign(caret.position);
            updateSelectionAfterCaretMove(event);
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();
        if (hasMoved) {
            scrollToCaret();
        }
    }

    private void moveCaretRight(InputEvent event) {
        caretOldPosition.assign(caret.position);
        document.readLock();

        try {
            if (event.isControlDown()) {
                caret.moveOneWordRight();
            } else {
                caret.stepRight();
            }
            caretNewPosition.assign(caret.position);
            updateSelectionAfterCaretMove(event);
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();
        scrollToCaret();
    }

    private void moveCaretOnePageUp(InputEvent event) {
        editorComponent.computeVisibleRect(visibleRect);
        contentRect.setBounds(visibleRect.x, caret.visualRect.y - visibleRect.height,
                              visibleRect.width, visibleRect.height);
        editorComponent.scrollRectToVisible(contentRect);
        moveCaretVertically(event,
                            caret.position.line - visibleRect.height / textMetrics.lineHeight);
    }

    private void moveCaretOnePageDown(InputEvent event) {
        editorComponent.computeVisibleRect(visibleRect);
        contentRect.setBounds(visibleRect.x, caret.visualRect.y + visibleRect.height,
                              visibleRect.width, visibleRect.height);
        editorComponent.scrollRectToVisible(contentRect);
        moveCaretVertically(event,
                            caret.position.line + visibleRect.height / textMetrics.lineHeight);
    }

    private void moveCaretVertically(InputEvent event, int caretNewLine) {
        caretOldPosition.assign(caret.position);
        document.readLock();

        try {
            caret.moveVertically(caretNewLine);
            caretNewPosition.assign(caret.position);
            updateSelectionAfterCaretMove(event);
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();
        scrollToCaret();
    }

    private void moveCaretHome(InputEvent event) {
        caretOldPosition.assign(caret.position);
        document.readLock();

        try {
            caret.moveTo(event.isControlDown() ? 0 : caret.position.line, 0);
            caretNewPosition.assign(caret.position);
            updateSelectionAfterCaretMove(event);
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();
        scrollToCaret();
    }

    private void moveCaretEnd(InputEvent event) {
        caretOldPosition.assign(caret.position);
        document.readLock();

        try {
            final int lineCount = document.lineCount();
            if (caret.position.line < lineCount) {
                final int newCaretLine = event.isControlDown() ? lineCount - 1
                                                               : caret.position.line;
                caret.moveTo(newCaretLine, document.lineLength(newCaretLine));
                caretNewPosition.assign(caret.position);
                updateSelectionAfterCaretMove(event);
            }
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();
        scrollToCaret();
    }

    private void moveCaretWithMouse(MouseEvent event) {
        caretOldPosition.assign(caret.position);
        document.readLock();

        try {
            caret.moveToXY(event.getX(), event.getY());
            caretNewPosition.assign(caret.position);
            updateSelectionAfterCaretMove(event);
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();
    }

    private void updateSelectionAfterCaretMove(InputEvent event) {
        if (event.getID() == MouseEvent.MOUSE_DRAGGED) {
            if (mouseState.hasLeftButtonStateChanged) {
                // Mouse dragging has just started (at the caretNewPosition).
                // Here we abolish an existing selection and retain the caretNewPosition as the
                // new selection's sticky point.
                selection.isValid = false;
                selection.stickyPoint.assign(caretNewPosition);
                adjustSelectionPosition(selection.stickyPoint);
            } else {
                selection.start.assign(selection.stickyPoint);
                selection.end.assign(caretNewPosition);
                adjustSelectionPosition(selection.end);
                selection.isValid = !selection.start.equals(selection.end);

                if (selection.isValid && selection.start.compareTo(selection.end) > 0) {
                    TextPosition.swap(selection.start, selection.end);
                    if (selection.end.column < document.lineLength(selection.end.line)) {
                        ++selection.end.column;
                        //++selection.stickyPoint.column;
                    }
                }
            }
        } else if (event.isShiftDown()) {
            if (selection.isValid) {
                selection.start.assign(selection.stickyPoint);
                selection.end.assign(caretNewPosition);
                adjustSelectionPosition(selection.end);
            } else {
                selection.stickyPoint.assign(caretOldPosition);
                adjustSelectionPosition(selection.stickyPoint);
                selection.start.assign(selection.stickyPoint);
                selection.end.assign(caretNewPosition);
                adjustSelectionPosition(selection.end);
                selection.isValid = !selection.start.equals(selection.end);
            }

            if (selection.isValid && selection.start.compareTo(selection.end) > 0) {
                TextPosition.swap(selection.start, selection.end);
            }
        } else {
            // Abolish an existing selection.
            selection.isValid = false;
        }
    }

    private void adjustSelectionPosition(TextPosition pos) {
        if (pos.line < document.lineCount()) {
            final int lineLength = document.lineLength(pos.line);
            if (pos.column > lineLength) {
                pos.column = lineLength;
            }
        } else {
            pos.line = Math.max(document.lineCount() - 1, 0);
            pos.column = 0;
        }
    }

    private void selectWordUnderXY(int x, int y) {
        boolean hasSelected = false;
        document.readLock();

        try {
            final int lineCount = document.lineCount();
            if (document.lineCount() > 0) {
                final int line = y / textMetrics.lineHeight;
                if (line >= 0 && line < lineCount) {
                    final Segment lineSegment = documentLine(line);
                    final int lineOffset = lineSegment.offset;
                    final int lineLength = lineSegment.length;
                    final CharacterIterator docIterator = documentCharIterator();
                    char ch = docIterator.setIndex(lineOffset);
                    int column = 0;
                    int charLeftX = 0;

                    while (column < lineLength && ch != CharacterIterator.DONE) {
                        final int charWidth = textMetrics.charWidth(ch);
                        if (charWidth < 0) {
                            break;
                        }

                        charLeftX += charWidth;
                        if (charLeftX > x) {
                            // We have found the column that intersects 'x'.
                            break;
                        }

                        ++column;
                        ch = docIterator.next();
                    }

                    if (ch != CharacterIterator.DONE && !Character.isWhitespace(ch)) {
                        int selectionStartColumn;

                        if (column > 0) {
                            selectionStartColumn = column;

                            if (isWordChar(ch)) {
                                ch = docIterator.previous();
                                // Go to the start of this word.
                                while (selectionStartColumn > 0 &&
                                       ch != CharacterIterator.DONE &&
                                       isWordChar(ch)) {
                                    --selectionStartColumn;
                                    ch = docIterator.previous();
                                }
                            } else {
                                ch = docIterator.previous();
                                // Go to the first letter or digit or whitespace.
                                while (selectionStartColumn > 0 &&
                                       ch != CharacterIterator.DONE &&
                                       !isWordChar(ch) &&
                                       !Character.isWhitespace(ch)) {
                                    --selectionStartColumn;
                                    ch = docIterator.previous();
                                }
                            }
                        } else {
                            selectionStartColumn = 0;
                        }

                        int selectionEndColumn = column;
                        ch = docIterator.setIndex(lineOffset + column);

                        if (isWordChar(ch)) {
                            // Go to the end of this word.
                            do {
                                ++selectionEndColumn;
                                ch = docIterator.next();
                            } while (selectionEndColumn < lineLength &&
                                     ch != CharacterIterator.DONE && isWordChar(ch));
                        } else {
                            // Go to the first letter or digit or whitespace.
                            do {
                                ++selectionEndColumn;
                                ch = docIterator.next();
                            } while (selectionEndColumn < lineLength &&
                                     ch != CharacterIterator.DONE &&
                                     !isWordChar(ch) &&
                                     !Character.isWhitespace(ch));
                        }

                        selection.start.line = line;
                        selection.start.column = selectionStartColumn;
                        selection.end.line = line;
                        selection.end.column = selectionEndColumn;
                        selection.stickyPoint.assign(selection.start);
                        selection.isValid = true;

                        // Move caret to the end of the selected word.
                        caret.moveTo(line, selectionEndColumn);
                        hasSelected = true;
                    }
                }
            }
        } finally {
            document.readUnlock();
        }

        if (hasSelected) {
            editorComponent.repaint();
            scrollToCaret();
        }
    }

    private void scrollToCaret() {
        editorComponent.computeVisibleRect(visibleRect);
        final int columnWidth = columnWidth();
        final int lineHeight = lineHeight();

        if (!caret.areVisualColumnAndXValid()) {
            document.readLock();
            try {
                caret.computeVisualColumnAndX();
            } finally {
                document.readUnlock();
            }
        }

        // Here we build the content rectangle the editor component is to be scrolled to.
        // At best the content rectangle looks as follows:
        //     +-----+-----+-----+
        //     |     |     |     |
        //     |     |     |     |
        //     |     |     |     |
        //     +-----+-----+-----+
        //     |     |Caret|     |
        //     |     |is   |     |
        //     |     |here.|     |
        //     +-----+-----+-----+
        //     |     |     |     |
        //     |     |     |     |
        //     |     |     |     |
        //     +-----+-----+-----+
        // It covers the digit where the caret is currently located, one digit to the left,
        // one digit to the right, three digits right above the caret and three digits
        // right beneath it.
        // Sometimes the content rectangle gets truncated. For instance, if the caret stays
        // at the first line and the first column (i.e. has the coordinates {0, 0}), the
        // content rectangle looks like this:
        //     +-----+-----+
        //     |Caret|     |
        //     |is   |     |
        //     |here |     |
        //     +-----+-----+
        //     |     |     |
        //     |     |     |
        //     |     |     |
        //     +-----+-----+
        // The content rectangle can be truncated if it doesn't fit the editor component's
        // visible rectangle.

        if (caret.position.column > 0) {
            contentRect.x = caret.visualRect.x - columnWidth;
            contentRect.width = columnWidth * 3;
        } else {
            contentRect.x = caret.visualRect.x;
            contentRect.width = columnWidth * 2;
        }
        if (contentRect.width > visibleRect.width) {
            // Make sure the content rect fits the visible rect.
            // The caret's digit must be shown, so adjust the contentRect.x coordinate
            // to ensure that.
            contentRect.x = caret.visualRect.x;
            contentRect.width = visibleRect.width;
        }

        contentRect.y = caret.visualRect.y - lineHeight;
        contentRect.height = lineHeight * 3;

        if (contentRect.y < 0) {
            contentRect.y += lineHeight;
            contentRect.height -= lineHeight;
        }
        if (contentRect.y + contentRect.height > editorComponent.getHeight()) {
            contentRect.height -= lineHeight;
        }
        if (contentRect.height > visibleRect.height) {
            contentRect.y = caret.visualRect.y;
            contentRect.height = visibleRect.height;
        }

        editorComponent.scrollRectToVisible(contentRect);
    }

    private void selectAll() {
        document.readLock();

        try {
            final int lineCount = document.lineCount();
            if (lineCount > 0) {
                selection.start.line = 0;
                selection.start.column = 0;
                selection.end.line = lineCount - 1;
                selection.end.column = document.lineLength(selection.end.line);
                selection.isValid = true;
            }
        } finally {
            document.readUnlock();
        }

        editorComponent.repaint();
    }

    private void copyToClipboard() {
        if (clipboard != null && selection.isValid) {
            selection.computeRegion();
            final StringSelection stringSelection =
                new StringSelection(textRegion(selection.region.offset,
                                               selection.region.length).toString());
            clipboard.setContents(stringSelection, stringSelection);
        }
    }

    private void cutToClipboard() {
        if (documentChangeData.isChanging) {
            throw new IllegalStateException();
        }

        if (clipboard != null && selection.isValid) {
            selection.computeRegion();
            final StringSelection stringSelection =
                new StringSelection(textRegion(selection.region.offset,
                                               selection.region.length).toString());
            clipboard.setContents(stringSelection, stringSelection);

            document.writeLock();

            try {
                if (selection.isValid) {
                    caret.moveTo(selection.end.line, selection.end.column);
                    selection.computeRegion();
                    document.changeText(selection.region.offset, selection.region.length,
                                        emptyCharIterator);
                }
            } finally {
                document.writeUnlock();
                // We need to clear this flag in order not to get stuck in the changing state
                // forever if something went wrong.
                documentChangeData.isChanging = false;
            }
        }

        scrollToCaret();
    }

    private void pasteFromClipboard() {
        if (clipboard != null) {
            final Transferable clipboardContents = clipboard.getContents(this);
            final DataFlavor bestTextFlavor =
                DataFlavor.selectBestTextFlavor(clipboardContents.getTransferDataFlavors());

            if (bestTextFlavor != null) {
                Object clipboardDataObject;
                try {
                    clipboardDataObject = clipboardContents.getTransferData(bestTextFlavor);
                } catch (Exception exception) {
                    throw new RuntimeException(exception);
                }

                if (clipboardDataObject != null) {
                    final Class representationClass = bestTextFlavor.getRepresentationClass();

                    if (String.class.equals(representationClass) &&
                        clipboardDataObject instanceof String) {
                        paste((String)clipboardDataObject);
                    } else {
                        try {
                            paste(
                                DataFlavor.getTextPlainUnicodeFlavor().
                                    getReaderForText(clipboardContents));
                        } catch (UnsupportedFlavorException unsupportedFlavorException) {
                            throw new RuntimeException(unsupportedFlavorException);
                        } catch (IOException ioException) {
                            throw new RuntimeException(ioException);
                        }
                    }
                }
            }
        }
    }

    private void paste(String string) {
        insertText(new StringCharacterIterator(string));
    }

    private void paste(Reader reader) {
        final int charCount;

        try {
            charCount = readText(reader);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        insertText(charArrayIterator.reset(textArray, 0, charCount));
    }

    private void insertChar(char ch) {
        if (documentChangeData.isChanging) {
            throw new IllegalStateException();
        }

        document.writeLock();

        try {
            if (selection.isValid) {
                // Move the caret to the end of the selected region, as most text editors do it
                // that way.
                caret.moveTo(selection.end.line, selection.end.column);
                selection.computeRegion();
                document.changeText(selection.region.offset, selection.region.length,
                                    singleCharIterator.reset(ch));
            } else {
                final int lineCount = document.lineCount();
                if (lineCount > 0) {
                    final Segment lineSegment = documentLine(caret.position.line);
                    final int lineOffset = lineSegment.offset;
                    final int lineLength = lineSegment.length;

                    if (isNewLine(ch)) {
                        // The new line shall start with spaces if the current line does so.
                        int spaceCount = countStartingSpaces(documentCharIterator(),
                                                             lineOffset, lineLength);
                        if (spaceCount > caret.position.column) {
                            if (!caret.isVisualColumnValid()) {
                                caret.computeVisualColumn();
                            }
                            spaceCount = Math.min(spaceCount, caret.visualColumn);
                        }

                        int insertOffset = lineOffset;
                        if (isOverwriteModeOn) {
                            insertOffset += lineLength;
                            caret.moveTo(caret.position.line, lineLength);
                        } else {
                            insertOffset += Math.min(caret.position.column, lineLength);
                        }

                        if (spaceCount > 0) {
                            final CharacterIterator textToInsert =
                                compositeCharIterator.reset(
                                    singleCharIterator.reset(ch),
                                    repeatingCharIterator.reset(' ', spaceCount));
                            document.changeText(insertOffset, 0, textToInsert);
                        } else {
                            document.insertChar(insertOffset, ch);
                        }
                    } else {
                        final int trailingSpaceCount = caret.position.column - lineLength;
                        if (trailingSpaceCount > 0) {
                            // We gotta insert 'trailingSpaceCount' spaces before the typed
                            // character.
                            final CharacterIterator textToInsert =
                                compositeCharIterator.reset(
                                    repeatingCharIterator.reset(' ', trailingSpaceCount),
                                    singleCharIterator.reset(ch));
                            document.changeText(lineOffset + lineLength, 0, textToInsert);
                        } else {
                            final int caretOffset = lineOffset + caret.position.column;
                            if (trailingSpaceCount < 0 && isOverwriteModeOn) {
                                caret.stepRight();
                                document.changeText(caretOffset, 1, singleCharIterator.reset(ch));
                            } else {
                                document.insertChar(caretOffset, ch);
                            }
                        }
                    }
                } else {
                    if (isNewLine(ch)) {
                        document.insertChar(0, ch);
                    } else {
                        final int spaceCount = caret.position.column;
                        if (spaceCount > 0) {
                            // We gotta insert 'spaceCount' spaces before the typed character.
                            final CharacterIterator textToInsert =
                                compositeCharIterator.reset(
                                    repeatingCharIterator.reset(' ', spaceCount),
                                    singleCharIterator.reset(ch));
                            document.changeText(0, 0, textToInsert);
                        } else {
                            document.insertChar(0, ch);
                        }
                    }
                }
            }
        } finally {
            document.writeUnlock();
            // We need to clear this flag in order not to get stuck in the changing state forever
            // if something went wrong.
            documentChangeData.isChanging = false;
        }

        scrollToCaret();
    }

    private void insertText(CharacterIterator text) {
        if (documentChangeData.isChanging) {
            throw new IllegalStateException();
        }

        document.writeLock();

        try {
            if (selection.isValid) {
                // Move the caret to the end of the selected region, as most text editors do it
                // that way.
                caret.moveTo(selection.end.line, selection.end.column);
                selection.computeRegion();
                document.changeText(selection.region.offset, selection.region.length,
                                    text);
            } else {
                if (document.lineCount() > 0) {
                    final Segment lineSegment = documentLine(caret.position.line);
                    final int lineOffset = lineSegment.offset;
                    final int lineLength = lineSegment.length;
                    char ch = text.first();

                    if (isNewLine(ch)) {
                        // The new line shall start with spaces if the current line does so.
                        int spaceCount = countStartingSpaces(documentCharIterator(),
                                                             lineOffset, lineLength);
                        if (spaceCount > caret.position.column) {
                            if (!caret.isVisualColumnValid()) {
                                caret.computeVisualColumn();
                            }
                            spaceCount = Math.min(spaceCount, caret.visualColumn);
                        }

                        final int insertOffset =
                            lineOffset + Math.min(caret.position.column, lineLength);
                        CharacterIterator textToInsert;
                        if (spaceCount > 0) {
                            textBuilder.setLength(1);
                            textBuilder.setCharAt(0, ch);
                            for (ch = text.next(); ch != CharacterIterator.DONE && isNewLine(ch);
                                 ch = text.next()) {
                                textBuilder.append(ch);
                            }
                            for (int i = 0; i < spaceCount; ++i) {
                                textBuilder.append(Characters.SPACE);
                            }
                            for (; ch != CharacterIterator.DONE; ch = text.next()) {
                                textBuilder.append(ch);
                            }
                            textToInsert = charSequenceIterator.reset(textBuilder);
                        } else {
                            textToInsert = text;
                        }

                        document.changeText(insertOffset, 0, textToInsert);
                    } else {
                        final int trailingSpaceCount = caret.position.column - lineLength;
                        if (trailingSpaceCount > 0) {
                            // We gotta insert 'trailingSpaceCount' spaces before the typed
                            // character.
                            final CharacterIterator textToInsert =
                                compositeCharIterator.reset(
                                    repeatingCharIterator.reset(' ', trailingSpaceCount),
                                    text);
                            document.changeText(lineOffset + lineLength, 0, textToInsert);
                        } else {
                            final int caretOffset = lineOffset + caret.position.column;
                            document.changeText(caretOffset, 0, text);
                        }
                    }
                }
            }
        } finally {
            document.writeUnlock();
            // We need to clear this flag in order not to get stuck in the changing state forever
            // if something went wrong.
            documentChangeData.isChanging = false;
        }

        scrollToCaret();
    }

    private void deleteCurrentChar() {
        if (documentChangeData.isChanging) {
            throw new IllegalStateException();
        }

        boolean hasDeleted = true;
        document.writeLock();

        try {
            if (selection.isValid) {
                selection.computeRegion();
                // Move the caret to the start of the selected region, as most text editors do it
                // that way.
                caret.moveTo(selection.start.line, selection.start.column);
                document.changeText(selection.region.offset, selection.region.length,
                                    emptyCharIterator);
            } else {
                final int lineCount = document.lineCount();
                if (lineCount > 0) {
                    final Segment lineSegment = documentLine(caret.position.line);
                    final int lineOffset = lineSegment.offset;
                    final int lineLength = lineSegment.length;
                    final int trailingSpaceCount = caret.position.column - lineLength;

                    if (trailingSpaceCount < 0) {
                        final int caretOffset = lineOffset + caret.position.column;
                        document.deleteChar(caretOffset);
                    } else {
                        if (caret.position.line < lineCount - 1) {
                            if (trailingSpaceCount > 0) {
                                // Here we substitute the 'new line' character at the end of the
                                // line with the series of spaces.
                                final CharacterIterator newText =
                                    repeatingCharIterator.reset(' ', trailingSpaceCount);
                                // Move caret to the start of the next line in order not to get
                                // shifted back after the text change.
                                caret.moveTo(caret.position.line + 1, 0);
                                document.changeText(lineOffset + lineLength, 1, newText);
                            } else {
                                document.deleteChar(lineOffset + lineLength);
                            }
                        } else {
                            hasDeleted = false;
                        }
                    }
                }
            }
        } finally {
            document.writeUnlock();
            // We need to clear this flag in order not to get stuck in the changing state forever
            // if something went wrong.
            documentChangeData.isChanging = false;
        }

        if (hasDeleted) {
            scrollToCaret();
        }
    }

    private void deletePreviousChar() {
        if (documentChangeData.isChanging) {
            throw new IllegalStateException();
        }

        boolean hasDeleted = true;
        boolean shallRepaint = false;
        document.writeLock();

        try {
            if (selection.isValid) {
                selection.computeRegion();
                document.changeText(selection.region.offset, selection.region.length,
                                    emptyCharIterator);
            } else {
                if (document.lineCount() > 0) {
                    final Segment lineSegment = documentLine(caret.position.line);

                    if (caret.position.column <= lineSegment.length) {
                        final int caretOffset = lineSegment.offset + caret.position.column;
                        if (caretOffset > 0) {
                            document.deleteChar(caretOffset - 1);
                        }
                    } else {
                        shallRepaint = hasDeleted = caret.stepLeft();
                    }
                } else {
                    shallRepaint = hasDeleted = caret.stepLeft();
                }
            }
        } finally {
            document.writeUnlock();
            // We need to clear this flag in order not to get stuck in the changing state
            // forever if something went wrong.
            documentChangeData.isChanging = false;
        }

        if (shallRepaint) {
            editorComponent.repaint();
        }

        if (hasDeleted) {
            scrollToCaret();
        }
    }

    private void invertOverwriteMode() {
        isOverwriteModeOn = !isOverwriteModeOn;
        if (listener != null) {
            listener.overwriteModeChanged(isOverwriteModeOn);
        }
    }

    private void fireCaretMoved() {
        if (listener != null) {
            listener.caretMoved(caret.position.line, caret.position.column);
        }
    }

    private void drawTextAndCaret(Graphics graphics,
                                  int clipLeftX,
                                  int clipTopY,
                                  int clipWidth,
                                  int clipHeight) {
        final int lineHeight = textMetrics.lineHeight;
        int firstLineToDraw = clipTopY / lineHeight;

        if (firstLineToDraw < document.lineCount()) {
            final int clipBottomY = clipTopY + clipHeight;
            int lastLineToDraw = clipBottomY / lineHeight;

            if (firstLineToDraw > 0) {
                if (firstLineToDraw * lineHeight + textMetrics.maxDescent > clipTopY) {
                    // Some glyphs from the line above may get into the clipping area.
                    // So we have to draw that caretLine as well.
                    --firstLineToDraw;
                }
            }

            if ((lastLineToDraw + 1) * lineHeight - textMetrics.maxAscent <= clipBottomY) {
                // Some glyphs from the line below may get into the clipping area.
                // So we have to draw that caretLine as well.
                ++lastLineToDraw;
            }

            int baselineY = (firstLineToDraw + 1) * lineHeight;
            final SegmentIterator lineIterator = documentLineIterator(firstLineToDraw);

            for (int lineIdx = firstLineToDraw; lineIdx <= lastLineToDraw;
                 ++lineIdx, baselineY += lineHeight) {
                if (!lineIterator.next(lineSegment)) {
                    break;
                }

                drawTextLineAndCaret(graphics, baselineY, clipLeftX, clipWidth,
                                     documentCharIterator(),
                                     lineSegment.offset, lineSegment.length, lineIdx);
            }
        }
    }

    private void drawCaretOnly(Graphics graphics) {
        graphics.setColor(CARET_COLOR);
        caret.paint(graphics);
    }

    private void drawTextLineAndCaret(Graphics graphics,
                                      int baselineY,
                                      int clipLeftX,
                                      int clipWidth,
                                      CharacterIterator docIterator,
                                      int lineOffset,
                                      int lineLength,
                                      int lineIndex) {
        final int clipRightX = clipLeftX + clipWidth;
        charBuffer.clear();
        charXBuffer.clear();
        char ch = docIterator.setIndex(lineOffset);
        int column = 0;
        int x = 0;
        int charWidth = 0;
        int firstVisibleColumn = 0;
        boolean shallDrawCaret = (lineIndex == caret.position.line && isCaretVisible());
        boolean shallComputeCaretVisualColumnAndX =
            (shallDrawCaret && !caret.areVisualColumnAndXValid());

        if (shallComputeCaretVisualColumnAndX) {
            int visualColumn = 0;
            int charVisualLength = 0;

            for (; column < lineLength; ch = docIterator.next(), ++column) {
                charVisualLength = charVisualLength(ch);
                if (charVisualLength < 0) {
                    break;
                }

                charWidth = textMetrics.charWidth(ch);
                if (charWidth < 0) {
                    break;
                }

                final int nextX = x + charWidth;
                if (nextX > clipLeftX) {
                    // The first character to be drawn is found.
                    firstVisibleColumn = column;
                    break;
                }

                if (column == caret.position.column) {
                    // The caret's visual column and x-coordinate are found.
                    caret.visualColumn = visualColumn;
                    caret.visualRect.x = x;
                    shallComputeCaretVisualColumnAndX = false;
                }

                visualColumn += charVisualLength;
                x = nextX;
            }

            if (shallComputeCaretVisualColumnAndX) {
                for (; column < lineLength; ++column) {
                    if (charVisualLength < 0 || charWidth < 0) {
                        break;
                    }

                    if (column == caret.position.column) {
                        // The caret's visual column and x-coordinate are found.
                        caret.visualColumn = visualColumn;
                        caret.visualRect.x = x;
                        shallComputeCaretVisualColumnAndX = false;
                    }

                    if (x > clipRightX) {
                        break;
                    }

                    charBuffer.append(ch);
                    charXBuffer.append(x);

                    visualColumn += charVisualLength;
                    x += charWidth;
                    ch = docIterator.next();
                    charVisualLength = charVisualLength(ch);
                    charWidth = textMetrics.charWidth(ch);
                }

                if (shallComputeCaretVisualColumnAndX) {
                    // The caret's visual column and x-coordinate still aren't computed.
                    if (column == lineLength) {
                        // We have iterated to the end of the line, but the caret stays beyond
                        // this end.
                        final int trailingSpaceCount = caret.position.column - lineLength;
                        caret.visualColumn = visualColumn + trailingSpaceCount;
                        caret.visualRect.x = x + trailingSpaceCount * textMetrics.spaceWidth;
                    } else {
                        // We are here, which basically means that we have reached the right
                        // margin of the clipping area, but the caret stays beyond this margin,
                        // so it's culled out by the clipping, and we don't need to draw it.
                        shallDrawCaret = false;
                    }
                }
            } else {
                for (; column < lineLength; ++column) {
                    if (x >= clipRightX || charWidth < 0) {
                        break;
                    }

                    charBuffer.append(ch);
                    charXBuffer.append(x);

                    x += charWidth;
                    ch = docIterator.next();
                    charWidth = textMetrics.charWidth(ch);
                }
            }
        } else { // No need to compute caret's coordinates.
            for (; column < lineLength; ch = docIterator.next(), ++column) {
                charWidth = textMetrics.charWidth(ch);
                if (charWidth < 0) {
                    break;
                }

                final int nextX = x + charWidth;
                if (nextX > clipLeftX) {
                    // The first character to be drawn is found.
                    firstVisibleColumn = column;
                    break;
                }

                x = nextX;
            }

            for (; column < lineLength; ++column) {
                if (x >= clipRightX || charWidth < 0) {
                    break;
                }

                charBuffer.append(ch);
                charXBuffer.append(x);

                x += charWidth;
                ch = docIterator.next();
                charWidth = textMetrics.charWidth(ch);
            }
        }

        if (charBuffer.size > 0) {
            int selectionStartInBuffer = Integer.MIN_VALUE;
            int selectionEndInBuffer = Integer.MIN_VALUE;

            if (selection.isValid) {
                if (selection.start.line < lineIndex) {
                    if (selection.end.line > lineIndex) {
                        selectionEndInBuffer = Integer.MAX_VALUE;
                    } else if (selection.end.line == lineIndex) {
                        selectionEndInBuffer = selection.end.column - firstVisibleColumn;
                    }
                } else if (selection.start.line == lineIndex) {
                    selectionStartInBuffer = selection.start.column - firstVisibleColumn;

                    if (selection.end.line > lineIndex) {
                        selectionEndInBuffer = Integer.MAX_VALUE;
                    } else if (selection.end.line == lineIndex) {
                        selectionEndInBuffer = selection.end.column - firstVisibleColumn;
                    }
                }
            }

            if (selectionStartInBuffer > 0) {
                // Draw text before the selection.
                drawColorizedText(graphics, baselineY, lineOffset + firstVisibleColumn,
                                  0, Math.min(selectionStartInBuffer, charBuffer.size));
            }

            if (selectionStartInBuffer <= charBuffer.size && selectionEndInBuffer > 0) {
                // Draw selection rectangle and and selected text.
                int firstCharIdx,
                    charCount;
                int selectionStartX,
                    selectionWidth;

                if (selectionStartInBuffer < 0) {
                    firstCharIdx = 0;
                    selectionStartX = clipLeftX;
                } else if (selectionStartInBuffer < charBuffer.size) {
                    firstCharIdx = selectionStartInBuffer;
                    selectionStartX = charXBuffer.content[selectionStartInBuffer];
                } else { // (selectionStartInBuffer == charBufferSize)
                    firstCharIdx = charBuffer.size;
                    final int lastCharIdx = charBuffer.size - 1;
                    selectionStartX =
                        lastCharIdx >= 0 ? charXBuffer.content[lastCharIdx] +
                                           textMetrics.charWidth(charBuffer.content[lastCharIdx])
                                         : 0;
                }

                if (selectionEndInBuffer < charBuffer.size) {
                    charCount = selectionEndInBuffer - firstCharIdx;
                    selectionWidth = charXBuffer.content[selectionEndInBuffer] - selectionStartX;
                } else if (selectionEndInBuffer == charBuffer.size) {
                    charCount = selectionEndInBuffer - firstCharIdx;
                    final int lastCharIdx = charBuffer.size - 1;
                    selectionWidth =
                        lastCharIdx >= 0 ? charXBuffer.content[lastCharIdx] +
                                           textMetrics.charWidth(charBuffer.content[lastCharIdx]) -
                                           selectionStartX
                                         : 0;
                } else {
                    charCount = charBuffer.size - firstCharIdx;
                    selectionWidth = clipWidth;
                }

                if (selectionWidth > 0) {
                    drawSelectionRect(graphics, baselineY, selectionStartX, selectionWidth);
                }

                if (charCount > 0) {
                    // Draw selected text.
                    graphics.setColor(SELECTED_TEXT_COLOR);
                    drawUntabifiedText(graphics, baselineY, firstCharIdx, charCount);
                }
            }

            if (selectionEndInBuffer < charBuffer.size) {
                // Draw text after the selection.
                final int firstCharIdx = Math.max(selectionEndInBuffer, 0);
                drawColorizedText(graphics, baselineY,
                                  lineOffset + firstVisibleColumn + firstCharIdx,
                                  firstCharIdx, charBuffer.size - firstCharIdx);
            }
        } else { // (charBufferSize == 0).
            // We don't need to render any text here, but might still need to draw the selection.
            if (selection.isValid &&
                (selection.start.line < lineIndex ||
                 (selection.start.line == lineIndex && selection.start.column <= lineLength)) &&
                selection.end.line > lineIndex) {
                // Draw it.
                drawSelectionRect(graphics, baselineY, clipLeftX, clipWidth);
            }
        }

        if (shallDrawCaret) {
            final int caretLeftX  = caret.visualRect.x,
                      caretRightX = caretLeftX + caret.visualRect.width;
            if (caretRightX > clipLeftX && caretLeftX < clipRightX) {
                graphics.setColor(CARET_COLOR);
                caret.paint(graphics);
            }
        }
    }

    private void drawColorizedText(Graphics graphics,
                                   int baselineY,
                                   int offset,
                                   int offsetInBuffer,
                                   int charsToDraw) {
        tokenIterator = textTinter.iterator(offset, tokenIterator);

        if (tokenIterator.next(tokenSegment)) {
            int charsToDrawWithCurrentColor = tokenSegment.offset - offset;

            if (charsToDrawWithCurrentColor > 0) {
                graphics.setColor(DEFAULT_TEXT_COLOR);

                if (charsToDrawWithCurrentColor >= charsToDraw) {
                    drawUntabifiedText(graphics, baselineY, offsetInBuffer, charsToDraw);
                    return;
                }

                drawUntabifiedText(graphics, baselineY, offsetInBuffer,
                                   charsToDrawWithCurrentColor);
                offset += charsToDrawWithCurrentColor;
                offsetInBuffer += charsToDrawWithCurrentColor;
                charsToDraw -= charsToDrawWithCurrentColor;
            } else {
                tokenSegment.length += charsToDrawWithCurrentColor;
            }

            for (;;) {
                final int tokenLength = tokenSegment.length;
                final Color tokenColor = tokenSegment.data == null ? DEFAULT_TEXT_COLOR
                                                                   : tokenSegment.data;
                graphics.setColor(tokenColor);
                charsToDrawWithCurrentColor = tokenLength;

                if (charsToDrawWithCurrentColor >= charsToDraw) {
                    drawUntabifiedText(graphics, baselineY, offsetInBuffer, charsToDraw);
                    break;
                }

                offset += charsToDrawWithCurrentColor;

                if (tokenIterator.next(tokenSegment)) {
                    if (tokenColor.equals(DEFAULT_TEXT_COLOR)) {
                        charsToDrawWithCurrentColor += tokenSegment.offset - offset;

                        if (charsToDrawWithCurrentColor >= charsToDraw) {
                            drawUntabifiedText(graphics, baselineY, offsetInBuffer, charsToDraw);
                            break;
                        }

                        drawUntabifiedText(graphics,  baselineY, offsetInBuffer,
                                           charsToDrawWithCurrentColor);
                        offset = tokenSegment.offset;
                        offsetInBuffer += charsToDrawWithCurrentColor;
                        charsToDraw -= charsToDrawWithCurrentColor;
                    } else {
                        drawUntabifiedText(graphics, baselineY, offsetInBuffer,
                                           charsToDrawWithCurrentColor);
                        offsetInBuffer += charsToDrawWithCurrentColor;
                        charsToDraw -= charsToDrawWithCurrentColor;
                        graphics.setColor(DEFAULT_TEXT_COLOR);
                        charsToDrawWithCurrentColor = tokenSegment.offset - offset;

                        if (charsToDrawWithCurrentColor >= charsToDraw) {
                            drawUntabifiedText(graphics, baselineY, offsetInBuffer, charsToDraw);
                            break;
                        }

                        drawUntabifiedText(graphics, baselineY, offsetInBuffer,
                                           charsToDrawWithCurrentColor);
                        offset = tokenSegment.offset;
                        offsetInBuffer += charsToDrawWithCurrentColor;
                        charsToDraw -= charsToDrawWithCurrentColor;
                    }
                } else {
                    if (tokenColor.equals(DEFAULT_TEXT_COLOR)) {
                        drawUntabifiedText(graphics,  baselineY, offsetInBuffer, charsToDraw);
                    } else {
                        drawUntabifiedText(graphics,  baselineY, offsetInBuffer,
                                           charsToDrawWithCurrentColor);
                        offsetInBuffer += charsToDrawWithCurrentColor;
                        charsToDraw -= charsToDrawWithCurrentColor;
                        graphics.setColor(DEFAULT_TEXT_COLOR);
                        drawUntabifiedText(graphics, baselineY, offsetInBuffer, charsToDraw);
                    }

                    break;
                }
            }
        } else {
            graphics.setColor(DEFAULT_TEXT_COLOR);
            drawUntabifiedText(graphics, baselineY, offsetInBuffer, charsToDraw);
        }
    }

    private void drawUntabifiedText(Graphics graphics,
                                    int baselineY,
                                    int offsetInBuffer,
                                    int charsToDraw) {
        if (charBuffer.tabCount > 0) {
            final int maxUntabifiedTextLength =
                charsToDraw + Math.min(charsToDraw, charBuffer.tabCount) * tabSize;
            if (maxUntabifiedTextLength > textArray.length) {
                textArray = new char[maxUntabifiedTextLength];
            }

            final char [] originalText = charBuffer.content;
            int charCountInUntabifiedText = 0;

            for (int i = offsetInBuffer, lastI = offsetInBuffer + charsToDraw; i < lastI; ++i) {
                final char ch = originalText[i];
                if (ch == Characters.CHARACTER_TABULATION) {
                    int j = charCountInUntabifiedText;
                    charCountInUntabifiedText += tabSize;
                    for (; j < charCountInUntabifiedText; ++j) {
                        textArray[j] = Characters.SPACE;
                    }
                } else {
                    textArray[charCountInUntabifiedText++] = ch;
                }
            }

            graphics.drawChars(textArray, 0, charCountInUntabifiedText,
                               charXBuffer.content[offsetInBuffer], baselineY);
        } else {
            // No need to untabify the text.
            graphics.drawChars(charBuffer.content, offsetInBuffer, charsToDraw,
                               charXBuffer.content[offsetInBuffer], baselineY);
        }
    }

    private void drawSelectionRect(Graphics graphics,
                                   int baselineY, int leftX,int width) {
        graphics.setColor(SELECTION_COLOR);
        graphics.fillRect(leftX,
                          baselineY - textMetrics.lineHeight + textMetrics.descent,
                          width,
                          textMetrics.lineHeight);
    }

    private static boolean isNewLine(char ch) {
        return (ch == Characters.LINE_FEED || ch == Characters.CARRIAGE_RETURN);
    }

    private boolean isCaretVisible() {
        return (caret.isVisible && editorComponent.isFocusOwner());
    }

    private CharacterIterator documentCharIterator() {
        charIterator = document.iterator(charIterator);
        return charIterator;
    }

    private SegmentIterator documentLineIterator(int lineIndex) {
        if (defaultLineIterator == null) {
            lineIterator = document.lineIterator(lineIndex, lineIterator);

            if (lineIterator == null) {
                defaultLineIterator = new LineIterator(lineIndex);
            } else {
                return lineIterator;
            }
        }

        return defaultLineIterator.reset(lineIndex);
    }

    private Segment documentLine(int lineIndex) {
        document.line(lineIndex, lineSegment);
        return lineSegment;
    }

    private int charVisualLength(char ch) {
        // TODO: Elaborate it to support special characters like e.g. line tabulation.
        switch (ch) {
            case Characters.CHARACTER_TABULATION :
                return tabSize;
            case CharacterIterator.DONE:
                return -1;
        }
        return 1;
    }

    private int textVisualLength(CharacterIterator charIterator,
                                 int firstCharIndex, int charCount) {
        int textVisualLength = 0;
        char ch = charIterator.setIndex(firstCharIndex);


        for (int i = 0; i < charCount; ++i, ch = charIterator.next()) {
            final int charVisualLength = charVisualLength(ch);
            if (charVisualLength < 0) {
                break;
            }
            textVisualLength += charVisualLength;
        }

        return textVisualLength;
    }

    private int textWidth(CharacterIterator charIterator,
                          int firstCharIndex, int charCount) {
        int textWidth = 0;
        char ch = charIterator.setIndex(firstCharIndex);

        for (int i = 0; i < charCount; ++i, ch = charIterator.next()) {
            final int charWidth = textMetrics.charWidth(ch);
            if (charWidth < 0) {
                break;
            }
            textWidth += charWidth;
        }

        return textWidth;
    }

    private void textVisualLengthAndWidth(CharacterIterator charIterator,
                                          int firstCharIndex, int charCount,
                                          int[] target) {
        int textVisualLength = 0;
        int textWidth = 0;
        char ch = charIterator.setIndex(firstCharIndex);


        for (int i = 0; i < charCount; ++i, ch = charIterator.next()) {
            final int charVisualLength = charVisualLength(ch);
            final int charWidth = textMetrics.charWidth(ch);
            if (charVisualLength < 0 || charWidth < 0) {
                break;
            }
            textVisualLength += charVisualLength;
            textWidth += charWidth;
        }

        target[0] = textVisualLength;
        target[1] = textWidth;
    }

    private void computeTextWidth() {
        lineWidthList.clear();
        lineWidthToCountMap.clear();

        if (document.lineCount() > 0) {
            final SegmentIterator lineIterator = documentLineIterator(0);
            final CharacterIterator charIterator = documentCharIterator();
            while (lineIterator.next(lineSegment)) {
                final Integer lineWidth = intObj(textWidth(charIterator,
                                                           lineSegment.offset,
                                                           lineSegment.length));
                lineWidthList.append(lineWidth);
                addLineWidth(lineWidth);
            }
        }

        textWidth = widestLineWidth();
    }

    private void computeTextHeight() {
        textHeight = document.lineCount() * textMetrics.lineHeight + textMetrics.maxDescent;
    }

    private void updateTextWidthAndHeight() {
        final SegmentIterator lineIterator =
            documentLineIterator(documentChangeData.firstChangedLine);

        if (lineIterator.next(lineSegment)) {
            final CharacterIterator charIterator = documentCharIterator();
            final int changedRegionEnd = documentChangeData.offset + documentChangeData.newLength;

            if (documentChangeData.length == 0 &&
                changedRegionEnd <= lineSegment.offset + lineSegment.length) {
                // This document update represents the insertion of text within one text-line.
                // Only one text-line has changed its width.
                final TreeList.Entry<Integer> lineWidthEntry =
                    lineWidthList.get(documentChangeData.firstChangedLine);

                if (lineWidthEntry != null) {
                    final Integer oldWidth = lineWidthEntry.value();
                    final Integer newWidth =
                        intObj(oldWidth + textWidth(charIterator,
                                                    documentChangeData.offset,
                                                    documentChangeData.newLength));
                    lineWidthEntry.setValue(newWidth);
                    removeLineWidth(oldWidth);
                    addLineWidth(newWidth);
                    textWidth = widestLineWidth();
                }
            } else {
                TreeList.Entry<Integer> lineWidthEntry =
                    lineWidthList.get(documentChangeData.firstChangedLine);

                if (lineWidthEntry != null) {
                    int lineIndex = documentChangeData.firstChangedLine;
                    final int oldLineCount = documentChangeData.lineCount;
                    final int newLineCount = document.lineIndex(changedRegionEnd) - lineIndex + 1;
                    final int numberOfLinesWithChangedWidths = Math.min(oldLineCount, newLineCount);

                    for (final int stopIndex = lineIndex + numberOfLinesWithChangedWidths;
                         lineIndex < stopIndex; ++lineIndex) {
                        final Integer oldWidth = lineWidthEntry.value();
                        final Integer newWidth = intObj(textWidth(charIterator,
                                                                  lineSegment.offset,
                                                                  lineSegment.length));
                        lineWidthEntry.setValue(newWidth);
                        removeLineWidth(oldWidth);
                        addLineWidth(newWidth);
                        lineIterator.next(lineSegment);
                        lineWidthEntry = lineWidthEntry.next();
                    }

                    final int linesToInsert = newLineCount - oldLineCount;
                    if (linesToInsert > 0) {
                        for (final int stopIndex = lineIndex + linesToInsert;
                             lineIndex < stopIndex; ++lineIndex) {
                            final Integer width = intObj(textWidth(charIterator,
                                                                   lineSegment.offset,
                                                                   lineSegment.length));
                            lineWidthList.insert(lineIndex, width);
                            addLineWidth(width);
                            if (!lineIterator.next(lineSegment)) {
                                break;
                            }
                        }
                    } else if (linesToInsert < 0) {
                        final int linesToRemove = -linesToInsert;
                        for (int i = 0; i < linesToRemove && lineWidthEntry != null; ++i) {
                            removeLineWidth(lineWidthEntry.value());
                            final TreeList.Entry<Integer> nextEntry = lineWidthEntry.next();
                            lineWidthList.remove(lineWidthEntry);
                            lineWidthEntry = nextEntry;
                        }
                    }

                    textWidth = widestLineWidth();
                    if (newLineCount != oldLineCount) {
                        computeTextHeight();
                    }
                }
            }
        }
    }

    private int widestLineWidth() {
        // The max text-line width value is the greatest key in the width->count map.
        return lineWidthToCountMap.isEmpty() ? 0 : lineWidthToCountMap.lastKey();
    }

    private void addLineWidth(Integer width) {
        final Integer count = lineWidthToCountMap.get(width);
        final int newCount = count == null ? 1 : count + 1;
        lineWidthToCountMap.put(width, intObj(newCount));
    }

    private void removeLineWidth(Integer width) {
        final Integer count = lineWidthToCountMap.get(width);
        if (count != null) {
            if (count.equals(1)) {
                lineWidthToCountMap.remove(width);
            } else {
                lineWidthToCountMap.put(width, intObj(count - 1));
            }
        }
    }

    private int countStartingSpaces(CharacterIterator charIterator,
                                    int firstCharIndex, int charCount) {
        int count = 0;
        char ch = charIterator.setIndex(firstCharIndex);

        for (int i = 0;
             i < charCount && Character.isWhitespace(ch) && ch != CharacterIterator.DONE;
             ++i, ch = charIterator.next()) {
            count += Math.max(charVisualLength(ch), 0);
        }

        return count;
    }

    private static Integer intObj(int value) {
        if (value < INTEGER_POOL.length) {
            if (INTEGER_POOL[value] == null) {
                synchronized (INTEGER_POOL) {
                    // Use double check lock here.
                    if (INTEGER_POOL[value] == null) {
                        INTEGER_POOL[value] = value;
                    }
                }
            }
            return INTEGER_POOL[value];
        }
        return value;
    }

    private Object textRegion(int offset, int length) {
        textBuilder.setLength(0);
        final CharacterIterator docIterator = documentCharIterator();
        char ch = docIterator.setIndex(offset);

        for (int i = 0; i < length && ch != CharacterIterator.DONE; ++i, ch = docIterator.next()) {
            textBuilder.append(ch);
        }

        return textBuilder;
    }

    private int readText(Reader reader) throws IOException {
        int charsRead = 0;
        int charsToRead = textArray.length;

        for (;;) {
            if (charsToRead > 0) {
                final int charsJustRead = reader.read(textArray, charsRead, charsToRead);
                if (charsJustRead < 0) {
                    // End of the stream is reached.
                    break;
                }

                charsRead += charsJustRead;
                charsToRead -= charsJustRead;
            } else {
                final int newArraySize = (textArray.length * 5) >>> 2;
                charsToRead = newArraySize - textArray.length;
                textArray = Arrays.copyOf(textArray, newArraySize);
            }
        }

        return charsRead;
    }

    private static boolean isWordChar(char ch) {
        return Character.isJavaIdentifierPart(ch);
    }

    static interface Listener {
        void preferredSizeChanged(int newWidth, int newHeight);
        void overwriteModeChanged(boolean isOverwriteModeOn);
        void caretMoved(int newLine, int newColumn);
    }

    private final class TextMetrics {
        private final FontMetrics fontMetrics;
        private final int lineHeight;
        private final int descent;
        private final int maxAscent;
        private final int maxDescent;
        private final int spaceWidth;
        private final int[] charWidths;
        private int tabWidth;

        private TextMetrics(FontMetrics fontMetrics) {
            if (fontMetrics == null) {
                throw new NullPointerException();
            }
            final Font font = fontMetrics.getFont();
            if (font == null) {
                throw new NullPointerException();
            }

            this.fontMetrics = fontMetrics;
            lineHeight = fontMetrics.getHeight();
            descent = fontMetrics.getDescent();
            maxAscent = fontMetrics.getMaxAscent();
            maxDescent = fontMetrics.getMaxDescent();
            spaceWidth = fontMetrics.charWidth(Characters.SPACE);
            final int[] charWidths = fontMetrics.getWidths();
            this.charWidths = new int[Math.min(charWidths.length, 0x80)];
            System.arraycopy(charWidths, 0, this.charWidths, 0, this.charWidths.length);
            tabWidth = spaceWidth * tabSize;
        }

        private int charWidth(char ch) {
            switch (ch) {
                case Characters.CHARACTER_TABULATION:
                    return tabWidth;
                case CharacterIterator.DONE :
                    return -1;
                default:
                    return ch < charWidths.length ? textMetrics.charWidths[ch]
                                                  : textMetrics.fontMetrics.charWidth(ch);
            }
        }

        private void setTabSize(int tabSize) {
            tabWidth = spaceWidth * tabSize;
        }
    }

    private final class Caret {
        private final TextPosition position = new TextPosition();

        // Negative value of visualColumn or visualRect.x means that the actual value is not
        // known and is to be computed as needed.
        private int visualColumn = -1;
        private final Rectangle visualRect = new Rectangle(-1, 0, WIDTH, 0);

        private static final int WIDTH = 2;
        private static final int BLINK_PERIOD = 500; // 500 ms.

        private final int[] visualLengthAndWidth = new int[2];
        private final Blinker blinker = new Blinker();
        private boolean isVisible = true;
        private long positionTime = System.currentTimeMillis();
        private final Runnable repainter = new Repainter();

        private Caret() {
            position.line = 0;
            position.column = 0;
            visualColumn = 0;
            visualRect.x = 0;
            computeYAndHeight();
            blinker.start(BLINK_PERIOD);
        }

        /**
         * Moves the caret.
         *
         * @param newLine    line the caret is to be moved to.
         * @param newColumn  logical column the caret is to be moved to.
         */
        private void moveTo(int newLine, int newColumn) {
            if (newColumn < 0) {
                newColumn = 0;
            }

            final int lineCount = document.lineCount();
            if (newLine >= lineCount) {
                newLine = lineCount - 1;
            }
            if (newLine < 0) {
                newLine = 0;
            }

            boolean hasMoved = false;
            if (newLine == position.line) {
                if (newColumn != position.column) {
                    hasMoved = true;
                }
            } else {
                hasMoved = true;
                position.line = newLine;
                computeYAndHeight();
            }

            if (hasMoved) {
                position.column = newColumn;

                if (lineCount > 0) {
                    // Invalidate visual data, it will be computed as needed.
                    invalidateVisualColumnAndX();
                } else {
                    // It's that simple to compute the visual column and caret x right here.
                    visualColumn = newColumn;
                    visualRect.x = newColumn * textMetrics.spaceWidth;
                }

                fireCaretMoved();
                positionTime = System.currentTimeMillis();
            }
        }

        private void moveToXY(int x, int y) {
            final int lineCount = document.lineCount();
            int newLine = y / textMetrics.lineHeight;
            if (newLine >= lineCount) {
                newLine = lineCount - 1;
            }
            if (newLine < 0) {
                newLine = 0;
            }

            boolean hasMoved = false;

            if (lineCount > 0) {
                // So now we know the new line for the caret and we gotta compute the new column for
                // it. To accomplish this we need to iterate though the text line and find the
                // position (column) of the character that intersect the specified x-coordinate.
                // In the most generic case we iterate from the first character on this text line,
                // but if the caret's line has not changed, and we know the caret's current
                // x-coordinate, the iteration can be optimized by means of starting from the
                // current caret position.

                int newColumn = 0;
                int newVisualColumn = 0;
                int newX = 0;
                boolean iterateRight = true;
                int shiftInPixels = x;

                if (newLine == position.line) {
                    if (areVisualColumnAndXValid()) {
                        // The current values of the caret's visual column and x-coordinate
                        // are valid.
                        shiftInPixels -= visualRect.x;
                        newColumn = position.column;
                        newVisualColumn = visualColumn;
                        newX = visualRect.x;

                        if (shiftInPixels < 0/* && x > -shiftInPixels*/) {
                            // Iterate left from the current caret position.
                            iterateRight = false;
                        }
                    }
                } else {
                    hasMoved = true;
                    position.line = newLine;
                    computeYAndHeight();
                }

                final Segment lineSegment = documentLine(position.line);
                final int lineOffset = lineSegment.offset;
                final int lineLength = lineSegment.length;

                if (iterateRight) {
                    if (newColumn < lineLength) {
                        final CharacterIterator charIterator = documentCharIterator();
                        char ch = charIterator.setIndex(lineOffset + newColumn);
                        boolean isIntersectionFound = false;

                        for (; newColumn < lineLength;
                             ++newColumn, ch = charIterator.next()) {
                            final int charVisualLength = charVisualLength(ch);
                            if (charVisualLength < 0) {
                                break;
                            }

                            final int charWidth = textMetrics.charWidth(ch);
                            if (charWidth < 0) {
                                break;
                            }

                            final int charRightX = newX + charWidth;
                            if (charRightX > x) {
                                // The intersection is found.
                                isIntersectionFound = true;
                                break;
                            }

                            newVisualColumn += charVisualLength;
                            newX = charRightX;
                        }

                        if (!isIntersectionFound) {
                            // We have iterated to the end of the line, but has not reached the
                            // desired x-coordinate.
                            final int trailingSpaceCount = (x - newX) / textMetrics.spaceWidth;
                            newColumn += trailingSpaceCount;
                            newVisualColumn += trailingSpaceCount;
                            newX += trailingSpaceCount * textMetrics.spaceWidth;
                        }
                    } else {
                        final int shiftInColumns = shiftInPixels / textMetrics.spaceWidth;
                        newColumn += shiftInColumns;
                        newVisualColumn += shiftInColumns;
                        newX += shiftInColumns * textMetrics.spaceWidth;
                    }
                } else { // Iterate left.
                    boolean isNewPositionFound = false;
                    final int trailingSpaceCount = position.column - lineLength;

                    if (trailingSpaceCount > 0) {
                        final int spaceTailWidth = trailingSpaceCount * textMetrics.spaceWidth;

                        if (spaceTailWidth >= -shiftInPixels) { // Here (shiftInPixels < 0).
                            final int trailingSpaceNewCount =
                                (spaceTailWidth + shiftInPixels) / textMetrics.spaceWidth;
                            final int shiftInColumns =
                                trailingSpaceNewCount - trailingSpaceCount; // Negative value.
                            newColumn += shiftInColumns;
                            newVisualColumn += shiftInColumns;
                            newX += shiftInColumns * textMetrics.spaceWidth;
                            isNewPositionFound = true;
                        } else {
                            newColumn -= trailingSpaceCount;
                            newVisualColumn -= trailingSpaceCount;
                            newX -= spaceTailWidth;
                        }
                    }

                    if (!isNewPositionFound) {
                        --newColumn;
                        final CharacterIterator charIterator = documentCharIterator();
                        char ch = charIterator.setIndex(lineOffset + newColumn);

                        for (; newColumn >= 0; --newColumn, ch = charIterator.previous()) {
                            newVisualColumn -= charVisualLength(ch);
                            newX -= textMetrics.charWidth(ch);
                            if (newX <= x) {
                                // The intersection is found.
                                break;
                            }
                        }
                    }
                }

                if (newColumn != position.column) {
                    hasMoved = true;
                    position.column = newColumn;
                }
                visualColumn = newVisualColumn;
                visualRect.x = newX;
            }
            else { // Line count is zero.
                final int newColumn = (x >= 0 ? x / textMetrics.spaceWidth : 0);
                if (position.line != 0 || position.column != newColumn) {
                    hasMoved = true;
                    position.line = 0;
                    position.column = newColumn;
                    visualColumn = newColumn;
                    visualRect.x = newColumn * textMetrics.spaceWidth;
                }
            }

            if (hasMoved) {
                fireCaretMoved();
                positionTime = System.currentTimeMillis();
            }
        }

        private void stepUp() {
            if (position.line > 0) {
                if (visualColumn < 0) {
                    computeVisualColumn();
                }
                --position.line;
                visualRect.y -= textMetrics.lineHeight;
                computeLogicalColumnAndXByVisualColumn();
                fireCaretMoved();
                positionTime = System.currentTimeMillis();
            }
        }

        private void stepDown() {
            if (position.line < document.lineCount() - 1) {
                if (visualColumn < 0) {
                    computeVisualColumn();
                }
                ++position.line;
                visualRect.y += textMetrics.lineHeight;
                computeLogicalColumnAndXByVisualColumn();
                fireCaretMoved();
                positionTime = System.currentTimeMillis();
            }
        }

        private void moveVertically(int newLine) {
            if (newLine != position.line) {
                final int lineCount = document.lineCount();
                if (newLine >= lineCount) {
                    newLine = lineCount - 1;
                }
                if (newLine < 0) {
                    newLine = 0;
                }

                if (visualColumn < 0) {
                    computeVisualColumn();
                }
                position.line = newLine;
                computeY();
                computeLogicalColumnAndXByVisualColumn();
                fireCaretMoved();
                positionTime = System.currentTimeMillis();
            }
        }

        private boolean stepLeft() {
            if (position.column > 0) {
                --position.column;

                if (areVisualColumnAndXValid()) {
                    if (position.line < document.lineCount()) {
                        final Segment lineSegment = documentLine(position.line);
                        if (position.column < lineSegment.length) {
                            final char currChar =
                                document.charAt(lineSegment.offset + position.column);
                            visualColumn -= charVisualLength(currChar);
                            visualRect.x -= textMetrics.charWidth(currChar);
                        } else {
                            // Move one space left.
                            --visualColumn;
                            visualRect.x -= textMetrics.spaceWidth;
                        }
                    } else {
                        // Move one space left.
                        --visualColumn;
                        visualRect.x -= textMetrics.spaceWidth;
                    }
                }

                fireCaretMoved();
                positionTime = System.currentTimeMillis();
                return true;
            }

            return false;
        }

        private void stepRight() {
            if (areVisualColumnAndXValid()) {
                if (position.line < document.lineCount()) {
                    final Segment lineSegment = documentLine(position.line);
                    if (position.column < lineSegment.length) {
                        final char currChar = document.charAt(lineSegment.offset + position.column);
                        visualColumn += charVisualLength(currChar);
                        visualRect.x += textMetrics.charWidth(currChar);
                    } else {
                        // Move one space right.
                        ++visualColumn;
                        visualRect.x += textMetrics.spaceWidth;
                    }
                } else {
                    // Move one space right.
                    ++visualColumn;
                    visualRect.x += textMetrics.spaceWidth;
                }
            }

            ++position.column;
            fireCaretMoved();
            positionTime = System.currentTimeMillis();
        }

        private boolean moveOneWordLeft() {
            boolean result = false;

            if (position.line < document.lineCount()) {
                final Segment lineSegment = documentLine(position.line);
                final int lineOffset = lineSegment.offset;
                final int lineLength = lineSegment.length;

                if (position.column > 0) {
                    final CharacterIterator docIterator = documentCharIterator();
                    int newColumn = Math.min(position.column, lineLength);
                    char ch = docIterator.setIndex(lineOffset + newColumn - 1);

                    if (Character.isWhitespace(ch)) {
                        // Move to the first non-whitespace character.
                        do {
                            --newColumn;
                            ch = docIterator.previous();
                        } while (newColumn > 0 &&
                            ch != CharacterIterator.DONE && Character.isWhitespace(ch));
                    }

                    if (isWordChar(ch)) {
                        // Go to the start of this word.
                        do {
                            --newColumn;
                            ch = docIterator.previous();
                        } while (newColumn > 0 &&
                                 ch != CharacterIterator.DONE && isWordChar(ch));
                    } else {
                        // Move to the first word-character or whitespace.
                        do {
                            --newColumn;
                            ch = docIterator.previous();
                        } while (newColumn > 0 &&
                                 ch != CharacterIterator.DONE &&
                                 !isWordChar(ch) && !Character.isWhitespace(ch));
                    }

                    position.column = newColumn;
                    invalidateVisualColumnAndX();
                    fireCaretMoved();
                    positionTime = System.currentTimeMillis();
                    result = true;
                }
            } else {
                result = moveOneSpaceLeft();
            }

            return result;
        }

        private void moveOneWordRight() {
            if (position.line < document.lineCount()) {
                final Segment lineSegment = documentLine(position.line);
                final int lineOffset = lineSegment.offset;
                final int lineLength = lineSegment.length;

                if (position.column < lineLength) {
                    final CharacterIterator docIterator = documentCharIterator();
                    int newColumn = position.column;
                    char ch = docIterator.setIndex(lineOffset + newColumn);

                    if (Character.isWhitespace(ch)) {
                        // Move to the first non-whitespace character.
                        do {
                             ++newColumn;
                            ch = docIterator.next();
                        } while (newColumn < lineLength &&
                                 ch != CharacterIterator.DONE && Character.isWhitespace(ch));
                    } else if (isWordChar(ch)) {
                        // Go to the end of this word.
                        do {
                            ++newColumn;
                            ch = docIterator.next();
                        } while (newColumn < lineLength &&
                                 ch != CharacterIterator.DONE && isWordChar(ch));

                        // And skip the whitespaces (if any) following it.
                        while (newColumn < lineLength && ch != CharacterIterator.DONE &&
                               Character.isWhitespace(ch)) {
                            ++newColumn;
                            ch = docIterator.next();
                        }
                    } else {
                        // Move to the first word-character.
                        do {
                            ++newColumn;
                            ch = docIterator.next();
                        } while (newColumn < lineLength &&
                                 ch != CharacterIterator.DONE && !isWordChar(ch));
                    }

                    position.column = newColumn;
                    invalidateVisualColumnAndX();
                    fireCaretMoved();
                    positionTime = System.currentTimeMillis();
                } else {
                    moveOneSpaceRight();
                }
            } else {
                moveOneSpaceRight();
            }
        }

        private void updateWithNewTextMetrics() {
            computeYAndHeight();
            invalidateVisualColumnAndX();
        }

        private void computeVisualColumn() {
            if (document.lineCount() > 0) {
                final Segment lineSegment = documentLine(position.line);
                final int lineLength = lineSegment.length;
                final int trailingSpaceCount = Math.max(position.column - lineLength, 0);
                visualColumn = textVisualLength(documentCharIterator(), lineSegment.offset,
                                                trailingSpaceCount > 0 ? lineLength
                                                                       : position.column);
                if (visualColumn < 0) {
                    visualColumn = 0;
                }

                visualColumn += trailingSpaceCount;
            } else {
                visualColumn = position.column;
            }
        }

        private void computeVisualColumnAndX() {
            if (document.lineCount() > 0) {
                final Segment lineSegment = documentLine(position.line);
                final int lineLength = lineSegment.length;
                final int trailingSpaceCount = Math.max(position.column - lineLength, 0);

                textVisualLengthAndWidth(documentCharIterator(), lineSegment.offset,
                                         trailingSpaceCount > 0 ? lineLength : position.column,
                                         visualLengthAndWidth);
                visualColumn = Math.max(visualLengthAndWidth[0], 0);
                visualRect.x = Math.max(visualLengthAndWidth[1], 0);

                visualColumn += trailingSpaceCount;
                visualRect.x += trailingSpaceCount * textMetrics.spaceWidth;
            } else {
                visualColumn = position.column;
                visualRect.x = position.column * textMetrics.spaceWidth;
            }
        }

        private boolean isVisualColumnValid() {
            return visualColumn >= 0;
        }

        private boolean isXValid() {
            return visualRect.x >= 0;
        }

        private boolean areVisualColumnAndXValid() {
            return (visualColumn >= 0 && visualRect.x >= 0);
        }

        private void invalidateVisualColumnAndX() {
            visualColumn = -1;
            visualRect.x = -1;
        }

        private void computeLogicalColumnAndXByVisualColumn() {
            if (document.lineCount() > 0) {
                final Segment lineSegment = documentLine(position.line);
                final CharacterIterator docIterator = documentCharIterator();
                position.column = 0;
                visualRect.x = 0;
                int visualColumnSoFar = 0;
                boolean isFound = false;

                for (char ch = docIterator.setIndex(lineSegment.offset);
                     position.column < lineSegment.length;
                     ++position.column, ch = docIterator.next()) {
                    if (visualColumnSoFar >= visualColumn) {
                        isFound = true;
                        break;
                    }
                    final int charVisualLength = charVisualLength(ch);
                    if (charVisualLength < 0) {
                        isFound = true;
                        break;
                    }
                    final int newVisualColumnSoFar = visualColumnSoFar + charVisualLength;
                    if (newVisualColumnSoFar > visualColumn) {
                        isFound = true;
                        break;
                    }
                    final int charWidth = textMetrics.charWidth(ch);
                    if (charWidth < 0) {
                        isFound = true;
                        break;
                    }

                    visualColumnSoFar = newVisualColumnSoFar;
                    visualRect.x += charWidth;
                }

                if (isFound) {
                    visualColumn = visualColumnSoFar;
                } else {
                    final int trailingSpaceCount = visualColumn - visualColumnSoFar;
                    if (trailingSpaceCount > 0) {
                        position.column += trailingSpaceCount;
                        visualRect.x += trailingSpaceCount * textMetrics.spaceWidth;
                    }
                }
            } else {
                position.line = 0;
                position.column = visualColumn;
                visualRect.x = position.column * textMetrics.spaceWidth;
            }
        }

        private void computeY() {
            visualRect.y = position.line * textMetrics.lineHeight + textMetrics.descent;
        }

        private void computeYAndHeight() {
            computeY();
            visualRect.height = textMetrics.lineHeight;
        }

        /**
         * Paints the caret.
         *
         * <p>Fills the caret's visual rectangle with color currently set on the {@code graphics}
         * object, see {@link Graphics#setColor(java.awt.Color)}.</p>
         *
         * @param graphics graphic object the caret is to be painted upon.
         */
        private void paint(Graphics graphics) {
            final int rightX  = visualRect.x + visualRect.width;
            final int topY    = visualRect.y;
            final int bottomY = topY + visualRect.height - 1;

            for (int x = visualRect.x; x < rightX; ++x) {
                graphics.drawLine(x, topY, x, bottomY);
            }
        }

        private boolean moveOneSpaceLeft() {
            if (position.column > 0) {
                --position.column;

                if (isVisualColumnValid()) {
                    --visualColumn;
                }
                if (isXValid()) {
                    visualRect.x -= textMetrics.spaceWidth;
                }

                fireCaretMoved();
                positionTime = System.currentTimeMillis();
                return true;
            }

            return false;
        }

        private void moveOneSpaceRight() {
            ++position.column;

            if (isVisualColumnValid()) {
                ++visualColumn;
            }
            if (isXValid()) {
                visualRect.x += textMetrics.spaceWidth;
            }

            fireCaretMoved();
            positionTime = System.currentTimeMillis();
        }

        private final class Blinker implements Runnable {
            private Future<?> futureBlink = null;
            private long blinkPeriod = 500;

            @Override
            public void run() {
                final long currTime = System.currentTimeMillis();
                boolean shallRepaint;
                if (currTime - positionTime >= blinkPeriod) {
                    isVisible = !isVisible;
                    shallRepaint = true;
                } else {
                    // The caret got moved recently. Let's make it visible.
                    shallRepaint = !isVisible;
                    isVisible = true;
                }

                if (shallRepaint) {
                    uiCommandExecutor.execute(repainter);
                }
            }

            void start(long blinkPeriod) {
                if (blinkPeriod < 10) {
                    blinkPeriod = 10;
                }

                this.blinkPeriod = blinkPeriod;

                if (futureBlink != null) {
                    futureBlink.cancel(false);
                }

                futureBlink = SCHEDULER.scheduleAtFixedRate(this,
                                                            blinkPeriod, blinkPeriod,
                                                            TimeUnit.MILLISECONDS);
            }
        }

        private final class Repainter implements Runnable {
            @Override
            public void run() {
                if (visualRect.x < 0) {
                    computeVisualColumnAndX();
                }
                editorComponent.repaint(visualRect);
            }
        }
    }

    private final class Selection {
        private final TextPosition start = new TextPosition();
        private final TextPosition end = new TextPosition();
        private final TextPosition stickyPoint = new TextPosition();
        private boolean isValid = false;
        private final Segment region = new Segment();

        private void computeRegion() {
            region.offset = document.lineOffset(start.line) + selection.start.column;
            region.length = end.line == start.line ? end.column - start.column
                                                   : document.lineOffset(selection.end.line) +
                                                     end.column - region.offset;
        }
    }

    private static final class MouseState {
        private boolean isLeftButtonDown = false;
        private boolean hasLeftButtonStateChanged = false;
    }

    private static final class DocumentChangeData {
        private boolean isChanging = false;
        private int offset = 0;
        private int length = 0;
        private int newLength = 0;
        private int caretOffset = Integer.MIN_VALUE;
        private int firstChangedLine = 0;
        private int lineCount = 0;
    }

    private final class LineIterator implements SegmentIterator {
        private int lineIndex;

        @Override
        public boolean hasNext() {
            return (lineIndex < document.lineCount());
        }

        @Override
        public Segment next() {
            final Segment line = new Segment();

            if (next(line)) {
                return line;
            }

            throw new NoSuchElementException();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean next(Segment target) {
            if (lineIndex < document.lineCount()) {
                target.assign(documentLine(lineIndex));
                ++lineIndex;
                return true;
            }

            return false;
        }

        private LineIterator(int lineIndex) {
            reset(lineIndex);
        }

        private LineIterator reset(int lineIndex) {
            this.lineIndex = lineIndex;
            return this;
        }
    }

    private static final class CharBuffer {
        private char[] content;
        private int size;
        private int tabCount = 0;

        private CharBuffer(int initialCapacity) {
            content = new char[initialCapacity];
            size = 0;
            tabCount = 0;
        }

        private void append(char ch) {
            if (size == content.length) {
                int newLength;
                if (size <= 0x10) {
                    newLength = size + 4;
                } else {
                    newLength = size * 5;
                    newLength >>>= 2;
                }
                content = Arrays.copyOf(content, newLength);
            }

            content[size++] = ch;
            if (ch == Characters.CHARACTER_TABULATION) {
                ++tabCount;
            }
        }

        private void clear() {
            size = 0;
            tabCount = 0;
        }
    }

    private static final class IntBuffer {
        private int[] content;
        private int size;

        private IntBuffer(int initialCapacity) {
            content = new int[initialCapacity];
            size = 0;
        }

        private void append(int i) {
            if (size == content.length) {
                int newLength;
                if (size <= 0x10) {
                    newLength = size + 4;
                } else {
                    newLength = size * 5;
                    newLength >>>= 2;
                }
                content = Arrays.copyOf(content, newLength);
            }

            content[size++] = i;
        }

        private void clear() {
            size = 0;
        }
    }

    private static final class ThreadFactoryImpl implements ThreadFactory  {
        @Override
        public Thread newThread(Runnable r) {
            final Thread thread = new Thread(r, "My scheduling thread");
            thread.setDaemon(true);
            thread.setPriority(Thread.NORM_PRIORITY);
            return thread;
        }
    }
}