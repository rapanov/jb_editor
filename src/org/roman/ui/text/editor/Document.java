/**
 * Author: Roman Panov
 * Date:   11/16/12
 */

package org.roman.ui.text.editor;

import java.text.CharacterIterator;
import org.roman.util.Segment;
import org.roman.util.SegmentIterator;
import org.roman.util.text.IterableCharSequence;

public interface Document extends IterableCharSequence {

    int lineCount();

    /**
     * Gets line index by offset.
     *
     * @param offset offset (in chars) from the beginning of this document.
     *
     * @return Index of line (beginning from 0) that contains character with the specified offset.
     */
    int lineIndex(int offset);

    int lineOffset(int lineIndex);
    int lineLength(int lineIndex);
    void line(int lineIndex, Segment target);
    int lineByOffset(int offset, Segment target);

    CharacterIterator iterator(CharacterIterator target);

    // These is optional operations.
    SegmentIterator lineIterator(int lineIndex, SegmentIterator target);

    void readLock();
    void readUnlock();
    void writeLock();
    void writeUnlock();

    void insertChar(int offset, char ch);
    void deleteChar(int offset);
    void changeText(int offset, int length, CharacterIterator newText);

    boolean addListener(TextListener listener);
    boolean removeListener(TextListener listener);
}