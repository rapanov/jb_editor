/**
 * Author: Roman Panov
 * Date:   11/16/12
 */

package org.roman.ui.text.editor;

import java.io.Reader;
import org.roman.util.IntegerMapper;
import org.roman.util.text.lexer.LexerException;
import org.roman.util.text.lexer.ResettableLexerFactory;

public final class EditorFactory {

    public static <P> TextHighlighter<P>
        createTextHighlighter(Document text,
                              ResettableLexerFactory<Reader> lexerFactory,
                              IntegerMapper<P> tokenTypeMapper) throws LexerException {
        return new TextHighlighterImpl<P>(text, lexerFactory, tokenTypeMapper);
    }
}
