/**
 * Author: Roman Panov
 * Date:   2/17/13
 */

package org.roman.ui.text.editor;

import java.io.Reader;
import java.text.CharacterIterator;
import java.util.NoSuchElementException;
import org.roman.util.DataSegment;
import org.roman.util.DataSegmentIterator;
import org.roman.util.IntegerMapper;
import org.roman.util.OffsetList;
import org.roman.util.text.IterableCharSequence;
import org.roman.util.text.TextReader;
import org.roman.util.text.lexer.Lexer;
import org.roman.util.text.lexer.LexerException;
import org.roman.util.text.lexer.ResettableLexer;
import org.roman.util.text.lexer.ResettableLexerFactory;

final class TextHighlighterImpl<P> implements TextHighlighter<P> {
    private final IterableCharSequence text;
    private CharacterIterator textIterator;
    private final TextReader textReader;
    private final ResettableLexer<Reader> lexer;
    private final IntegerMapper<P> tokenTypeMapper;
    private final OffsetList<Token<P>> tokenList = new OffsetList<Token<P>>();
    private final OffsetList.EntryEx<Token<P>> tokenEntryEx = new OffsetList.EntryEx<Token<P>>();
    private final TextChangeData textChangeData = new TextChangeData();

    @Override
    public void textChanging(int offset, int length, int newLength) {
        if (textChangeData.isChanging) {
            throw new IllegalStateException();
        }

        textChangeData.isChanging = true;
        textChangeData.offset = offset;
        textChangeData.length = length;
        textChangeData.newLength = newLength;
    }

    @Override
    public void textChanged() {
        if (!textChangeData.isChanging) {
            throw new IllegalStateException();
        }

        textChangeData.isChanging = false;
        final int shift = textChangeData.newLength - textChangeData.length;
        textIterator = text.iterator(textIterator);
        final int changedAreaStart = textChangeData.offset;
        final int changedAreaEnd = changedAreaStart + textChangeData.length;
        int rescannedAreaStart;
        tokenList.lowerBound(changedAreaStart, tokenEntryEx);

        if (tokenEntryEx.entry == null) {
            tokenList.first(tokenEntryEx);
            rescannedAreaStart = textIterator.getBeginIndex();
        } else {
            if (tokenEntryEx.offset == changedAreaStart) {
                final OffsetList.Entry<Token<P>> tokenEntry = tokenEntryEx.entry;
                final int tokenOffset = tokenEntryEx.offset;
                tokenEntryEx.entry.previous(tokenEntryEx);

                if (tokenEntryEx.entry == null) {
                    rescannedAreaStart = textIterator.getBeginIndex();
                    tokenEntryEx.entry = tokenEntry;
                    tokenEntryEx.offset = tokenOffset;
                } else {
                    tokenEntryEx.offset += tokenOffset;
                    final int firstImpactedTokenStart = tokenEntryEx.offset;
                    final int firstImpactedTokenEnd =
                        firstImpactedTokenStart + tokenEntryEx.entry.value().length;
                    if (firstImpactedTokenEnd < changedAreaStart) {
                        // This token has ended before the changed area.
                        rescannedAreaStart = firstImpactedTokenEnd;
                        tokenEntryEx.entry = tokenEntry;
                        tokenEntryEx.offset = tokenOffset;
                    } else {
                        rescannedAreaStart = firstImpactedTokenStart;
                    }
                }


            } else {
                final int firstImpactedTokenStart = tokenEntryEx.offset;
                final int firstImpactedTokenEnd =
                    firstImpactedTokenStart + tokenEntryEx.entry.value().length;
                if (firstImpactedTokenEnd < changedAreaStart) {
                    // This token has ended before the changed area.
                    rescannedAreaStart = firstImpactedTokenEnd;

                    // We need to retain this, as next() will provide us with the relative
                    // (not absolute) offset.
                    final int offsetBase = tokenEntryEx.offset;
                    tokenEntryEx.entry.next(tokenEntryEx);
                    tokenEntryEx.offset += offsetBase;
                } else {
                    rescannedAreaStart = firstImpactedTokenStart;
                }
            }
        }

        // We have to remove all tokens that start before 'changedAreaEnd'.
        while (tokenEntryEx.entry != null) {
            final int tokenOffset = tokenEntryEx.offset;
            if (tokenOffset >= changedAreaEnd) {
                break;
            }

            final OffsetList.Entry<Token<P>> tokenEntry = tokenEntryEx.entry;
            tokenEntry.next(tokenEntryEx);
            // 'tokenEntryEx.offset' is relative, so we have to add the previous token's offset
            // to it.
            tokenEntryEx.offset += tokenOffset;
            tokenList.remove(tokenEntry,  false);
        }

        // The following tokens are not removed, but still need to be shifted.
        if (shift != 0) {
            if (tokenEntryEx.entry != null) {
                tokenEntryEx.entry.shiftAllFromThis(shift);
                tokenEntryEx.offset += shift;
            }
        }

        textIterator.setIndex(rescannedAreaStart);

        try {
            lexer.reset(textReader.reset(textIterator));
            int tokenType;

            while ((tokenType = lexer.nextToken()) != Lexer.TOKEN_TYPE_EOF) {
                final int newTokenOffset = lexer.tokenOffset() + rescannedAreaStart;
                final int newTokenLength = lexer.tokenLength();
                final int newTokenEnd = newTokenOffset + newTokenLength;
                int removedTokenEnd = Integer.MAX_VALUE;

                // We gotta remove all the existing tokens that start before 'tokenEnd'.
                while (tokenEntryEx.entry != null) {
                    final int removedTokenOffset = tokenEntryEx.offset;
                    if (removedTokenOffset >= newTokenEnd) {
                        // No need to remove this token
                        break;
                    }

                    final OffsetList.Entry<Token<P>> removedTokenEntry = tokenEntryEx.entry;
                    removedTokenEnd = removedTokenOffset + removedTokenEntry.value().length;
                    removedTokenEntry.next(tokenEntryEx);
                    tokenEntryEx.offset += removedTokenOffset;
                    tokenList.remove(removedTokenEntry,  false);
                }

                tokenList.put(newTokenOffset,
                              new Token<P>(newTokenLength, tokenTypeMapper.map(tokenType)));

                if (removedTokenEnd <= newTokenEnd) {
                    // Here we can stop scanning, as the new token has completely covered all the
                    // existing tokens that might need to be removed.
                    break;
                }
            }
        } catch (LexerException lexerException) {
            throw new RuntimeException(lexerException);
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    public DataSegmentIterator<P> iterator(int offset, DataSegmentIterator<P> target) {
        tokenList.lowerBound(offset, tokenEntryEx);
        final OffsetList.Entry<Token<P>> tokenEntry = tokenEntryEx.entry;

        if (tokenEntry == null) {
            tokenList.first(tokenEntryEx);
        } else {
            final Token<P> token = tokenEntry.value();
            if (tokenEntryEx.offset + token.length <= offset) {
                // This token does not intersect the offset line, let's take the next one.
                // We need to retain this, as next() will provide us with the relative
                // (not absolute) offset.
                final int offsetBase = tokenEntryEx.offset;
                tokenEntry.next(tokenEntryEx);

                if (tokenEntryEx.entry != null) {
                    tokenEntryEx.offset += offsetBase;
                }
            }
        }

        return target instanceof TokenIterator ? ((TokenIterator)target).reset(tokenEntryEx)
                                               : new TokenIterator<P>(tokenEntryEx);
    }

    TextHighlighterImpl(IterableCharSequence text,
                        ResettableLexerFactory<Reader> lexerFactory,
                        IntegerMapper<P> tokenTypeMapper) throws LexerException {
        this.text = text;
        textIterator = text.iterator(null);
        textIterator.first();
        textReader = new TextReader(textIterator);
        lexer = lexerFactory.createLexer(textReader);
        this.tokenTypeMapper = tokenTypeMapper;

        int tokenType;
        while ((tokenType = lexer.nextToken()) != Lexer.TOKEN_TYPE_EOF) {
            final int tokenLength = lexer.tokenLength();
            final Token<P> token = new Token<P>(tokenLength,
                                                tokenTypeMapper.map(tokenType));
            tokenList.put(lexer.tokenOffset(), token, tokenLength);
        }
    }

    private static final class Token<P> {
        final int length;
        final P properties;

        @Override
        public String toString() {
            final StringBuilder strBld = new StringBuilder("Token of length \'");
            strBld.append(length).append("\' with properties \'").append(properties).append('\'');
            return strBld.toString();
        }

        Token(int length, P properties) {
            this.length = length;
            this.properties = properties;
        }
    }

    private static final class TokenIterator<P> implements DataSegmentIterator<P> {
        private final OffsetList.EntryEx<Token<P>> tokenEntryEx;
        boolean hasStarted = false;

        @Override
        public boolean hasNext() {
            return
                (tokenEntryEx.entry != null && (!hasStarted || tokenEntryEx.entry.next() != null));
        }

        @Override
        public DataSegment<P> next() {
            if (tokenEntryEx.entry == null) {
                throw new NoSuchElementException();
            }

            if (hasStarted) {
                // We need to retain this, as next() will provide us with the relative
                // (not absolute) offset.
                final int offsetBase = tokenEntryEx.offset;
                tokenEntryEx.entry.next(tokenEntryEx);

                if (tokenEntryEx.entry == null) {
                    throw new NoSuchElementException();
                }

                tokenEntryEx.offset += offsetBase;
            } else {
                hasStarted = true;
            }

            final Token<P> token = tokenEntryEx.entry.value();
            return new DataSegment<P>(tokenEntryEx.offset, token.length, token.properties);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean next(DataSegment<P> target) {
            if (tokenEntryEx.entry == null) {
                return false;
            }

            if (hasStarted) {
                // We need to retain this, as next() will provide us with the relative
                // (not absolute) offset.
                final int offsetBase = tokenEntryEx.offset;
                tokenEntryEx.entry.next(tokenEntryEx);

                if (tokenEntryEx.entry == null) {
                    return false;
                }

                tokenEntryEx.offset += offsetBase;
            } else {
                hasStarted = true;
            }

            final Token<P> token = tokenEntryEx.entry.value();
            target.offset = tokenEntryEx.offset;
            target.length = token.length;
            target.data = token.properties;
            return true;
        }

        private TokenIterator(OffsetList.EntryEx<Token<P>> tokenEntryEx) {
            this.tokenEntryEx = new OffsetList.EntryEx<Token<P>>();
            reset(tokenEntryEx);
        }

        private DataSegmentIterator reset(OffsetList.EntryEx<Token<P>> tokenEntryEx) {
            this.tokenEntryEx.assign(tokenEntryEx);
            hasStarted = false;
            return this;
        }
    }

    private static final class TextChangeData {
        private boolean isChanging = false;
        private int offset = 0;
        private int length = 0;
        private int newLength = 0;
    }
}
