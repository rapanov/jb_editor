/**
 * Author: Roman Panov
 * Date:   12/13/12
 */

package org.roman.ui.text.editor;

import java.awt.AWTEvent;
import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.concurrent.Executor;
import javax.swing.JComponent;
import javax.swing.JViewport;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public final class EditorComponent extends JComponent implements Scrollable {
    private static final int DEFAULT_TAB_SIZE = 4;

    private final EditorCore editorCore;
    private final TextHighlighter<Color> textTinter;
    private final Set<ChangeListener> changeListeners = new HashSet<ChangeListener>();
    private final Set<CaretListener> caretListeners = new HashSet<CaretListener>();

    public EditorComponent(Document document,
                           Font font,
                           TextHighlighter<Color> textTinter) {
        this(document, font, DEFAULT_TAB_SIZE, textTinter);
    }

    public EditorComponent(Document document,
                           Font font,
                           int tabSize,
                           TextHighlighter<Color> textTinter) {
        enableEvents(AWTEvent.KEY_EVENT_MASK);
        //enableInputMethods(true);
        //setFocusCycleRoot(true);
        setOpaque(true);
        setBackground(Color.WHITE);
        super.setFont(font);

        final Executor uiCommandExecutor = new Executor() {
            @Override
            public void execute(Runnable command) {
                SwingUtilities.invokeLater(command);
            }
        };
        editorCore = new EditorCore(document, tabSize, textTinter, this,
                                    uiCommandExecutor, new CoreListener(),
                                    getToolkit().getSystemClipboard());
        document.addListener(new DocumentListener());
        this.textTinter = textTinter;
        registerComponentListeners();
        setCursor(new Cursor(Cursor.TEXT_CURSOR));

        @SuppressWarnings("unchecked")
        final Set<AWTKeyStroke> emptySet = Collections.EMPTY_SET;
        setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, emptySet);
        setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, emptySet);
        setFocusTraversalKeys(KeyboardFocusManager.UP_CYCLE_TRAVERSAL_KEYS, emptySet);
        setFocusTraversalKeys(KeyboardFocusManager.DOWN_CYCLE_TRAVERSAL_KEYS, emptySet);
    }

    public Document getDocument() {
        return editorCore.document();
    }

    public TextHighlighter<Color> getTextTinter() {
        return textTinter;
    }

    @Override
    public Dimension getPreferredSize() {
        return isPreferredSizeSet() ? super.getPreferredSize()
                                    : new Dimension(editorCore.preferredWidth(),
                                                    editorCore.preferredHeight());
    }

    public int getTabSize() {
        return editorCore.tabSize();
    }

    public boolean getOverwriteMode() {
        return editorCore.isOverwriteMOdeOn();
    }

    public void computeCaretPosition(TextPosition target) {
        editorCore.caretPosition(target);
    }

    public TextPosition getCaretPosition() {
        final TextPosition caretPosition = new TextPosition();
        editorCore.caretPosition(caretPosition);
        return caretPosition;
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle visibleRect,
                                           int orientation, int direction) {
        switch (orientation) {
            case SwingConstants.HORIZONTAL:
                return visibleRect.width;

            case SwingConstants.VERTICAL:
                final int lineHeight = editorCore.lineHeight();
                if (direction > 0) {
                    final int lineNumber =
                        (visibleRect.y + visibleRect.height) / lineHeight;
                    return (lineNumber * lineHeight - visibleRect.y);
                } else if (direction < 0) {
                    final int lineNumber =
                        (visibleRect.y - visibleRect.height) / lineHeight;
                    return (visibleRect.y - lineNumber * lineHeight);
                } else {
                    throw new IllegalArgumentException();
                }

            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        final Component parent = getParent();
        return (parent instanceof JViewport && parent.getWidth() > editorCore.preferredWidth());
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        final Component parent = getParent();
        return (parent instanceof JViewport && parent.getHeight() > editorCore.preferredHeight());
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect,
                                          int orientation, int direction) {
        switch (orientation) {
            case SwingConstants.HORIZONTAL:
                return editorCore.columnWidth();
            case SwingConstants.VERTICAL:
                return editorCore.lineHeight();
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public Color getBackground() {
        return editorCore.backgroundColor();
    }

    @Override
    public void setBackground(Color color) {
        // Do nothing here, the background color is forced to be white.
    }

    @Override
    public void setFont(Font font) {
        if (font == null) {
            throw new NullPointerException();
        }

        super.setFont(font);
        editorCore.setFontMetrics(getFontMetrics(font));

        if (isVisible()) {
            repaint();
        }
    }

    public void setTabSize(int tabSize) {
        if (editorCore.setTabSize(tabSize)) {
            if (isVisible()) {
                repaint();
            }
        }
    }

    public void addChangeListener(ChangeListener listener) {
        changeListeners.add(listener);
    }

    public void addCaretListener(CaretListener listener) {
        caretListeners.add(listener);
    }

    public void removeChangeListener(ChangeListener listener) {
        changeListeners.remove(listener);
    }

    public void removeCaretListener(CaretListener listener) {
        caretListeners.remove(listener);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        final Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                                    RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        editorCore.paint(graphics);
    }

    private void registerComponentListeners() {
        addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent event) {
                editorCore.processFocusGained(event);
            }

            @Override
            public void focusLost(FocusEvent event) {
                editorCore.processFocusLost(event);
            }
        });

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent event) {
                editorCore.processKeyPressed(event);
            }

            @Override
            public void keyTyped(KeyEvent event) {
                editorCore.processKeyTyped(event);
            }
        });

        final MouseAdapter mouseListener = new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                editorCore.processMousePressed(event);
            }

            @Override
            public void mouseReleased(MouseEvent event) {
                editorCore.processMouseReleased(event);
            }

            @Override
            public void mouseClicked(MouseEvent event) {
                editorCore.processMouseClicked(event);
            }

            @Override
            public void mouseMoved(MouseEvent event) {
                editorCore.processMouseMoved(event);
            }

            @Override
            public void mouseDragged(MouseEvent event) {
                editorCore.processMouseDragged(event);
            }
        };
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);
    }

    private final class DocumentListener implements TextListener {
        private final Runnable documentChangedInvoker;

        @Override
        public void textChanging(final int offset, final int length, final int newLength) {
            if (SwingUtilities.isEventDispatchThread()) {
                textTinter.textChanging(offset, length, newLength);
                editorCore.processDocumentChanging(offset, length, newLength);
            } else {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        textTinter.textChanging(offset, length, newLength);
                        editorCore.processDocumentChanging(offset, length, newLength);
                    }
                });
            }
        }

        @Override
        public void textChanged() {
            if (SwingUtilities.isEventDispatchThread()) {
                textTinter.textChanged();
                editorCore.processDocumentChanged();
            } else {
                SwingUtilities.invokeLater(documentChangedInvoker);
            }
        }

        private DocumentListener() {
            documentChangedInvoker = new Runnable() {
                @Override
                public void run() {
                    textTinter.textChanged();
                    editorCore.processDocumentChanged();
                }
            };
        }
    }

    private final class CoreListener implements EditorCore.Listener {
        @Override
        public void preferredSizeChanged(int newWidth, int newHeight) {
            final Object parent = getParent();
            if (parent instanceof JViewport) {
                ((JViewport)parent).setViewSize(new Dimension(newWidth, newHeight));
            }
        }

        @Override
        public void overwriteModeChanged(boolean isOverwriteModeOn) {
            for (final ChangeListener listener: changeListeners) {
                if (listener != null) {
                    listener.stateChanged(new ChangeEvent(EditorComponent.this));
                }
            }
        }

        @Override
        public void caretMoved(int newLine, int newColumn) {
            for (final CaretListener listener: caretListeners) {
                if (listener != null) {
                    listener.caretMoved(new CaretEvent(EditorComponent.this, newLine, newColumn));
                }
            }
        }
    }
}
