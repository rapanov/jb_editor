/**
 * Author: Roman Panov
 * Date:   12/8/12
 */

package org.roman.ui.font;

import java.awt.Font;
import java.awt.FontMetrics;

public interface FontMetricsProvider {
    FontMetrics getFontMetrics(Font font);
}
