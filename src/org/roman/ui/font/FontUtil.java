/**
 * Author: Roman Panov
 * Date:   12/8/12
 */

package org.roman.ui.font;

import java.awt.Font;
import java.awt.FontMetrics;

public class FontUtil {
    private static final int DEFAULT_FONT_SIZE = 12;

    public static boolean isFontMonospaced(String fontName, FontMetricsProvider metricsProvider) {
        final Font plainFont = new Font(fontName, Font.PLAIN, DEFAULT_FONT_SIZE);
        final Font boldFont = plainFont.deriveFont(Font.BOLD);
        return isMonospaced(plainFont, boldFont, metricsProvider);
    }

    public static boolean isFontMonospaced(Font font, FontMetricsProvider metricsProvider) {
        Font plainFont = null;
        Font boldFont = null;
        final int fontStyle = font.getStyle();

        switch (fontStyle) {
            case Font.PLAIN:
                plainFont = font;
                boldFont = font.deriveFont(Font.BOLD);
                break;

            case Font.BOLD:
                plainFont = font.deriveFont(Font.PLAIN);
                boldFont = font;
                break;

            default:
                plainFont = font.deriveFont(Font.PLAIN);
                boldFont = font.deriveFont(Font.BOLD);
        }

        return isMonospaced(plainFont, boldFont, metricsProvider);
    }

    private static boolean isMonospaced(Font plainFont,
                                 Font boldFont,
                                 FontMetricsProvider metricsProvider) {
        if (plainFont == null || boldFont == null || metricsProvider == null) {
            return false;
        }

        if (plainFont.canDisplay('W') && boldFont.canDisplay('W') &&
            plainFont.canDisplay('l') && boldFont.canDisplay('l')) {
            final FontMetrics plainMetrics = metricsProvider.getFontMetrics(plainFont);
            final FontMetrics boldMetrics = metricsProvider.getFontMetrics(boldFont);

            if (plainMetrics == null || boldMetrics == null) {
                return false;
            }

            final int plainLWidth = plainMetrics.charWidth('l');
            final int boldLWidth = boldMetrics.charWidth('l');
            final int plainWWidth = plainMetrics.charWidth('W');
            final int boldWWidth = boldMetrics.charWidth('W');
            final int plainSpaceWidth = plainMetrics.charWidth(' ');
            final int boldSpaceWidth = boldMetrics.charWidth(' ');

            return (plainLWidth == plainWWidth && plainLWidth == boldLWidth &&
                    plainLWidth == boldWWidth && plainSpaceWidth == boldSpaceWidth);
        }

        return false;
    }
}
