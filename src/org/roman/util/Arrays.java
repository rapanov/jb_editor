/**
 * Author: Roman Panov
 * Date:   11/17/12
 */

package org.roman.util;

import java.lang.reflect.Array;

public final class Arrays {

    public static void validateArraySegment(DataSegment<?> arraySegment) {
        validateArraySegment(arraySegment.data,
                             arraySegment.offset,
                             arraySegment.length);
    }

    public static void validateArraySegment(Object array,
                                            int    segmentOffsetInArray,
                                            int    segmentLength) {
        if (array == null) {
            throw new NullPointerException();
        }

        validateArraySegment(Array.getLength(array), segmentOffsetInArray, segmentLength);
    }

    public static void validateArraySegment(int arrayLength,
                                            int segmentOffsetInArray,
                                            int segmentLength) {
        if (arrayLength < 0) {
            throw new NegativeArraySizeException();
        }

        if (segmentOffsetInArray < 0) {
            throw new ArrayIndexOutOfBoundsException(segmentOffsetInArray);
        }

        if (segmentLength < 0) {
            throw new ArrayIndexOutOfBoundsException(segmentLength);
        }

        final int end = segmentOffsetInArray + segmentLength;

        if (end > arrayLength) {
            throw new ArrayIndexOutOfBoundsException(end);
        }
    }
}
