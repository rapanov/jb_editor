/**
 * Author: Roman Panov
 * Date:   1/29/13
 */

package org.roman.util;

public interface BinaryTree<V> {
    Node<V> root();

    public static interface Node<V>
    {
        V value();
        Node<V> parent();
        Node<V> leftChild();
        Node<V> rightChild();
    }
}
