/**
 * Author: Roman Panov
 * Date:   2/17/13
 */

package org.roman.util;

public interface IntegerMapper<V> extends Mapper<Integer, V> {
    V map(int key);
}
