/**
 * Author: Roman Panov
 * Date:   1/26/13
 */

package org.roman.util;

/**
 * Maps integer offsets (keys) to values.
 *
 * <p>This data structure is intended to store elements ordered by their offsets, i.e. distances
 * from some fixed reference point. The implementation of this data structure is based upon
 * balanced binary search tree (BST) (red-black tree in this case, but another kind of balanced
 * BST could have been employed). The BST is augmented with the support for elements indices and
 * offsets. The operations add, remove, locate (by index) run in log(n) time. Besides these the
 * shift operation is also supported and runs in log(n) time as well. The Shift operation increments
 * (or decrements if the shift value is negative) offsets of all elements starting from the
 * specified one by the specified value.</p>
 *
 * @param <V> type of elements stored in the list.
 */
public final class OffsetList<V> implements BinaryTree<V> {
    // TODO: Implement interface java.util.Map<Integer, V>.

    // The following scheme is to depict how the data structure is organized and how it implements
    // the shift operation.
    //                                          root
    //                                         ---+---
    //                                            |
    //                                    +--------------+
    //                                    |Node C        |
    //                                    |idx = 2 (+2)  |
    //                                    |off = 30 (+30)|
    //                                    +--------------+
    //                                   /                \
    //                  +--------------+                   +--------------+
    //                  |Node B        |                   |Node E        |
    //                  |idx = 1 (+1)  |                   |idx = 4 (+2)  |
    //                  |off = 20 (+20)|                   |off = 50 (+20)|
    //                  +--------------+                   +--------------+
    //                 /                                  /                \
    // +--------------+                   +--------------+                  +--------------+
    // |Node A        |                   |Node D        |                  |Node F        |
    // |idx = 0 (+0)  |                   |idx = 3 (+1)  |                  |idx = 5 (+1)  |
    // |off = 10 (+10)|                   |off = 40 (+10)|                  |off = 60 (+10)|
    // +--------------+                   +--------------+                  +--------------+
    //
    // Each node stores its relative index and relative offset (shown in parentheses). They are
    // relative to the index and offset of the corresponding base node respectively. Nodes A, B
    // and C do not have base nodes, and store their indices and offsets relative to 0, i.e. they
    // store the absolute values of indices and offsets. For the nodes D and E the base node is C,
    // for the node F the base node is E.
    // Consider we want to shift all nodes starting from B (i.e. nodes B, C, D, E and F) by 2. To
    // attain this we need to increase the values of relative offsets of nodes B and C by 2. Nodes
    // D and E will get shifted automatically, as their offsets are calculated relative to the
    // offset of their base node - node C. Node F has the base node E, and therefore will also get
    // shifted automatically. Thus the shift operation runs in log(n) time at worst.

    private static final byte FLAG_NODE_IS_LEFT_CHILD = 0x01;
    private static final byte FLAG_NODE_IS_RED        = 0x02;

    private Entry<V> root = null;
    private int nodeCount = 0;

    public Node<V> root() {
        return root;
    }

    public int size() {
        return nodeCount;
    }

    public boolean isEmpty() {
        return (nodeCount == 0);
    }

    public Entry<V> first() {
        return Entry.leftMost(root);
    }

    public void first(EntryEx<V> target) {
        final Entry<V> leftMostNode = Entry.leftMost(root);
        target.entry = leftMostNode;
        if (leftMostNode != null) {
            target.index = leftMostNode.index; // Should be 0.
            target.offset = leftMostNode.offset;
        }
    }

    public Entry<V> last() {
        return Entry.rightMost(root);
    }

    public void last(EntryEx<V> target) {
        Entry<V> node = root;
        int index = 0;
        int offset = 0;
        while (node != null) {
            index += node.index;
            offset += node.offset;
            final Entry<V> nextNode = node.leftChild;
            if (nextNode == null) {
                break;
            } else {
                node = nextNode;
            }
        }

        target.entry = node;
        if (node != null) {
            target.index = index;
            target.offset = offset;
        }
    }

    public Entry<V> get(int index) {
        Entry<V> node = root;
        int indexBase = 0;
        int nodeIndex;

        while (node != null) {
            nodeIndex = indexBase + node.index;
            if (index < nodeIndex) {
                node = node.leftChild;
            } else if (index > nodeIndex) {
                indexBase = nodeIndex;
                node = node.rightChild;
            } else {
                // The node is found.
                break;
            }
        }

        return node;
    }

    public void get(int index, EntryEx<V> target) {
        Entry<V> node = root;
        int indexBase = 0;
        int nodeIndex;
        int offsetBase = 0;
        int nodeOffset = 0;

        while (node != null) {
            nodeIndex = indexBase + node.index;
            nodeOffset = offsetBase + node.offset;
            if (index < nodeIndex) {
                node = node.leftChild;
            } else if (index > nodeIndex) {
                indexBase = nodeIndex;
                offsetBase = nodeOffset;
                node = node.rightChild;
            } else {
                // The node is found.
                break;
            }
        }

        target.entry = node;
        if (node != null) {
            target.index = index;
            target.offset = nodeOffset;
        }
    }

    /**
     * Finds the element that is the lower bound for the specified offset.
     *
     * <p>This method is to locate the element with the greatest offset that is less than or equal
     * to the specified {@code offset}.</p>
     *
     * @param offset offset of the interest.
     *
     * @return entry representing the found element or {@code null} if offset of all the
     *         elements in the list are greater than {@code offset}, or the list is empty.
     */
    public Entry<V> lowerBound(int offset) {
        Entry<V> node = root;
        Entry<V> foundNode = null;
        int offsetBase = 0;
        int nodeOffset;

        while (node != null) {
            nodeOffset = offsetBase + node.offset;
            if (offset < nodeOffset) {
                node = node.leftChild;
            } else if (offset > nodeOffset) {
                // At least we found a node whose offset is less than the specified offset.
                foundNode = node;
                offsetBase = nodeOffset;
                node = node.rightChild;
            } else {
                // The node with exactly matching offset is found.
                foundNode = node;
                break;
            }
        }

        return foundNode;
    }

    /**
     * Finds the element that is the lower bound for the specified offset.
     *
     * <p>This method is to locate the element with the greatest offset that is less than or equal
     * to the specified {@code offset}.</p>
     *
     * @param offset offset of the interest.
     * @param target entry, index and offset of the found element. {@code target.entry} is
     *               {@code null} if offsets of all the elements in the list are greater than
     *               {@code offset}, or the list is empty.
     */
    public void lowerBound(int offset, EntryEx<V> target) {
        Entry<V> node = root;
        Entry<V> foundNode = null;
        int indexBase = 0;
        int nodeIndex = 0;
        int foundNodeIndex = 0;
        int offsetBase = 0;
        int nodeOffset;
        int foundNodeOffset = 0;

        while (node != null) {
            nodeIndex = indexBase + node.index;
            nodeOffset = offsetBase + node.offset;
            if (offset < nodeOffset) {
                node = node.leftChild;
            } else if (offset > nodeOffset) {
                // At least we found a node whose offset is less than the specified offset.
                foundNode = node;
                foundNodeIndex = nodeIndex;
                foundNodeOffset = nodeOffset;
                indexBase = nodeIndex;
                offsetBase = nodeOffset;
                node = node.rightChild;
            } else {
                // The node with exactly matching offset is found.
                foundNode = node;
                foundNodeIndex = nodeIndex;
                foundNodeOffset = nodeOffset;
                break;
            }
        }

        target.entry = foundNode;
        if (foundNode != null) {
            target.index = foundNodeIndex;
            target.offset = foundNodeOffset;
        }
    }

    /**
     * Finds the element that is the upper bound for the specified offset.
     *
     * <p>This method is to locate the element with the smallest offset that is greater than the
     * specified {@code offset}.</p>
     *
     * @param offset offset of the interest.
     *
     * @return entry representing the found element or {@code null} if offset of all the
     *         elements in the list are less than or equal to {@code offset}, or the list is empty.
     */
    public Entry<V> upperBound(int offset) {
        Entry<V> node = root;
        Entry<V> foundNode = null;
        int offsetBase = 0;
        int nodeOffset;

        while (node != null) {
            nodeOffset = offsetBase + node.offset;
            if (offset < nodeOffset) {
                // At least we found a node whose offset is greater than the specified offset.
                foundNode = node;
                node = node.leftChild;
            } else {
                offsetBase = nodeOffset;
                node = node.rightChild;
            }
        }

        return foundNode;
    }

    /**
     * Finds the element that is the upper bound for the specified offset.
     *
     * <p>This method is to locate the element with the smallest offset that is greater than the
     * specified {@code offset}.</p>
     *
     * @param offset offset of the interest.
     * @param target entry, index and offset of the found element. {@code target.entry} is
     *               {@code null} if offsets of all the elements in the list are less than or equal
     *               to {@code offset}, or the list is empty.
     */
    public void upperBound(int offset, EntryEx<V> target) {
        Entry<V> node = root;
        Entry<V> foundNode = null;
        int indexBase = 0;
        int nodeIndex = 0;
        int foundNodeIndex = 0;
        int offsetBase = 0;
        int nodeOffset;
        int foundNodeOffset = 0;

        while (node != null) {
            nodeIndex = indexBase + node.index;
            nodeOffset = offsetBase + node.offset;
            if (offset < nodeOffset) {
                // At least we found a node whose offset greater than the specified offset.
                foundNode = node;
                foundNodeIndex = nodeIndex;
                foundNodeOffset = nodeOffset;
                node = node.leftChild;
            } else {
                indexBase = nodeIndex;
                offsetBase = nodeOffset;
                node = node.rightChild;
            }
        }

        target.entry = foundNode;
        if (foundNode != null) {
            target.index = foundNodeIndex;
            target.offset = foundNodeOffset;
        }
    }

    public Entry<V> put(int offset, V value) {
        return put(offset, value, 0);
    }

    public Entry<V> put(int offset, V value, int size) {
        if (size < 0) {
            throw new IllegalArgumentException("Negative element size is not allowed.");
        }

        if (root == null) {
            root = createEntry(0, offset, value, (byte)0, null);
            nodeCount = 1;
            return root;
        }

        if (size == 0) {
            // Check if the tree contains an entry with exactly matching offset.
            Entry<V> node = root;
            int offsetBase = 0;
            int nodeOffset;

            while (node != null) {
                nodeOffset = offsetBase + node.offset;
                if (offset < nodeOffset) {
                    node = node.leftChild;
                } else if (offset > nodeOffset) {
                    offsetBase = nodeOffset;
                    node = node.rightChild;
                } else {
                    // The node with exactly matching offset is found.
                    node.setValue(value);
                    return node;
                }
            }
        }

        Entry<V> node = root; // Is to be the parent node for the new element's node.
        boolean isLeftChild = false;
        int indexBase = 0;
        int nodeIndex;
        int offsetBase = 0;
        int nodeOffset;
        int newNodeIndex;

        for (Entry<V> nextNode; ; node = nextNode) {
            nodeIndex = indexBase + node.index;
            nodeOffset = offsetBase + node.offset;
            if (offset > nodeOffset) {
                // Let's go to the right subtree.
                indexBase = nodeIndex;
                offsetBase = nodeOffset;
                nextNode = node.rightChild;
                if (nextNode == null) {
                    // The parent node is found.
                    newNodeIndex = nodeIndex + 1;
                    break;
                }
            } else {
                // Let's go to the left subtree.
                // But before doing this we gotta amend the current node's index and offset.
                ++node.index;
                node.offset += size;
                nextNode = node.leftChild;
                if (nextNode == null) {
                    // The parent node is found.
                    // And the new node is to be the left child of this parent.
                    isLeftChild = true;
                    newNodeIndex = nodeIndex;
                    break;
                }
            }
        }

        final Entry<V> newNode = createEntry(newNodeIndex - indexBase,
                                             offset - offsetBase,
                                             value,
                                             FLAG_NODE_IS_RED,
                                             node);
        if (isLeftChild) {
            node.leftChild = newNode;
            newNode.flags |= FLAG_NODE_IS_LEFT_CHILD;
        } else {
            node.rightChild = newNode;
        }

        ++nodeCount;
        fixUpAfterInsertion(newNode);
        return newNode;
    }

    public void remove(Entry<V> entry) {
        remove(entry, false);
    }

    public void remove(Entry<V> entry, boolean shallShiftFollowingNodes) {
        // Here we operate the notions of 'original node' and 'transplanted node', see comments
        // below for more information on what these nodes are. The fix-up procedure will make its
        // tricks upon the transplanted node, but this node might be null (the leaf node),
        // therefore we don't take it directly, but take its parent and its side, see
        // transplantedNodeParent and isTransplantedNodeLeft.

        final Entry<V> leftMostNodeInRightSubtree = Entry.leftMost(entry.rightChild);

        SHIFT: if (shallShiftFollowingNodes) {
            // We need to compute the offset shift, which is basically the relative offset
            // of the next node (next_node_offset - node_being_removed_offset).
            if (leftMostNodeInRightSubtree == null) {
                Entry<V> node = entry.parent;
                int offsetShift = -entry.offset;
                for (byte flags = entry.flags; ; node = node.parent) {
                    if (node == null) {
                        // This is the last node in the tree, no need to shift anything.
                        break SHIFT;
                    }
                    if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                        offsetShift += node.offset;
                        break;
                    }
                    offsetShift -= node.offset;
                    flags = node.flags;
                }

                // Now we know the offset shift.
                --node.index;
                node.offset -= offsetShift;
                byte flags = node.flags;
                for (node = node.parent; node != null; node = node.parent) {
                    if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                        --node.index;
                        node.offset -= offsetShift;
                    }
                    flags = node.flags;
                }
            } else {
                // In this case the offset shift is computed trivially.
                final int offsetShift = leftMostNodeInRightSubtree.offset;

                Entry<V> node = entry.parent;
                for (byte flags = entry.flags; node != null; node = node.parent) {
                    if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                        --node.index;
                        node.offset -= offsetShift;
                    }
                    flags = node.flags;
                }

                for (node = entry.rightChild;
                     node != leftMostNodeInRightSubtree; node = node.leftChild) {
                    --node.index;
                    node.offset -= offsetShift;
                }

                leftMostNodeInRightSubtree.offset = 0;
            }
        } else {
            // Shift the indices.
            Entry<V> node = entry.parent;
            for (byte flags = entry.flags; node != null; node = node.parent) {
                if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                    --node.index;
                }
                flags = node.flags;
            }

            if (leftMostNodeInRightSubtree != null) {
                final int offsetShift = leftMostNodeInRightSubtree.offset;

                for (node = entry.rightChild;
                     node != leftMostNodeInRightSubtree; node = node.leftChild) {
                    --node.index;
                    node.offset -= offsetShift;
                }
            }
        }

        byte originalNodeFlags;
        Entry<V> transplantedNodeParent;
        boolean isTransplantedNodeLeft;

        if (entry.leftChild == null) {
            // The node being removed has empty left subtree, therefore its direct right child
            // shall be transplanted to the removed node place.
            // So here the 'original node' is the node being removed, and the 'transplanted node'
            // is its right child.
            originalNodeFlags = entry.flags;
            transplantedNodeParent = entry.parent;
            final Entry<V> rightChild = entry.rightChild;
            isTransplantedNodeLeft = transplant(entry, rightChild);

            if (rightChild != null) {
                // Fix-up the index and the offset.
                rightChild.index = entry.index;
                rightChild.offset += entry.offset;
            }
        } else if (entry.rightChild == null) {
            // The node being removed has empty right subtree, therefore its direct left child
            // shall be transplanted to the removed node place.
            // So here the 'original node' is the node being removed, and the 'transplanted node'
            // is its left child.
            originalNodeFlags = entry.flags;
            transplantedNodeParent = entry.parent;
            isTransplantedNodeLeft = transplant(entry, entry.leftChild);
        } else {
            // The node being removed has two children. We gotta find the next node, which is the
            // leftmost node in the right subtree. This next node will be transplanted to the
            // removed node place. But things are not that simple is this case - the next node's
            // left subtree is definitely empty (otherwise it wouldn't be the leftmost node), but
            // its right subtree might be not empty. In this case the 'original node' is the next
            // node, and the 'transplanted node' is the next node's direct right child.
            final Entry<V> nextNode = leftMostNodeInRightSubtree;
            originalNodeFlags = nextNode.flags;

            if (nextNode.parent == entry) {
                // The next node is a direct child of the node being removed. Thus
                // transplantedNodeParent cannot be nextNode.parent, as nextNode.parent is about
                // to get removed.
                transplantedNodeParent = nextNode;
                isTransplantedNodeLeft = false;
            } else {
                transplantedNodeParent = nextNode.parent;
                final Entry<V> rightChild = nextNode.rightChild;
                isTransplantedNodeLeft = transplant(nextNode, nextNode.rightChild);
                // TODO: Do we need to fix-up index and offset here??? Seems we don't...
                nextNode.rightChild = entry.rightChild;
                nextNode.rightChild.parent = nextNode;
            }

            transplant(entry, nextNode);
            nextNode.leftChild = entry.leftChild;
            nextNode.leftChild.parent = nextNode;
            nextNode.flags = entry.flags;

            // Fix-up the index and the offset.
            nextNode.index = entry.index;
            nextNode.offset += entry.offset;
        }

        --nodeCount;
        // Close the removed node.
        entry.value = null;
        entry.parent = null;
        entry.leftChild = null;
        entry.rightChild = null;

        if ((originalNodeFlags & FLAG_NODE_IS_RED) == 0) {
            fixUpAfterRemoval(transplantedNodeParent, isTransplantedNodeLeft);
        }
    }

    public void shiftAll(int shift) {
        for (Entry<V> node = root; node != null; node = node.leftChild) {
            node.offset += shift;
        }
    }

    public void clear() {
        root = null;
        nodeCount = 0;
    }

    private static <V> Entry<V> createEntry(int index,
                                            int offset,
                                            V value,
                                            byte flags,
                                            Entry<V> parent) {
        return new Entry<V>(index, offset,  value,  flags, parent);
    }

    private void fixUpAfterInsertion(Entry<V> newNode) {
        for (Entry<V> redNode = newNode, parent = newNode.parent; ; parent = redNode.parent) {
            if (parent == null) {
                // This is the root, so make it black.
                redNode.flags &= ~FLAG_NODE_IS_RED;
                break;
            }

            if ((parent.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                final Entry<V> grandParent = parent.parent; // This node is definitely black.

                if ((parent.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                    // Parent is the left child.
                    if ((redNode.flags & FLAG_NODE_IS_LEFT_CHILD) == 0) {
                        // But the red node itself is right child.
                        rotateLeft(parent);
                        parent = redNode;
                    }

                    final Entry<V> uncle = grandParent.rightChild;
                    if (isNodeBlack(uncle)) {
                        // The uncle is black.
                        rotateRight(grandParent);
                        // The parent is to become black.
                        parent.flags &= ~FLAG_NODE_IS_RED;
                        // And the grandparent is to become red.
                        grandParent.flags |= FLAG_NODE_IS_RED;
                        break;
                    } else {
                        // The uncle is red.
                        // Make both, the parent and the uncle, black.
                        parent.flags &= ~FLAG_NODE_IS_RED;
                        uncle.flags &= ~FLAG_NODE_IS_RED;
                        // Make the grandparent red.
                        grandParent.flags |= FLAG_NODE_IS_RED;
                        // And we might still need to fix-up the tree, as the grandparent
                        // has become red.
                        redNode = grandParent;
                    }
                } else {
                    // Parent is the right child.
                    if ((redNode.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                        // But the red node itself is left child.
                        rotateRight(parent);
                        parent = redNode;
                    }

                    final Entry<V> uncle = grandParent.leftChild;
                    if (isNodeBlack(uncle)) {
                        // The uncle is black.
                        rotateLeft(grandParent);
                        // The parent is to become black.
                        parent.flags &= ~FLAG_NODE_IS_RED;
                        // And the grandparent is to become red.
                        grandParent.flags |= FLAG_NODE_IS_RED;
                        break;
                    } else {
                        // The uncle is red.
                        // Make both, the parent and the uncle, black.
                        parent.flags &= ~FLAG_NODE_IS_RED;
                        uncle.flags &= ~FLAG_NODE_IS_RED;
                        // Make the grandparent red.
                        grandParent.flags |= FLAG_NODE_IS_RED;
                        // And we might still need to fix-up the tree, as the grandparent
                        // has become red.
                        redNode = grandParent;
                    }
                }
            } else {
                // The parent is black, no fix-up is needed.
                break;
            }
        }
    }

    private void fixUpAfterRemoval(Entry<V> transplantedNodeParent, boolean isLeft) {
        for (Entry<V> parent = transplantedNodeParent;;) {
            if (parent == null) {
                if (root != null) {
                    root.flags &= ~FLAG_NODE_IS_RED;
                }
                break;
            }

            if (isLeft) {
                final Entry<V> node = parent.leftChild;
                if (isNodeBlack(node)) {
                    Entry<V> sibling = parent.rightChild;
                    assert sibling != null;

                    if ((sibling.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                        sibling.flags ^= FLAG_NODE_IS_RED; // Make the sibling black.
                        parent.flags |= FLAG_NODE_IS_RED;  // Make the parent red.
                        rotateLeft(parent);
                        sibling = parent.rightChild;
                    }

                    final boolean isSiblingLeftChildBlack = isNodeBlack(sibling.leftChild);
                    final boolean isSiblingRightChildBlack = isNodeBlack(sibling.rightChild);

                    if (isSiblingLeftChildBlack && isSiblingRightChildBlack) {
                        sibling.flags |= FLAG_NODE_IS_RED; // Make the sibling red.
                        isLeft =
                            (parent.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD;
                        parent = parent.parent;
                    } else {
                        if (isSiblingRightChildBlack) {
                            sibling.leftChild.flags ^= FLAG_NODE_IS_RED; // Make it back too.
                            sibling.flags |= FLAG_NODE_IS_RED;
                            rotateRight(sibling);
                            sibling = parent.rightChild;
                        }

                        // Make the sibling the same color as the parent is,
                        // and make the parent black.
                        if ((parent.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                            sibling.flags |= FLAG_NODE_IS_RED;
                            parent.flags ^= FLAG_NODE_IS_RED;
                        } else {
                            sibling.flags &= ~FLAG_NODE_IS_RED;
                        }

                        sibling.rightChild.flags &= ~FLAG_NODE_IS_RED;
                        rotateLeft(parent);
                        parent = null;
                    }
                } else {
                    // Make it black.
                    node.flags ^= FLAG_NODE_IS_RED;
                    // And exit.
                    break;
                }
            } else {
                final Entry<V> node = parent.rightChild;
                if (isNodeBlack(node)) {
                    Entry<V> sibling = parent.leftChild;
                    assert sibling != null;

                    if ((sibling.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                        sibling.flags ^= FLAG_NODE_IS_RED; // Make the sibling black.
                        parent.flags |= FLAG_NODE_IS_RED;  // Make the parent red.
                        rotateRight(parent);
                        sibling = parent.leftChild;
                    }

                    final boolean isSiblingLeftChildBlack = isNodeBlack(sibling.leftChild);
                    final boolean isSiblingRightChildBlack = isNodeBlack(sibling.rightChild);

                    if (isSiblingLeftChildBlack && isSiblingRightChildBlack) {
                        sibling.flags |= FLAG_NODE_IS_RED; // Make the sibling red.
                        isLeft =
                            (parent.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD;
                        parent = parent.parent;
                    } else {
                        if (isSiblingLeftChildBlack) {
                            sibling.rightChild.flags ^= FLAG_NODE_IS_RED; // Make it back too.
                            sibling.flags |= FLAG_NODE_IS_RED;
                            rotateLeft(sibling);
                            sibling = parent.leftChild;
                        }

                        // Make the sibling the same color as the parent is,
                        // and make the parent black.
                        if ((parent.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                            sibling.flags |= FLAG_NODE_IS_RED;
                            parent.flags ^= FLAG_NODE_IS_RED;
                        } else {
                            sibling.flags &= ~FLAG_NODE_IS_RED;
                        }

                        sibling.leftChild.flags &= ~FLAG_NODE_IS_RED;
                        rotateRight(parent);
                        parent = null;
                    }
                } else {
                    // Make it black.
                    node.flags ^= FLAG_NODE_IS_RED;
                    // And exit.
                    break;
                }
            }
        }
    }

    private void rotateLeft(Entry<V> pivotNode) {
        final Entry<V> rightChild = pivotNode.rightChild;
        if (rightChild != null) {
            final Entry<V> rightLeftGrandChild = rightChild.leftChild;
            pivotNode.rightChild = rightLeftGrandChild;
            if (rightLeftGrandChild != null) {
                rightLeftGrandChild.parent = pivotNode;
                // Reset the left-child flag.
                rightLeftGrandChild.flags &= ~FLAG_NODE_IS_LEFT_CHILD;
            }

            final Entry<V> parent = pivotNode.parent;
            pivotNode.parent = rightChild;
            rightChild.leftChild = pivotNode;
            rightChild.parent = parent;
            if (parent == null) {
                // The pivot node was the root of the tree.
                root = rightChild;
                pivotNode.flags |= FLAG_NODE_IS_LEFT_CHILD;
            } else {
                if ((pivotNode.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                    parent.leftChild = rightChild;
                    rightChild.flags |= FLAG_NODE_IS_LEFT_CHILD;
                } else {
                    parent.rightChild = rightChild;
                    pivotNode.flags |= FLAG_NODE_IS_LEFT_CHILD;
                }
            }

            // Correct the index and offset.
            rightChild.index += pivotNode.index;
            rightChild.offset += pivotNode.offset;
        }
    }

    private void rotateRight(Entry<V> pivotNode) {
        final Entry<V> leftChild = pivotNode.leftChild;
        if (leftChild != null) {
            final Entry<V> leftRightGrandChild = leftChild.rightChild;
            pivotNode.leftChild = leftRightGrandChild;
            if (leftRightGrandChild != null) {
                leftRightGrandChild.parent = pivotNode;
                // Set the left-child flag.
                leftRightGrandChild.flags |= FLAG_NODE_IS_LEFT_CHILD;
            }

            final Entry<V> parent = pivotNode.parent;
            pivotNode.parent = leftChild;
            leftChild.rightChild = pivotNode;
            leftChild.parent = parent;
            if (parent == null) {
                // The pivot node was the root of the tree.
                root = leftChild;
                leftChild.flags ^= FLAG_NODE_IS_LEFT_CHILD;
            } else {
                if ((pivotNode.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                    parent.leftChild = leftChild;
                    // Reset the left-child flag.
                    pivotNode.flags &= ~FLAG_NODE_IS_LEFT_CHILD;
                } else {
                    parent.rightChild = leftChild;
                    leftChild.flags &= ~FLAG_NODE_IS_LEFT_CHILD;
                }
            }

            // Correct the index and offset.
            pivotNode.index -= leftChild.index;
            pivotNode.offset -= leftChild.offset;
        }
    }

    private boolean transplant(Entry<V> originalNode, Entry<V> transplantedNode) {
        boolean isTransplantedNodeLeft = false;
        final Entry<V> originalParent = originalNode.parent;

        if (originalParent == null) {
            root = transplantedNode;
        } else {
            if ((originalNode.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                originalParent.leftChild = transplantedNode;
                isTransplantedNodeLeft = true;
            } else {
                originalParent.rightChild = transplantedNode;
            }
        }

        if (transplantedNode != null) {
            transplantedNode.parent = originalParent;
            if (isTransplantedNodeLeft) {
                transplantedNode.flags |= FLAG_NODE_IS_LEFT_CHILD;
            } else {
                transplantedNode.flags &= ~FLAG_NODE_IS_LEFT_CHILD;
            }
        }

        return isTransplantedNodeLeft;
    }

    private static <V> boolean isNodeBlack(Entry<V> node) {
        return (node == null || (node.flags & FLAG_NODE_IS_RED) == 0);
    }

    public static final class Entry<V> implements BinaryTree.Node<V> {
        private int index;  // Absolute or relative.
        private int offset; // Absolute or relative.
        private V value;
        private byte flags;
        private Entry<V> parent;
        private Entry<V> leftChild = null;
        private Entry<V> rightChild = null;

        public int index() {
            int index = this.index;
            Entry<V> node = parent;
            for (byte flags = this.flags; node != null; node = node.parent) {
                if ((flags & FLAG_NODE_IS_LEFT_CHILD) == 0) {
                    index += node.index;
                }
                flags = node.flags;
            }

            return index;
        }

        public int offset() {
            int offset = this.offset;
            Entry<V> node = parent;
            for (byte flags = this.flags; node != null; node = node.parent) {
                if ((flags & FLAG_NODE_IS_LEFT_CHILD) == 0) {
                    offset += node.offset;
                }
                flags = node.flags;
            }

            return offset;
        }

        @Override
        public V value() {
            return value;
        }

        @Override
        public BinaryTree.Node<V> parent() {
            return parent;
        }

        @Override
        public BinaryTree.Node<V> leftChild() {
            return leftChild;
        }

        @Override
        public BinaryTree.Node<V> rightChild() {
            return rightChild;
        }

        public void setValue(V newValue) {
            value = newValue;
        }

        public Entry<V> next() {
            Entry<V> node = leftMost(rightChild);

            if (node == null) {
                node = parent;
                for (byte flags = this.flags; node != null; node = node.parent) {
                    if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                        break;
                    }
                    flags = node.flags;
                }
            }

            return node;
        }

        public void next(EntryEx<V> target) {
            Entry<V> node = leftMost(rightChild);

            if (node == null) {
                node = parent;
                int relativeOffset = -offset;
                for (byte flags = this.flags; node != null; node = node.parent) {
                    if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                        break;
                    }
                    relativeOffset -= node.offset;
                    flags = node.flags;
                }

                if (node == null) {
                    target.entry = null;
                } else {
                    relativeOffset += node.offset;
                    target.entry = node;
                    target.index = 1; // Relative index, i.e. (nextEntryIndex - thisEntryIndex).
                    target.offset = relativeOffset;
                }
            } else {
                target.entry = node;
                target.index = 1; // Relative index, i.e. (nextEntryIndex - thisEntryIndex).
                target.offset = node.offset;
            }
        }

        public Entry<V> previous() {
            Entry<V> node = rightMost(leftChild);

            if (node == null) {
                node = parent;
                for (byte flags = this.flags; node != null; node = node.parent) {
                    if ((flags & FLAG_NODE_IS_LEFT_CHILD) == 0){
                        break;
                    }
                    flags = node.flags;
                }
            }

            return node;
        }

        public void previous(EntryEx<V> target) {
            Entry<V> node;

            if (leftChild == null) {
                node = parent;
                for (byte flags = this.flags; node != null; node = node.parent) {
                    if ((flags & FLAG_NODE_IS_LEFT_CHILD) == 0) {
                        break;
                    }
                    flags = node.flags;
                }

                if (node == null) {
                    target.entry = null;
                } else {
                    target.entry = node;
                    target.index =  -1; // Relative index, i.e. (prevEntryIndex - thisEntryIndex).
                    target.offset = -offset; // Negative value.
                }
            } else {
                int relativeOffset = -offset; // Negative value.
                Entry<V> nextNode = leftChild;
                do {
                    node = nextNode;
                    relativeOffset += node.offset;
                    nextNode = node.rightChild;
                } while (nextNode != null);

                target.entry = node;
                target.index =  -1; // Relative index, i.e. (prevEntryIndex - thisEntryIndex).
                target.offset = relativeOffset; // Still negative.
            }
        }

        public void shiftAllFromThis(int shift) {
            CHECK_SHIFT: if (shift < 0) {
                // If the shift is negative, its absolute value must be less than the offset of this
                // node relative to previous node. In other words, let thisOffset be the offset
                // of this node, and prevOffset be the offset of the previous node. Then the
                // inequality |shift| < (thisOffset - prevOffset) must hold.

                int relativeOffset; // Supposed to be negative.

                if (leftChild == null) {
                    Entry<V> node = parent;
                    for (byte flags = this.flags; ; node = node.parent) {
                        if (node == null) {
                            // This is the first node in the tree.
                            break CHECK_SHIFT;
                        }
                        if ((flags & FLAG_NODE_IS_LEFT_CHILD) == 0) {
                            break;
                        }
                        flags = node.flags;
                    }
                    relativeOffset = -offset;
                } else {
                    relativeOffset = -offset;
                    Entry<V> node;
                    Entry<V> nextNode = leftChild;
                    do {
                        node = nextNode;
                        relativeOffset += node.offset;
                        nextNode = node.rightChild;
                    } while (nextNode != null);
                }

                if (shift <= relativeOffset) {
                    throw new IllegalArgumentException("Too big left shift.");
                }
            }

            offset += shift;
            Entry<V> node = parent;
            for (byte flags = this.flags; node != null; node = node.parent) {
                if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                    node.offset += shift;
                }
                flags = node.flags;
            }
        }

        public void shiftAllAfterThis(int shift) {
            if (shift < 0) {
                // If the shift is negative, its absolute value must be less than the offset of the
                // next node relative to this node. In other words, let thisOffset be the offset
                // of this node, and nextOffset be the offset of the next node. Then the inequality
                // |shift| < (nextOffset - thisOffset) must hold.

                int relativeOffset;

                if (rightChild == null) {
                    Entry<V> node = parent;
                    relativeOffset = -offset;
                    for (byte flags = this.flags; (flags & FLAG_NODE_IS_LEFT_CHILD) == 0;
                         node = node.parent) {
                        if (node == null) {
                            // This is the last node in the tree. Nothing is to be shifted.
                            return;
                        }
                        relativeOffset -= node.offset;
                        flags = node.flags;
                    }
                    relativeOffset += node.offset;
                } else {
                    Entry<V> node;
                    Entry<V> nextNode = rightChild;
                    do {
                        node = nextNode;
                        nextNode = node.leftChild;
                    } while (nextNode != null);
                    relativeOffset = node.offset;
                }

                if (-shift >= relativeOffset) {
                    throw new IllegalArgumentException("Too big left shift.");
                }
            }

            Entry<V> node = parent;
            for (byte flags = this.flags; node != null; node = node.parent) {
                if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                    node.offset += shift;
                }
                flags = node.flags;
            }

            for (node = rightChild; node != null; node = node.leftChild) {
                node.offset += shift;
            }
        }

        public static <V> Entry<V> leftMost(Entry<V> subtreeRoot) {
            Entry<V> node = subtreeRoot;
            if (node != null) {
                for (;;) {
                    final Entry<V> nextNode = node.leftChild;
                    if (nextNode == null) {
                        break;
                    } else {
                        node = nextNode;
                    }
                }
            }

            return node;
        }

        public static <V> Entry<V> rightMost(Entry<V> subtreeRoot) {
            Entry<V> node = subtreeRoot;
            if (node != null) {
                for (;;) {
                    final Entry<V> nextNode = node.rightChild;
                    if (nextNode == null) {
                        break;
                    } else {
                        node = nextNode;
                    }
                }
            }

            return node;
        }

        @Override
        public String toString() {
            final StringBuilder strBld = new StringBuilder("[");
            strBld.append(index()).append("(+").append(index).append("), ");
            strBld.append(offset()).append("(+").append(offset).append(")]");

            if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                if ((flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                    strBld.append("(l,r)");
                } else {
                    strBld.append("(l)");
                }
            } else {
                if ((flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                    strBld.append("(r)");
                }
            }

            strBld.append(": ").append(value);
            return strBld.toString();
        }

        private Entry(int index,
                      int offset,
                      V value,
                      byte flags,
                      Entry<V> parent) {
            this.index = index;
            this.offset = offset;
            this.value = value;
            this.flags = flags;
            this.parent = parent;
        }
    }

    public static final class EntryEx<V> {
        public Entry<V> entry = null;
        public int index = 0;
        public int offset = 0;

        public void assign(EntryEx<V> anotherEntry) {
            entry = anotherEntry.entry;
            index = anotherEntry.index;
            offset = anotherEntry.offset;
        }
    }
}
