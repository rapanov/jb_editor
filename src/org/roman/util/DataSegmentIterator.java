/**
 * Author: Roman Panov
 * Date:   12/1/12
 */

package org.roman.util;

import java.util.Iterator;

public interface DataSegmentIterator<D> extends Iterator<DataSegment<D>> {
    boolean next(DataSegment<D> target);
}
