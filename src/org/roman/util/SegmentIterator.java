/**
 * Author: Roman Panov
 * Date:   12/1/12
 */

package org.roman.util;

import java.util.Iterator;

public interface SegmentIterator extends Iterator<Segment> {
    boolean next(Segment target);
}
