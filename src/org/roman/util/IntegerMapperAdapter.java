/**
 * Author: Roman Panov
 * Date:   2/17/13
 */

package org.roman.util;

public abstract class IntegerMapperAdapter<V> implements IntegerMapper<V> {
    @Override
    public V map(Integer key) {
        return map(key.intValue());
    }
}
