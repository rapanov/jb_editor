/**
 * Author: Roman Panov
 * Date:   12/1/12
 */

package org.roman.util;

public final class DataSegment<D> extends Segment {

    public D data;

    public DataSegment() {
        this.data = null;
    }

    public DataSegment(int offset, int length) {
        this(offset,  length, null);
    }

    public DataSegment(D data) {
        this.data = data;
    }

    public DataSegment(int offset, int length, D data)
    {
        super(offset, length);
        this.data = data;
    }

    public void assign(DataSegment<D> anotherSegment) {
        offset = anotherSegment.offset;
        length = anotherSegment.length;
        data = anotherSegment.data;
    }
}