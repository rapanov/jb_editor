/**
 * Author: Roman Panov
 * Date:   1/26/13
 */

package org.roman.util;

public final class TreeList<V> implements BinaryTree<V> {

    private static final byte FLAG_NODE_IS_LEFT_CHILD = 0x01;
    private static final byte FLAG_NODE_IS_RED        = 0x02;

    private Entry<V> root = null;
    private int nodeCount = 0;

    public Node<V> root() {
        return root;
    }

    public int size() {
        return nodeCount;
    }

    public boolean isEmpty() {
        return (nodeCount == 0);
    }

    public Entry<V> first() {
        return Entry.leftMost(root);
    }

    public Entry<V> last() {
        return Entry.rightMost(root);
    }

    public Entry<V> get(int index) {
        Entry<V> node = root;
        int indexBase = 0;
        int nodeIndex;

        while (node != null) {
            nodeIndex = indexBase + node.index;
            if (index < nodeIndex) {
                node = node.leftChild;
            } else if (index > nodeIndex) {
                indexBase = nodeIndex;
                node = node.rightChild;
            } else {
                // The node is found.
                break;
            }
        }

        return node;
    }

    public Entry<V> prepend(V value) {
        if (root == null) {
            return setRoot(value);
        }

        Entry<V> node = root; // Is to be the parent node for the new element's node.
        for (Entry<V> nextNode; ; node = nextNode) {
            ++node.index;
            nextNode = node.leftChild;
            if (nextNode == null) {
                break;
            }
        }

        final Entry<V> newNode =
            createEntry(0, value, (byte)(FLAG_NODE_IS_LEFT_CHILD | FLAG_NODE_IS_RED), node);
        node.leftChild = newNode;
        ++nodeCount;
        fixUpAfterInsertion(newNode);
        return newNode;
    }

    public Entry<V> append(V value) {
        if (root == null) {
            return setRoot(value);
        }

        final Entry<V> parentNode = Entry.rightMost(root);
        final Entry<V> newNode = createEntry(1, value, FLAG_NODE_IS_RED, parentNode);
        parentNode.rightChild = newNode;
        ++nodeCount;
        fixUpAfterInsertion(newNode);
        return newNode;
    }

    public Entry<V> insert(int index, V value) {
        if (index < 0 || index > nodeCount) {
            throw new IllegalArgumentException("Illegal insertion index " + index +
                                               " for list of size " + nodeCount + ".");
        }

        if (root == null) {
            return setRoot(value);
        }

        Entry<V> node = root; // Is to be the parent node for the new element's node.
        boolean isLeftChild = false;
        int indexBase = 0;
        int nodeIndex;

        for (Entry<V> nextNode; ; node = nextNode) {
            nodeIndex = indexBase + node.index;
            if (index > nodeIndex) {
                // Let's go to the right subtree.
                indexBase = nodeIndex;
                nextNode = node.rightChild;
                if (nextNode == null) {
                    // The parent node is found.
                    break;
                }
            } else {
                // Let's go to the left subtree.
                // But before doing this we gotta increment the current node's index.
                ++node.index;
                nextNode = node.leftChild;
                if (nextNode == null) {
                    // The parent node is found.
                    // And the new node is to be the left child of this parent.
                    isLeftChild = true;
                    break;
                }
            }
        }

        final Entry<V> newNode = createEntry(index - indexBase, value, FLAG_NODE_IS_RED, node);
        if (isLeftChild) {
            node.leftChild = newNode;
            newNode.flags |= FLAG_NODE_IS_LEFT_CHILD;
        } else {
            node.rightChild = newNode;
        }

        ++nodeCount;
        fixUpAfterInsertion(newNode);
        return newNode;
    }

    public void remove(Entry<V> entry) {
        // Here we operate the notions of 'original node' and 'transplanted node', see comments
        // below for more information on what these nodes are. The fix-up procedure will make its
        // tricks upon the transplanted node, but this node might be null (the leaf node),
        // therefore we don't take it directly, but take its parent and its side, see
        // transplantedNodeParent and isTransplantedNodeLeft.

        final Entry<V> leftMostNodeInRightSubtree = Entry.leftMost(entry.rightChild);

        // Shift the indices.
        Entry<V> node = entry.parent;
        for (byte flags = entry.flags; node != null; node = node.parent) {
            if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                --node.index;
            }
            flags = node.flags;
        }

        if (leftMostNodeInRightSubtree != null) {
            for (node = entry.rightChild;
                 node != leftMostNodeInRightSubtree; node = node.leftChild) {
                --node.index;
            }
        }

        byte originalNodeFlags;
        Entry<V> transplantedNodeParent;
        boolean isTransplantedNodeLeft;

        if (entry.leftChild == null) {
            // The node being removed has empty left subtree, therefore its direct right child
            // shall be transplanted to the removed node place.
            // So here the 'original node' is the node being removed, and the 'transplanted node'
            // is its right child.
            originalNodeFlags = entry.flags;
            transplantedNodeParent = entry.parent;
            final Entry<V> rightChild = entry.rightChild;
            isTransplantedNodeLeft = transplant(entry, rightChild);

            if (rightChild != null) {
                // Fix-up the index and the offset.
                rightChild.index = entry.index;
            }
        } else if (entry.rightChild == null) {
            // The node being removed has empty right subtree, therefore its direct left child
            // shall be transplanted to the removed node place.
            // So here the 'original node' is the node being removed, and the 'transplanted node'
            // is its left child.
            originalNodeFlags = entry.flags;
            transplantedNodeParent = entry.parent;
            isTransplantedNodeLeft = transplant(entry, entry.leftChild);
        } else {
            // The node being removed has two children. We gotta find the next node, which is the
            // leftmost node in the right subtree. This next node will be transplanted to the
            // removed node place. But things are not that simple is this case - the next node's
            // left subtree is definitely empty (otherwise it wouldn't be the leftmost node), but
            // its right subtree might be not empty. In this case the 'original node' is the next
            // node, and the 'transplanted node' is the next node's direct right child.
            final Entry<V> nextNode = leftMostNodeInRightSubtree;
            originalNodeFlags = nextNode.flags;

            if (nextNode.parent == entry) {
                // The next node is a direct child of the node being removed. Thus
                // transplantedNodeParent cannot be nextNode.parent, as nextNode.parent is about
                // to get removed.
                transplantedNodeParent = nextNode;
                isTransplantedNodeLeft = false;
            } else {
                transplantedNodeParent = nextNode.parent;
                final Entry<V> rightChild = nextNode.rightChild;
                isTransplantedNodeLeft = transplant(nextNode, nextNode.rightChild);
                // TODO: Do we need to fix-up index here??? Seems we don't...
                nextNode.rightChild = entry.rightChild;
                nextNode.rightChild.parent = nextNode;
            }

            transplant(entry, nextNode);
            nextNode.leftChild = entry.leftChild;
            nextNode.leftChild.parent = nextNode;
            nextNode.flags = entry.flags;

            // Fix-up the index and the offset.
            nextNode.index = entry.index;
        }

        --nodeCount;
        // Close the removed node.
        entry.value = null;
        entry.parent = null;
        entry.leftChild = null;
        entry.rightChild = null;

        if ((originalNodeFlags & FLAG_NODE_IS_RED) == 0) {
            fixUpAfterRemoval(transplantedNodeParent, isTransplantedNodeLeft);
        }
    }

    public int remove(Entry<V> entry, int count) {
        int entriesRemoved = 0;
        for (; entry != null && entriesRemoved < count; ++ entriesRemoved) {
            final Entry<V> nextEntry = entry.next();
            remove(entry);
            entry = nextEntry;
        }

        return entriesRemoved;
    }

    public void clear() {
        root = null;
        nodeCount = 0;
    }

    private static <V> Entry<V> createEntry(int index,
                                            V value,
                                            byte flags,
                                            Entry<V> parent) {
        return new Entry<V>(index, value,  flags, parent);
    }

    private Entry<V> setRoot(V value) {
        root = createEntry(0, value, (byte)0, null);
        nodeCount = 1;
        return root;
    }

    private void fixUpAfterInsertion(Entry<V> newNode) {
        for (Entry<V> redNode = newNode, parent = newNode.parent; ; parent = redNode.parent) {
            if (parent == null) {
                // This is the root, so make it black.
                redNode.flags &= ~FLAG_NODE_IS_RED;
                break;
            }

            if ((parent.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                final Entry<V> grandParent = parent.parent; // This node is definitely black.

                if ((parent.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                    // Parent is the left child.
                    if ((redNode.flags & FLAG_NODE_IS_LEFT_CHILD) == 0) {
                        // But the red node itself is right child.
                        rotateLeft(parent);
                        parent = redNode;
                    }

                    final Entry<V> uncle = grandParent.rightChild;
                    if (isNodeBlack(uncle)) {
                        // The uncle is black.
                        rotateRight(grandParent);
                        // The parent is to become black.
                        parent.flags &= ~FLAG_NODE_IS_RED;
                        // And the grandparent is to become red.
                        grandParent.flags |= FLAG_NODE_IS_RED;
                        break;
                    } else {
                        // The uncle is red.
                        // Make both, the parent and the uncle, black.
                        parent.flags &= ~FLAG_NODE_IS_RED;
                        uncle.flags &= ~FLAG_NODE_IS_RED;
                        // Make the grandparent red.
                        grandParent.flags |= FLAG_NODE_IS_RED;
                        // And we might still need to fix-up the tree, as the grandparent
                        // has become red.
                        redNode = grandParent;
                    }
                } else {
                    // Parent is the right child.
                    if ((redNode.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                        // But the red node itself is left child.
                        rotateRight(parent);
                        parent = redNode;
                    }

                    final Entry<V> uncle = grandParent.leftChild;
                    if (isNodeBlack(uncle)) {
                        // The uncle is black.
                        rotateLeft(grandParent);
                        // The parent is to become black.
                        parent.flags &= ~FLAG_NODE_IS_RED;
                        // And the grandparent is to become red.
                        grandParent.flags |= FLAG_NODE_IS_RED;
                        break;
                    } else {
                        // The uncle is red.
                        // Make both, the parent and the uncle, black.
                        parent.flags &= ~FLAG_NODE_IS_RED;
                        uncle.flags &= ~FLAG_NODE_IS_RED;
                        // Make the grandparent red.
                        grandParent.flags |= FLAG_NODE_IS_RED;
                        // And we might still need to fix-up the tree, as the grandparent
                        // has become red.
                        redNode = grandParent;
                    }
                }
            } else {
                // The parent is black, no fix-up is needed.
                break;
            }
        }
    }

    private void fixUpAfterRemoval(Entry<V> transplantedNodeParent, boolean isLeft) {
        for (Entry<V> parent = transplantedNodeParent;;) {
            if (parent == null) {
                if (root != null) {
                    root.flags &= ~FLAG_NODE_IS_RED;
                }
                break;
            }

            if (isLeft) {
                final Entry<V> node = parent.leftChild;
                if (isNodeBlack(node)) {
                    Entry<V> sibling = parent.rightChild;
                    assert sibling != null;

                    if ((sibling.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                        sibling.flags ^= FLAG_NODE_IS_RED; // Make the sibling black.
                        parent.flags |= FLAG_NODE_IS_RED;  // Make the parent red.
                        rotateLeft(parent);
                        sibling = parent.rightChild;
                    }

                    final boolean isSiblingLeftChildBlack = isNodeBlack(sibling.leftChild);
                    final boolean isSiblingRightChildBlack = isNodeBlack(sibling.rightChild);

                    if (isSiblingLeftChildBlack && isSiblingRightChildBlack) {
                        sibling.flags |= FLAG_NODE_IS_RED; // Make the sibling red.
                        isLeft =
                            (parent.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD;
                        parent = parent.parent;
                    } else {
                        if (isSiblingRightChildBlack) {
                            sibling.leftChild.flags ^= FLAG_NODE_IS_RED; // Make it back too.
                            sibling.flags |= FLAG_NODE_IS_RED;
                            rotateRight(sibling);
                            sibling = parent.rightChild;
                        }

                        // Make the sibling the same color as the parent is,
                        // and make the parent black.
                        if ((parent.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                            sibling.flags |= FLAG_NODE_IS_RED;
                            parent.flags ^= FLAG_NODE_IS_RED;
                        } else {
                            sibling.flags &= ~FLAG_NODE_IS_RED;
                        }

                        sibling.rightChild.flags &= ~FLAG_NODE_IS_RED;
                        rotateLeft(parent);
                        parent = null;
                    }
                } else {
                    // Make it black.
                    node.flags ^= FLAG_NODE_IS_RED;
                    // And exit.
                    break;
                }
            } else {
                final Entry<V> node = parent.rightChild;
                if (isNodeBlack(node)) {
                    Entry<V> sibling = parent.leftChild;
                    assert sibling != null;

                    if ((sibling.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                        sibling.flags ^= FLAG_NODE_IS_RED; // Make the sibling black.
                        parent.flags |= FLAG_NODE_IS_RED;  // Make the parent red.
                        rotateRight(parent);
                        sibling = parent.leftChild;
                    }

                    final boolean isSiblingLeftChildBlack = isNodeBlack(sibling.leftChild);
                    final boolean isSiblingRightChildBlack = isNodeBlack(sibling.rightChild);

                    if (isSiblingLeftChildBlack && isSiblingRightChildBlack) {
                        sibling.flags |= FLAG_NODE_IS_RED; // Make the sibling red.
                        isLeft =
                            (parent.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD;
                        parent = parent.parent;
                    } else {
                        if (isSiblingLeftChildBlack) {
                            sibling.rightChild.flags ^= FLAG_NODE_IS_RED; // Make it back too.
                            sibling.flags |= FLAG_NODE_IS_RED;
                            rotateLeft(sibling);
                            sibling = parent.leftChild;
                        }

                        // Make the sibling the same color as the parent is,
                        // and make the parent black.
                        if ((parent.flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                            sibling.flags |= FLAG_NODE_IS_RED;
                            parent.flags ^= FLAG_NODE_IS_RED;
                        } else {
                            sibling.flags &= ~FLAG_NODE_IS_RED;
                        }

                        sibling.leftChild.flags &= ~FLAG_NODE_IS_RED;
                        rotateRight(parent);
                        parent = null;
                    }
                } else {
                    // Make it black.
                    node.flags ^= FLAG_NODE_IS_RED;
                    // And exit.
                    break;
                }
            }
        }
    }

    private void rotateLeft(Entry<V> pivotNode) {
        final Entry<V> rightChild = pivotNode.rightChild;
        if (rightChild != null) {
            final Entry<V> rightLeftGrandChild = rightChild.leftChild;
            pivotNode.rightChild = rightLeftGrandChild;
            if (rightLeftGrandChild != null) {
                rightLeftGrandChild.parent = pivotNode;
                // Reset the left-child flag.
                rightLeftGrandChild.flags &= ~FLAG_NODE_IS_LEFT_CHILD;
            }

            final Entry<V> parent = pivotNode.parent;
            pivotNode.parent = rightChild;
            rightChild.leftChild = pivotNode;
            rightChild.parent = parent;
            if (parent == null) {
                // The pivot node was the root of the tree.
                root = rightChild;
                pivotNode.flags |= FLAG_NODE_IS_LEFT_CHILD;
            } else {
                if ((pivotNode.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                    parent.leftChild = rightChild;
                    rightChild.flags |= FLAG_NODE_IS_LEFT_CHILD;
                } else {
                    parent.rightChild = rightChild;
                    pivotNode.flags |= FLAG_NODE_IS_LEFT_CHILD;
                }
            }

            // Correct the index and offset.
            rightChild.index += pivotNode.index;
        }
    }

    private void rotateRight(Entry<V> pivotNode) {
        final Entry<V> leftChild = pivotNode.leftChild;
        if (leftChild != null) {
            final Entry<V> leftRightGrandChild = leftChild.rightChild;
            pivotNode.leftChild = leftRightGrandChild;
            if (leftRightGrandChild != null) {
                leftRightGrandChild.parent = pivotNode;
                // Set the left-child flag.
                leftRightGrandChild.flags |= FLAG_NODE_IS_LEFT_CHILD;
            }

            final Entry<V> parent = pivotNode.parent;
            pivotNode.parent = leftChild;
            leftChild.rightChild = pivotNode;
            leftChild.parent = parent;
            if (parent == null) {
                // The pivot node was the root of the tree.
                root = leftChild;
                leftChild.flags ^= FLAG_NODE_IS_LEFT_CHILD;
            } else {
                if ((pivotNode.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                    parent.leftChild = leftChild;
                    // Reset the left-child flag.
                    pivotNode.flags &= ~FLAG_NODE_IS_LEFT_CHILD;
                } else {
                    parent.rightChild = leftChild;
                    leftChild.flags &= ~FLAG_NODE_IS_LEFT_CHILD;
                }
            }

            // Correct the index and offset.
            pivotNode.index -= leftChild.index;
        }
    }

    private boolean transplant(Entry<V> originalNode, Entry<V> transplantedNode) {
        boolean isTransplantedNodeLeft = false;
        final Entry<V> originalParent = originalNode.parent;

        if (originalParent == null) {
            root = transplantedNode;
        } else {
            if ((originalNode.flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                originalParent.leftChild = transplantedNode;
                isTransplantedNodeLeft = true;
            } else {
                originalParent.rightChild = transplantedNode;
            }
        }

        if (transplantedNode != null) {
            transplantedNode.parent = originalParent;
            if (isTransplantedNodeLeft) {
                transplantedNode.flags |= FLAG_NODE_IS_LEFT_CHILD;
            } else {
                transplantedNode.flags &= ~FLAG_NODE_IS_LEFT_CHILD;
            }
        }

        return isTransplantedNodeLeft;
    }

    private static <V> boolean isNodeBlack(Entry<V> node) {
        return (node == null || (node.flags & FLAG_NODE_IS_RED) == 0);
    }

    public static final class Entry<V> implements Node<V> {
        private int index;  // Absolute or relative.
        private V value;
        private byte flags;
        private Entry<V> parent;
        private Entry<V> leftChild = null;
        private Entry<V> rightChild = null;

        public int index() {
            int index = this.index;
            Entry<V> node = parent;
            for (byte flags = this.flags; node != null; node = node.parent) {
                if ((flags & FLAG_NODE_IS_LEFT_CHILD) == 0) {
                    index += node.index;
                }
                flags = node.flags;
            }

            return index;
        }

        @Override
        public V value() {
            return value;
        }

        @Override
        public Node<V> parent() {
            return parent;
        }

        @Override
        public Node<V> leftChild() {
            return leftChild;
        }

        @Override
        public Node<V> rightChild() {
            return rightChild;
        }

        public void setValue(V newValue) {
            value = newValue;
        }

        public Entry<V> next() {
            Entry<V> node = leftMost(rightChild);

            if (node == null) {
                node = parent;
                for (byte flags = this.flags; node != null; node = node.parent) {
                    if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                        break;
                    }
                    flags = node.flags;
                }
            }

            return node;
        }

        public Entry<V> previous() {
            Entry<V> node = rightMost(leftChild);

            if (node == null) {
                node = parent;
                for (byte flags = this.flags; node != null; node = node.parent) {
                    if ((flags & FLAG_NODE_IS_LEFT_CHILD) == 0){
                        break;
                    }
                    flags = node.flags;
                }
            }

            return node;
        }

        public static <V> Entry<V> leftMost(Entry<V> subtreeRoot) {
            Entry<V> node = subtreeRoot;
            if (node != null) {
                for (;;) {
                    final Entry<V> nextNode = node.leftChild;
                    if (nextNode == null) {
                        break;
                    } else {
                        node = nextNode;
                    }
                }
            }

            return node;
        }

        public static <V> Entry<V> rightMost(Entry<V> subtreeRoot) {
            Entry<V> node = subtreeRoot;
            if (node != null) {
                for (;;) {
                    final Entry<V> nextNode = node.rightChild;
                    if (nextNode == null) {
                        break;
                    } else {
                        node = nextNode;
                    }
                }
            }

            return node;
        }

        @Override
        public String toString() {
            final StringBuilder strBld = new StringBuilder("[");
            strBld.append(index()).append("(+").append(index).append(")]");

            if ((flags & FLAG_NODE_IS_LEFT_CHILD) == FLAG_NODE_IS_LEFT_CHILD) {
                if ((flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                    strBld.append("(l,r)");
                } else {
                    strBld.append("(l)");
                }
            } else {
                if ((flags & FLAG_NODE_IS_RED) == FLAG_NODE_IS_RED) {
                    strBld.append("(r)");
                }
            }

            strBld.append(": ").append(value);
            return strBld.toString();
        }

        private Entry(int index,
                      V value,
                      byte flags,
                      Entry<V> parent) {
            this.index = index;
            this.value = value;
            this.flags = flags;
            this.parent = parent;
        }
    }
}
