/**
 * Author: Roman Panov
 * Date:   11/18/12
 */

package org.roman.util;

public class Segment {

    public int offset;
    public int length;

    public Segment() {
        this(0, 0);
    }

    public Segment(int offset, int length) {
        this.offset = offset;
        this.length = length;
    }

    public void assign(Segment anotherSegment) {
        offset = anotherSegment.offset;
        length = anotherSegment.length;
    }
}
