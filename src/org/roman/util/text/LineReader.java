/**
 * Author: Roman Panov
 * Date:   3/3/13
 */

package org.roman.util.text;

import java.io.IOException;
import java.io.Reader;
import java.text.CharacterIterator;

public final class LineReader {
    private static final int DEFAULT_BUFFER_SIZE = 0x800;
    private static final char DONE = CharacterIterator.DONE;

    private Reader sourceReader;
    char[] buffer = null;
    int charsInBuffer;
    int currPosInBuffer;
    char currChar;
    boolean hasStarted;
    boolean isDone;

    public LineReader(Reader sourceReader) {
        reset(sourceReader);
    }

    public LineReader(Reader sourceReader, int bufferSize) {
        reset(sourceReader, bufferSize);
    }

    public int readLine(Appendable target) throws IOException {
        if (isDone) {
            return -1;
        }
        if (!hasStarted) {
            fillBuffer();
            currChar = charsInBuffer > 0 ? buffer[0] : DONE;
            hasStarted = true;
        }

        int lineLength = 0;

        // Try to locate where the next line starts.
        for (;;) {
            switch (currChar) {
                case CharacterIterator.DONE:
                    isDone = true;
                    return lineLength;

                case Characters.CARRIAGE_RETURN:
                    currChar = nextChar();
                    if (currChar == Characters.LINE_FEED) {
                        // This line ends with CR+LF.
                        currChar = nextChar();
                    }

                    return lineLength;

                case Characters.LINE_FEED:
                    currChar = nextChar();
                    if (currChar == Characters.CARRIAGE_RETURN) {
                        // This line ends with LF+CR.
                        currChar = nextChar();
                    }

                    return lineLength;

                default:
                    // Just ordinary character.
                    target.append(currChar);
                    ++lineLength;
                    currChar = nextChar();
            }
        }
    }

    public LineReader reset(Reader sourceReader) {
        return reset(sourceReader, DEFAULT_BUFFER_SIZE);
    }

    public LineReader reset(Reader sourceReader, int bufferSize) {
        if (bufferSize <= 0) {
            throw new IllegalArgumentException("Illegal buffer size: " + bufferSize + ".");
        }

        this.sourceReader = sourceReader;
        charsInBuffer = 0;
        currPosInBuffer = 0;
        currChar = DONE;
        hasStarted = false;
        isDone = false;

        if (buffer == null || buffer.length != bufferSize) {
            buffer = new char[bufferSize];
        }

        return this;
    }

    private char nextChar() throws IOException {
        ++currPosInBuffer;
        if (currPosInBuffer >= charsInBuffer) {
            if (hasStarted && charsInBuffer < buffer.length) {
                return DONE;
            }
            fillBuffer();
            currPosInBuffer = 0;
            hasStarted = true;
        }

        return buffer[currPosInBuffer];
    }

    private void fillBuffer() throws IOException {
        charsInBuffer = 0;
        int charsToRead = buffer.length;

        while (charsToRead > 0) {
            final int charsRead = sourceReader.read(buffer, charsInBuffer, charsToRead);
            if (charsRead < 0) {
                break;
            }
            charsInBuffer += charsRead;
            charsToRead -= charsRead;
        }
    }
}
