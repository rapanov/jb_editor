/**
 * Author: Roman Panov
 * Date:   1/4/13
 */

package org.roman.util.text;

public class Characters {
    public static final char BACKSPACE            = 0x0008;
    public static final char CHARACTER_TABULATION = 0x0009;
    public static final char LINE_FEED            = 0x000a;
    public static final char LINE_TABULATION      = 0x000b;
    public static final char FORM_FEED            = 0x000c;
    public static final char CARRIAGE_RETURN      = 0x000d;
    public static final char ESCAPE               = 0x001b;
    public static final char SPACE                = 0x0020;
    public static final char DOUBLE_QUOTE         = 0x0022;
    public static final char SINGLE_QUOTE         = 0x0027;
    public static final char BACKSLASH            = 0x005c;
    public static final char DELETE               = 0x007f;
}
