/**
 * Author: Roman Panov
 * Date:   2/17/13
 */

package org.roman.util.text;

import java.io.Reader;
import java.text.CharacterIterator;

public final class TextReader extends Reader {
    private CharacterIterator source;
    private int charsRead = 0;
    private int maxCharsToRead = Integer.MAX_VALUE;
    private Marker marker = new Marker();

    public TextReader(CharacterIterator source) {
        reset(source);
    }

    public TextReader(CharacterIterator source, int charCount) {
        reset(source, charCount);
    }

    @Override
    public void close() {
    }

    @Override
    public void mark(int readAheadLimit) {
        marker.sourceIndex = source.getIndex();
        marker.charsRead = charsRead;
    }

    @Override
    public boolean markSupported() {
        return true;
    }

    @Override
    public int read() {
        char ch = source.current();
        if (ch != CharacterIterator.DONE && charsRead < maxCharsToRead) {
            ++charsRead;
            source.next();
            return ch;
        }
        return -1;
    }

    @Override
    public int read(char[] buffer, int offset, int length) {
        if (charsRead < maxCharsToRead) {
            char ch = source.current();
            if (ch != CharacterIterator.DONE) {
                length = Math.min(length, maxCharsToRead - charsRead);
                int buffIdx = offset;
                for (final int buffMaxIdx = offset + length;
                     buffIdx < buffMaxIdx && ch != CharacterIterator.DONE;
                     ++buffIdx, ch = source.next()) {
                    buffer[buffIdx] = ch;
                }

                final int charCount = buffIdx - offset;
                charsRead += charCount;
                return charCount;
            }
        }

        return -1;
    }

    @Override
    public boolean ready() {
        return true;
    }

    @Override
    public void reset() {
        source.setIndex(marker.sourceIndex);
        charsRead = marker.charsRead;
    }

    @Override
    public long skip(long count) {
        if (count < 0L) {
            throw new IllegalArgumentException("Skip value is negative.");
        }

        final int sourceIndex = source.getIndex();
        int skip = Math.min(source.getEndIndex() - sourceIndex,
                            maxCharsToRead - charsRead);
        if (count < (long)skip) {
            skip = (int)count;
        }

        source.setIndex(sourceIndex + skip);
        charsRead += skip;
        return skip;
    }

    public Reader reset(CharacterIterator source) {
        return reset(source, Integer.MAX_VALUE);
    }

    public Reader reset(CharacterIterator source, int charCount) {
        this.source = source;
        charsRead = 0;
        maxCharsToRead = charCount;
        marker.sourceIndex = source.getIndex();
        marker.charsRead = 0;
        return this;
    }

    private static final class Marker {
        int sourceIndex;
        int charsRead;
    }
}
