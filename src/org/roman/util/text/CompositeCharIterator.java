/**
 * Author: Roman Panov
 * Date:   2/10/13
 */

package org.roman.util.text;

import java.text.CharacterIterator;

public final class CompositeCharIterator implements CharacterIterator {
    private CharacterIterator firstPartIterator;
    private CharacterIterator secondPartIterator;
    private CharacterIterator currentPartIterator;
    int index;

    public CompositeCharIterator(CharacterIterator firstPartIterator,
                                 CharacterIterator secondPartIterator) {
        reset(firstPartIterator,  secondPartIterator);
    }

    @Override
    public Object clone() {
        final CharacterIterator clonedFirstPartIterator =
            (CharacterIterator)firstPartIterator.clone();
        final CharacterIterator clonedSecondPartIterator =
            (CharacterIterator)secondPartIterator.clone();
        return new CompositeCharIterator(clonedFirstPartIterator,
                                         clonedSecondPartIterator,
                                         currentPartIterator == firstPartIterator
                                         ? clonedFirstPartIterator
                                         : clonedSecondPartIterator,
                                         index);
    }

    @Override
    public char current() {
        return currentPartIterator.current();
    }

    @Override
    public char first() {
        index = firstPartIterator.getBeginIndex();
        char result = firstPartIterator.first();

        if (result == DONE) {
            result = secondPartIterator.first();
            currentPartIterator = secondPartIterator;
        } else {
            currentPartIterator = firstPartIterator;
        }

        return result;
    }

    @Override
    public int getBeginIndex() {
        return firstPartIterator.getBeginIndex();
    }

    @Override
    public int getEndIndex() {
        return firstPartIterator.getEndIndex() +
               secondPartIterator.getEndIndex() -
               secondPartIterator.getBeginIndex();
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public char last() {
        index = getEndIndex();
        char result = secondPartIterator.last();

        if (result == DONE) {
            result = firstPartIterator.last();
            currentPartIterator = firstPartIterator;
        } else {
            currentPartIterator = secondPartIterator;
        }

        return result;
    }

    @Override
    public char next() {
        ++index;
        char result = currentPartIterator.next();
        if (result == DONE && currentPartIterator == firstPartIterator) {
            result = secondPartIterator.first();
            currentPartIterator = secondPartIterator;
        }

        return result;
    }

    @Override
    public char previous() {
        --index;
        char result = currentPartIterator.previous();
        if (result == DONE && currentPartIterator == secondPartIterator) {
            result = firstPartIterator.last();
            currentPartIterator = firstPartIterator;
        }

        return result;
    }

    @Override
    public char setIndex(int index) {
        this.index = index;
        if (index < firstPartIterator.getEndIndex()) {
            currentPartIterator = firstPartIterator;
            return firstPartIterator.setIndex(index);
        }

        currentPartIterator = secondPartIterator;
        return secondPartIterator.setIndex(index - (firstPartIterator.getEndIndex() -
                                                    firstPartIterator.getBeginIndex()));
    }

    public CompositeCharIterator reset(CharacterIterator firstPartIterator,
                                       CharacterIterator secondPartIterator) {
        this.firstPartIterator = firstPartIterator;
        this.secondPartIterator = secondPartIterator;
        currentPartIterator = firstPartIterator;
        index = firstPartIterator.getIndex();
        return this;
    }

    private CompositeCharIterator(CharacterIterator firstPartIterator,
                                  CharacterIterator secondPartIterator,
                                  CharacterIterator currentPartIterator,
                                  int index) {
        this.firstPartIterator = firstPartIterator;
        this.secondPartIterator = secondPartIterator;
        this.currentPartIterator = currentPartIterator;
        this.index = index;
    }
}
