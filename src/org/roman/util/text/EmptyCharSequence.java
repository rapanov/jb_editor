/**
 * Author: Roman Panov
 * Date:   11/17/12
 */

package org.roman.util.text;

import java.text.CharacterIterator;

final class EmptyCharSequence implements IterableCharSequence {

    @Override
    public boolean equals(Object o) {
        return (o instanceof EmptyCharSequence);
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public char charAt(int index) {
        throw new StringIndexOutOfBoundsException(index);
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public IterableCharSequence subSequence(int start, int end) {
        if (start != 0) {
            throw new StringIndexOutOfBoundsException(start);
        }

        if (end != 0) {
            throw new StringIndexOutOfBoundsException(end);
        }

        return TextUtil.EMPTY_CHAR_SEQUENCE;
    }

    @Override
    public String toString() {
        return TextUtil.EMPTY_STRING;
    }

    @Override
    public CharacterIterator iterator(CharacterIterator target) {
        return target instanceof EmptyCharIterator ? ((EmptyCharIterator)target).reset()
                                                   : new EmptyCharIterator();
    }
}
