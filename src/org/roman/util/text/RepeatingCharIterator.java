/**
 * Author: Roman Panov
 * Date:   2/10/13
 */

package org.roman.util.text;

import java.text.CharacterIterator;

public final class RepeatingCharIterator implements CharacterIterator {
    private char ch;
    private int count;
    private int index;

    public RepeatingCharIterator(char ch, int count) {
        reset(ch, count);
    }

    @Override
    public Object clone() {
        return new RepeatingCharIterator(ch, count, index);
    }

    @Override
    public char current() {
        return (index >= 0 && index < count) ? ch : DONE;
    }

    @Override
    public char first() {
        index = 0;
        return current();
    }

    @Override
    public int getBeginIndex() {
        return 0;
    }

    @Override
    public int getEndIndex() {
        return count;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public char last() {
        index = count > 0 ? count - 1 : 0;
        return current();
    }

    @Override
    public char next() {
        ++index;
        return current();
    }

    @Override
    public char previous() {
        --index;
        return current();
    }

    @Override
    public char setIndex(int index) {
        this.index = index;
        return current();
    }

    public CharacterIterator reset(char ch) {
        return reset(ch, this.count);
    }

    public CharacterIterator reset(int count) {
        return reset(this.ch, count);
    }

    public RepeatingCharIterator reset(char ch, int count) {
        if (count < 0) {
            throw new IllegalArgumentException("Negative value for 'char count' is illegal.");
        }

        this.ch = ch;
        this.count = count;
        index = 0;
        return this;
    }

    private RepeatingCharIterator(char ch, int count, int index) {
        this.ch = ch;
        this.count = count;
        this.index = index;
    }
}
