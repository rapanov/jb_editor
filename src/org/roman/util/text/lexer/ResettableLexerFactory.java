/**
 * Author: Roman Panov
 * Date:   2/17/13
 */

package org.roman.util.text.lexer;

public interface ResettableLexerFactory<S> {
    ResettableLexer<S> createLexer(S source);
}
