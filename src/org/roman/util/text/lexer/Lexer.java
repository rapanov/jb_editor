/**
 * Author: Roman Panov
 * Date:   2/16/13
 */

package org.roman.util.text.lexer;

public interface Lexer {

    /** End of stream. */
    public static final int TOKEN_TYPE_EOF = -1;

    int nextToken() throws LexerException;
    int tokenOffset();
    int tokenLength();
}
