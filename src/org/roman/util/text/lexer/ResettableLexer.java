/**
 * Author: Roman Panov
 * Date:   2/16/13
 */

package org.roman.util.text.lexer;

public interface ResettableLexer<S> extends Lexer {
    void reset(S newSource) throws LexerException;
}
