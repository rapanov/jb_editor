/**
 * Author: Roman Panov
 * Date:   2/16/13
 */

package org.roman.util.text.lexer.java;

import org.roman.util.text.Characters;
import org.roman.util.text.lexer.LexerException;

/**
 * This class is a simple example lexer.
 */
%%

%class JavaLexerImpl
%extends JavaLexer
%final
%integer
%unicode
%char
%buffer 2048
%scanerror LexerException

%{
    private int tokenType = TOKEN_TYPE_EOF;
    private int tokenOffset = -1;
    private int tokenLength = 0;
    private int charLiteral = -1;
    private StringBuilder stringLiteral = new StringBuilder();
    private boolean isStringLiteralMalformed = false;

    @Override
    public int nextToken() throws LexerException {
        try {
            return yylex();
        } catch (java.io.IOException exception) {
            throw new LexerException(exception);
        }
    }

    @Override
    public int tokenOffset() {
        return tokenOffset;
    }

    @Override
    public int tokenLength() {
        return tokenLength;
    }

    @Override
    public int charLiteral() {
        return charLiteral;
    }

    @Override
    public CharSequence stringLiteral() {
        return stringLiteral;
    }

    @Override
    public void reset(java.io.Reader reader) {
        yyreset(reader);
    }

    private int processTokenInInitialState(int tokenType) {
        this.tokenType = tokenType;
        tokenOffset = yychar;
        tokenLength = yylength();
        return tokenType;
    }
%}

LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]

BooleanLiteral         = true | false
IntegerLiteral         = {DecIntegerLiteral} | {HexIntegerLiteral} | {OctIntegerLiteral}
LongLiteral            = {DecLongLiteral} | {HexLongLiteral} | {OctLongLiteral}
FloatLiteral           = ({Float1}|{Float2}|{Float3}) {Exponent}? [fF]
DoubleLiteral          = ({Float1}|{Float2}|{Float3}) {Exponent}?
MalformedFloatLiteral  = ({Float1}|{Float2}|{Float3}) {IncompleteExponent}? [fF]
MalformedDoubleLiteral = ({Float1}|{Float2}|{Float3}) {IncompleteExponent}?

DecIntegerLiteral = 0 | [1-9][0-9]*
HexIntegerLiteral = 0 [xX] 0* {HexDigit} {1,8}
OctIntegerLiteral = 0+ [1-3]? {OctDigit} {1,15}
DecLongLiteral    = {DecIntegerLiteral} [lL]
HexLongLiteral    = 0 [xX] 0* {HexDigit} {1,16} [lL]
OctLongLiteral    = 0+ 1? {OctDigit} {1,21} [lL]

OctDigit = [0-7]
HexDigit = [0-9a-fA-F]

Float1   = [0-9]+ \. [0-9]*
Float2   = \. [0-9]+
Float3   = [0-9]+
Exponent = [eE] [+-]? [0-9]+
IncompleteExponent = [eE] [+-]?

SingleChar = [^\n\r\'\\]
StringChar = [^\n\r\"\\]

Identifier = [:jletter:] [:jletterdigit:]*

%state CHAR
%state STRING
%state TRADITIONAL_COMMENT
%state SINGLE_LINE_COMMENT
%state DOCUMENTATION_COMMENT

%%

/* Keywords. */
<YYINITIAL> {
    "abstract" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_ABSTRACT);
    }

    "assert" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_ASSERT);
    }

    "boolean" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_BOOLEAN);
    }

    "break" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_BREAK);
    }

    "byte" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_BYTE);
    }

    "case" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_CASE);
    }

    "catch" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_CATCH);
    }

    "char" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_CHAR);
    }

    "class" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_CLASS);
    }

    "const" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_CONST);
    }

    "continue" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_CONTINUE);
    }

    "default" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_DEFAULT);
    }

    "do" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_DO);
    }

    "double" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_DOUBLE);
    }

    "else" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_ELSE);
    }

    "enum" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_ENUM);
    }

    "extends" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_EXTENDS);
    }

    "final" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_FINAL);
    }

    "finally" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_FINALLY);
    }

    "float" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_FLOAT);
    }

    "for" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_FOR);
    }

    "goto" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_GOTO);
    }

    "if" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_IF);
    }

    "implements" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_IMPLEMENTS);
    }

    "import" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_IMPORT);
    }

    "instanceof" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_INSTANCEOF);
    }

    "int" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_INT);
    }

    "interface" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_INTERFACE);
    }

    "long" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_LONG);
    }

    "native" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_NATIVE);
    }

    "new" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_NEW);
    }

    "package" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_PACKAGE);
    }

    "private" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_PRIVATE);
    }

    "protected" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_PROTECTED);
    }

    "public" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_PUBLIC);
    }

    "return" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_RETURN);
    }

    "short" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_SHORT);
    }

    "static" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_STATIC);
    }

    "strictfp" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_STRICTFP);
    }

    "super" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_SUPER);
    }

    "switch" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_SWITCH);
    }

    "synchronized" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_SYNCHRONIZED);
    }

    "this" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_THIS);
    }

    "throw" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_THROW);
    }

    "throws" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_THROWS);
    }

    "transient" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_TRANSIENT);
    }

    "try" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_TRY);
    }

    "void" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_VOID);
    }

    "volatile" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_VOLATILE);
    }

    "while" {
        return processTokenInInitialState(TOKEN_TYPE_KEYWORD_WHILE);
    }
}

/* Literals. */
<YYINITIAL> {
    \' {
            tokenType = TOKEN_TYPE_CHAR_LITERAL;
            tokenOffset = yychar;
            yybegin(CHAR);
        }

    \" {
        tokenType = TOKEN_TYPE_STRING_LITERAL;
        tokenOffset = yychar;
        stringLiteral.setLength(0);
        yybegin(STRING);
    }

    {BooleanLiteral} {
        return processTokenInInitialState(TOKEN_TYPE_BOOLEAN_LITERAL);
    }

    {IntegerLiteral} {
        return processTokenInInitialState(TOKEN_TYPE_INTEGER_LITERAL);
    }

    0[x|X] {
        return processTokenInInitialState(TOKEN_TYPE_MALFORMED_INTEGER_LITERAL);
    }

    {LongLiteral} {
        return processTokenInInitialState(TOKEN_TYPE_LONG_LITERAL);
    }

    {FloatLiteral} {
        return processTokenInInitialState(TOKEN_TYPE_FLOAT_LITERAL);
    }

    {DoubleLiteral} {
        return processTokenInInitialState(TOKEN_TYPE_DOUBLE_LITERAL);
    }

    {MalformedFloatLiteral} {
        return processTokenInInitialState(TOKEN_TYPE_MALFORMED_FLOAT_LITERAL);
    }

    {MalformedDoubleLiteral} {
        return processTokenInInitialState(TOKEN_TYPE_MALFORMED_DOUBLE_LITERAL);
    }
}

/* Identifiers. */
<YYINITIAL> {Identifier} {
    return processTokenInInitialState(TOKEN_TYPE_IDENTIFIER);
}

/* Comments. */
<YYINITIAL> {
    "/**/" {
        return processTokenInInitialState(TOKEN_TYPE_TRADITIONAL_COMMENT);
    }

    "/*" {
        tokenType = TOKEN_TYPE_TRADITIONAL_COMMENT;
        tokenOffset = yychar;
        yybegin(TRADITIONAL_COMMENT);
    }

    "//" {
        tokenType = TOKEN_TYPE_SINGLE_LINE_COMMENT;
        tokenOffset = yychar;
        yybegin(SINGLE_LINE_COMMENT);
    }

    "/**" {
        tokenType = TOKEN_TYPE_DOCUMENTATION_COMMENT;
        tokenOffset = yychar;
        yybegin(DOCUMENTATION_COMMENT);
    }
}

<YYINITIAL> {WhiteSpace} {
    /* Ignore. */
}

<CHAR> {
    {SingleChar}\' {
        charLiteral = yycharat(0);
        tokenLength = yychar + 2 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    \' {
        charLiteral = -1;
        tokenLength = yychar + 1 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_MALFORMED_CHAR_LITERAL;
    }

    LineTerminator {
        charLiteral = -1;
        tokenLength = yychar - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_MALFORMED_CHAR_LITERAL;
    }

    {SingleChar}+ {
        charLiteral = -1;
    }

    "\\b"\' {
        charLiteral = Characters.BACKSPACE;
        tokenLength = yychar + 3 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    "\\t"\' {
        charLiteral = Characters.CHARACTER_TABULATION;
        tokenLength = yychar + 3 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    "\\n"\' {
        charLiteral = Characters.LINE_FEED;
        tokenLength = yychar + 3 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    "\\f"\' {
        charLiteral = Characters.FORM_FEED;
        tokenLength = yychar + 3 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    "\\r"\' {
        charLiteral = Characters.CARRIAGE_RETURN;
        tokenLength = yychar + 3 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    "\\\""\' {
        charLiteral = Characters.DOUBLE_QUOTE;
        tokenLength = yychar + 3 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    "\\'"\' {
        charLiteral = Characters.SINGLE_QUOTE;
        tokenLength = yychar + 3 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    "\\\\"\' {
        charLiteral = Characters.BACKSLASH;
        tokenLength = yychar + 3 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    \\[0-3]?{OctDigit}?{OctDigit}\' {
        final String tokenStr = yytext();
        charLiteral = (char)Integer.parseInt(tokenStr.substring(1, tokenStr.length() - 1), 8);
        tokenLength = yychar + yylength() - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    \\u00(0[A|a|D|d]|27|5[c|C])\' {
        // Line feed, carriage return, single quote and backslash cannot be represented as
        // Unicode escape sequences inside a character literal. So this character literal is to
        // be considered malformed.
        charLiteral = -1;
        tokenLength = yychar + yylength() - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_MALFORMED_CHAR_LITERAL;
    }

    \\u{HexDigit}{4}\' {
        charLiteral = (char)Integer.parseInt(yytext().substring(2, 6), 16);
        tokenLength = yychar + yylength() - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_CHAR_LITERAL;
    }

    "\\'" {
        // Escaped closing quote - consider the character literal is to be continued, though
        // it is definitely malformed.
        charLiteral = -1;
    }

    \\ {
        // Seems to be an illegal escape sequence.
        charLiteral = -1;
    }

    .|\n {
        charLiteral = -1;
        tokenLength = yychar - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_MALFORMED_CHAR_LITERAL;
    }
}

<STRING> {
    \" {
        tokenLength = yychar + 1 - tokenOffset;
        yybegin(YYINITIAL);
        final int tokenType = isStringLiteralMalformed ? TOKEN_TYPE_MALFORMED_STRING_LITERAL
                                                       : TOKEN_TYPE_STRING_LITERAL;
        isStringLiteralMalformed = false;
        return tokenType;
    }

    LineTerminator {
        tokenLength = yychar - tokenOffset;
        yybegin(YYINITIAL);
        isStringLiteralMalformed = false;
        return TOKEN_TYPE_MALFORMED_STRING_LITERAL;
    }

    {StringChar}+ {
        stringLiteral.append(yytext());
    }

    "\\b" {
        stringLiteral.append(Characters.BACKSPACE);
    }

    "\\t" {
        stringLiteral.append(Characters.CHARACTER_TABULATION);
    }

    "\\n" {
        stringLiteral.append(Characters.LINE_FEED);
    }

    "\\f" {
        stringLiteral.append(Characters.FORM_FEED);
    }

    "\\r" {
        stringLiteral.append(Characters.CARRIAGE_RETURN);
    }

    "\\\"" {
        stringLiteral.append(Characters.DOUBLE_QUOTE);
    }

    "\\'" {
        stringLiteral.append(Characters.SINGLE_QUOTE);
    }

    "\\\\" {
        stringLiteral.append(Characters.BACKSLASH);
    }

    \\[0-3]?{OctDigit}?{OctDigit} {
        final char ch = (char)Integer.parseInt(yytext().substring(1), 8);
        stringLiteral.append(ch);
    }

    \\u00(0[A|a|D|d]|22|5[c|C]) {
        // Line feed, carriage return, double quote and backslash cannot be represented as
        // Unicode escape sequences inside a string literal. So this string literal is to
        // be considered malformed.
        isStringLiteralMalformed = true;
    }

    \\u{HexDigit}{4} {
        final char ch = (char)Integer.parseInt(yytext().substring(2, 6), 16);
        stringLiteral.append(ch);
    }

    \\ {
        // Seems to be an illegal escape sequence.
        isStringLiteralMalformed = true;
        stringLiteral.append(Characters.BACKSLASH);
    }

    .|\n {
        tokenLength = yychar - tokenOffset;
        yybegin(YYINITIAL);
        isStringLiteralMalformed = false;
        return TOKEN_TYPE_MALFORMED_STRING_LITERAL;
    }
}

<TRADITIONAL_COMMENT> {
    "*/" {
        tokenLength = yychar + 2 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_TRADITIONAL_COMMENT;
    }

    . {
    }
}

<SINGLE_LINE_COMMENT> {
    LineTerminator {
        tokenLength = yychar - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_SINGLE_LINE_COMMENT;
    }

    . {
    }

    .|\n {
        tokenLength = yychar - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_SINGLE_LINE_COMMENT;
    }
}

<DOCUMENTATION_COMMENT> {
    "*/" {
        tokenLength = yychar + 2 - tokenOffset;
        yybegin(YYINITIAL);
        return TOKEN_TYPE_DOCUMENTATION_COMMENT;
    }

    . {
    }
}

<<EOF>> {
    final int state = yystate();

    switch (state) {
        case CHAR:
            tokenLength = yychar - tokenOffset;
            yybegin(YYINITIAL);
            return TOKEN_TYPE_MALFORMED_CHAR_LITERAL;

        case STRING:
            tokenLength = yychar - tokenOffset;
            yybegin(YYINITIAL);
            return TOKEN_TYPE_MALFORMED_STRING_LITERAL;

        case TRADITIONAL_COMMENT:
            tokenLength = yychar - tokenOffset;
            yybegin(YYINITIAL);
            return TOKEN_TYPE_TRADITIONAL_COMMENT;

        case SINGLE_LINE_COMMENT:
            tokenLength = yychar - tokenOffset;
            yybegin(YYINITIAL);
            return TOKEN_TYPE_SINGLE_LINE_COMMENT;

        case DOCUMENTATION_COMMENT:
            tokenLength = yychar - tokenOffset;
            yybegin(YYINITIAL);
            return TOKEN_TYPE_DOCUMENTATION_COMMENT;
    }

    return TOKEN_TYPE_EOF;
}

/* Unknown character. */
.|\n {
    // Ignore it.
}