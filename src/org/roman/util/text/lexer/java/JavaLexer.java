/**
 * Author: Roman Panov
 * Date:   2/16/13
 */

package org.roman.util.text.lexer.java;

import java.io.Reader;
import org.roman.util.text.lexer.ResettableLexer;

public abstract class JavaLexer implements ResettableLexer<Reader> {

    /** Keyword <tt>abstract</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_ABSTRACT = 0x100;

    /** Keyword <tt>assert</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_ASSERT = 0x101;

    /** Keyword <tt>boolean</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_BOOLEAN = 0x102;

    /** Keyword <tt>break</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_BREAK = 0x103;

    /** Keyword <tt>byte</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_BYTE = 0x104;

    /** Keyword <tt>case</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_CASE = 0x105;

    /** Keyword <tt>catch</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_CATCH = 0x106;

    /** Keyword <tt>char</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_CHAR = 0x107;

    /** Keyword <tt>class</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_CLASS = 0x108;

    /** Keyword <tt>const</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_CONST = 0x109;

    /** Keyword <tt>continue</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_CONTINUE = 0x10a;

    /** Keyword <tt>default</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_DEFAULT = 0x10b;

    /** Keyword <tt>do</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_DO = 0x10c;

    /** Keyword <tt>double</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_DOUBLE = 0x10d;

    /** Keyword <tt>else</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_ELSE = 0x10e;

    /** Keyword <tt>enum</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_ENUM = 0x10f;

    /** Keyword <tt>extends</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_EXTENDS = 0x110;

    /** Keyword <tt>final</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_FINAL = 0x111;

    /** Keyword <tt>finally</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_FINALLY = 0x112;

    /** Keyword <tt>float</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_FLOAT = 0x113;

    /** Keyword <tt>for</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_FOR = 0x114;

    /** Keyword <tt>goto</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_GOTO = 0x115;

    /** Keyword <tt>if</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_IF = 0x116;

    /** Keyword <tt>implements</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_IMPLEMENTS = 0x117;

    /** Keyword <tt>import</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_IMPORT = 0x118;

    /** Keyword <tt>instanceof</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_INSTANCEOF = 0x119;

    /** Keyword <tt>int</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_INT = 0x11a;

    /** Keyword <tt>interface</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_INTERFACE = 0x11b;

    /** Keyword <tt>long</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_LONG = 0x11c;

    /** Keyword <tt>native</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_NATIVE = 0x11d;

    /** Keyword <tt>new</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_NEW = 0x11e;

    /** Keyword <tt>package</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_PACKAGE = 0x11f;

    /** Keyword <tt>private</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_PRIVATE = 0x120;

    /** Keyword <tt>protected</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_PROTECTED = 0x121;

    /** Keyword <tt>public</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_PUBLIC = 0x122;

    /** Keyword <tt>return</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_RETURN = 0x123;

    /** Keyword <tt>short</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_SHORT = 0x124;

    /** Keyword <tt>static</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_STATIC = 0x125;

    /** Keyword <tt>strictfp</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_STRICTFP = 0x126;

    /** Keyword <tt>super</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_SUPER = 0x127;

    /** Keyword <tt>switch</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_SWITCH = 0x128;

    /** Keyword <tt>synchronized</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_SYNCHRONIZED = 0x129;

    /** Keyword <tt>this</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_THIS = 0x12a;

    /** Keyword <tt>throw</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_THROW = 0x12b;

    /** Keyword <tt>throws</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_THROWS = 0x12c;

    /** Keyword <tt>transient</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_TRANSIENT = 0x12d;

    /** Keyword <tt>try</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_TRY = 0x12e;

    /** Keyword <tt>void</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_VOID = 0x12f;

    /** Keyword <tt>volatile</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_VOLATILE = 0x130;

    /** Keyword <tt>while</tt>. */
    public static final int TOKEN_TYPE_KEYWORD_WHILE = 0x131;

    /** An identifier. */
    public static final int TOKEN_TYPE_IDENTIFIER = 0x200;

    /** A well-formatted character literal. */
    public static final int TOKEN_TYPE_CHAR_LITERAL = 0x300;

    /** A malformed character literal. */
    public static final int TOKEN_TYPE_MALFORMED_CHAR_LITERAL = 0x301;

    /** A well-formatted string literal. */
    public static final int TOKEN_TYPE_STRING_LITERAL = 0x302;

    /** A malformed string literal. */
    public static final int TOKEN_TYPE_MALFORMED_STRING_LITERAL = 0x303;

    /** A <tt>boolean</tt> literal. */
    public static final int TOKEN_TYPE_BOOLEAN_LITERAL = 0x304;

    /** An <tt>int</tt> literal. */
    public static final int TOKEN_TYPE_INTEGER_LITERAL = 0x305;

    /** A malformed <tt>int</tt> literal. */
    public static final int TOKEN_TYPE_MALFORMED_INTEGER_LITERAL = 0x306;

    /** A <tt>long</tt> literal. */
    public static final int TOKEN_TYPE_LONG_LITERAL = 0x307;

    /** A <tt>float</tt> literal. */
    public static final int TOKEN_TYPE_FLOAT_LITERAL = 0x308;

    /** A malformed <tt>float</tt> literal. */
    public static final int TOKEN_TYPE_MALFORMED_FLOAT_LITERAL = 0x309;

    /** A <tt>double</tt> literal. */
    public static final int TOKEN_TYPE_DOUBLE_LITERAL = 0x310;

    /** A malformed <tt>double</tt> literal. */
    public static final int TOKEN_TYPE_MALFORMED_DOUBLE_LITERAL = 0x311;

    /** Traditional comment. */
    public static final int TOKEN_TYPE_TRADITIONAL_COMMENT = 0x700;

    /** Single-line comment. */
    public static final int TOKEN_TYPE_SINGLE_LINE_COMMENT = 0x701;

    /** Documentation comment. */
    public static final int TOKEN_TYPE_DOCUMENTATION_COMMENT = 0x702;

    private static final int FIRST_KEYWORD = TOKEN_TYPE_KEYWORD_ABSTRACT;
    private static final int LAST_KEYWORD = TOKEN_TYPE_KEYWORD_WHILE;

    public abstract int charLiteral();
    public abstract CharSequence stringLiteral();

    public static JavaLexer create(Reader reader) {
        return new JavaLexerImpl(reader);
    }

    public static boolean isKeyword(int tokenType) {
        return (tokenType >= FIRST_KEYWORD && tokenType <= LAST_KEYWORD);
    }

    public static boolean isCharLiteral(int tokenType) {
        return (tokenType == TOKEN_TYPE_CHAR_LITERAL ||
                tokenType == TOKEN_TYPE_MALFORMED_CHAR_LITERAL);
    }

    public static boolean isStringLiteral(int tokenType) {
        return (tokenType == TOKEN_TYPE_STRING_LITERAL ||
                tokenType == TOKEN_TYPE_MALFORMED_STRING_LITERAL);
    }

    public static boolean isNumericLiteral(int tokenType) {
        return (tokenType >= TOKEN_TYPE_INTEGER_LITERAL &&
                tokenType <= TOKEN_TYPE_MALFORMED_DOUBLE_LITERAL);
    }

    public static boolean isComment(int tokenType) {
        return (tokenType >= TOKEN_TYPE_TRADITIONAL_COMMENT &&
                tokenType <= TOKEN_TYPE_DOCUMENTATION_COMMENT);
    }
}
