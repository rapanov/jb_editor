/**
 * Author: Roman Panov
 * Date:   2/16/13
 */

package org.roman.util.text.lexer;

public class LexerException extends Exception {
    public LexerException() {
    }

    public LexerException(String message) {
        super(message);
    }

    public LexerException(Throwable cause) {
        super(cause);
    }

    public LexerException(String message, Throwable cause) {
        super(message, cause);
    }
}
