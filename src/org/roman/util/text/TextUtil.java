/**
 * Author: Roman Panov
 * Date:   12/1/12
 */

package org.roman.util.text;

import java.nio.CharBuffer;

public class TextUtil {
    public static final String EMPTY_STRING = "";
    public static final char[] EMPTY_CHAR_ARRAY = new char[0];
    public static final IterableCharSequence EMPTY_CHAR_SEQUENCE = new EmptyCharSequence();

    public static IterableCharSequence createCharSequence(char[] array) {
        return new CharArraySequence(array);
    }

    public static IterableCharSequence createCharSequence(char[] array, int offset, int length) {
        return new CharArraySequence(array, offset, length);
    }

    public static IterableCharSequence toIterableCharSequence(CharSequence charSequence) {
        if (charSequence instanceof IterableCharSequence) {
            return (IterableCharSequence)charSequence;
        }

        if (charSequence instanceof CharBuffer) {
            final CharBuffer buffer = (CharBuffer)charSequence;

            if (buffer.hasArray() && !buffer.isReadOnly()) {
                return createCharSequence(buffer.array(), buffer.arrayOffset(), buffer.length());
            }
        }

        return new SimpleIterableCharSequence(charSequence);
    }
}
