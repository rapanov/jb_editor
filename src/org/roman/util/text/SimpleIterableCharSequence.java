/**
 * Author: Roman Panov
 * Date:   12/1/12
 */

package org.roman.util.text;

import java.text.CharacterIterator;

final class SimpleIterableCharSequence implements IterableCharSequence {

    private final CharSequence charSequence;

    @Override
    public char charAt(int index) {
        return charSequence.charAt(index);
    }

    @Override
    public int length() {
        return charSequence.length();
    }

    @Override
    public IterableCharSequence subSequence(int start, int end) {
        return new SimpleIterableCharSequence(charSequence.subSequence(start, end));
    }

    @Override
    public String toString() {
        return charSequence.toString();
    }

    @Override
    public CharacterIterator iterator(CharacterIterator target) {
        return
            target instanceof CharSequenceIterator
            ? ((CharSequenceIterator)target).reset(charSequence)
            : new CharSequenceIterator(charSequence);
    }

    SimpleIterableCharSequence(CharSequence charSequence) {
        this.charSequence = charSequence;
    }
}
