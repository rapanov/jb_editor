/**
 * Author: Roman Panov
 * Date:   11/17/12
 */

package org.roman.util.text;

import java.util.NoSuchElementException;

public abstract class SingleCharSequence implements IterableCharSequence {
/*

    private char ch;

    SingleCharSequence(char ch) {
        this.ch = ch;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof SingleCharSequence && ch == ((SingleCharSequence)o).ch);
    }

    @Override
    public int hashCode() {
        return ch;
    }

    @Override
    public char charAt(int index) {
        if (index != 0) {
            throw new StringIndexOutOfBoundsException(index);
        }

        return ch;
    }

    @Override
    public int length() {
        return 1;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        if (start < 0) {
            throw new StringIndexOutOfBoundsException(start);
        }

        if (end > 1) {
            throw new StringIndexOutOfBoundsException(end);
        }

        final int len = end - start;

        if (len < 0) {
            throw new StringIndexOutOfBoundsException(len);
        }

        return (len == 0 ? TextUtil.EMPTY_CHAR_SEQUENCE : this);
    }

    @Override
    public String toString() {
        return new String(new char[] {ch});
    }

    @Override
    public CharIterator iterator() {
        return new Iterator(ch);
    }

    @Override
    public CharIterator iterator(int index) {
        if (index != 0) {
            throw new StringIndexOutOfBoundsException(index);
        }

        return new Iterator(ch);
    }

    public char getChar() {
        return ch;
    }

    public void setChar(char ch) {
        this.ch = ch;
    }

    private static final class Iterator implements CharIterator {
        private int ch;

        @Override
        public boolean hasNext() {
            return (ch >= 0);
        }

        @Override
        public Character next() {
            if (ch >= 0) {
                final char result = (char)ch;
                ch = -1;
                return result;
            }

            throw new NoSuchElementException();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @Override
        public int nextChar() {
            final int result = ch;
            ch = -1;
            return result;
        }

        private Iterator(char ch) {
            this.ch = ch;
        }
    }
*/
}
