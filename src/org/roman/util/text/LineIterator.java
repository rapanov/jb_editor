/**
 * Author: Roman Panov
 * Date:   11/17/12
 */

package org.roman.util.text;

import java.text.CharacterIterator;
import java.util.NoSuchElementException;
import org.roman.util.Segment;
import org.roman.util.SegmentIterator;

public final class LineIterator implements SegmentIterator {
    private CharacterIterator charIterator;
    private int maxChars;
    private char currChar;
    private int charsConsumed;
    private boolean isDone;

    public LineIterator() {
        this(new EmptyCharIterator());
    }

    public LineIterator(CharacterIterator charIterator) {
        reset(charIterator);
    }

    public LineIterator(CharacterIterator charIterator, int maxChars) {
        reset(charIterator, maxChars);
    }

    @Override
    public boolean hasNext() {
        return !isDone;
    }

    @Override
    public Segment next() {
        final Segment result = new Segment();

        if (next(result)) {
            return result;
        }

        throw new NoSuchElementException();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean next(Segment target) {
        if (isDone) {
            return false;
        }

        final int offset = charIterator.getIndex();

        // Try to locate where the next line starts.
        for (;;) {
            switch (currChar) {
                case CharacterIterator.DONE:
                    target.offset = offset;
                    target.length = charIterator.getIndex() - offset;
                    isDone = true;
                    return true;

                case Characters.CARRIAGE_RETURN:
                    target.offset = offset;
                    target.length = charIterator.getIndex() - offset;
                    currChar = nextChar();

                    if (currChar == Characters.LINE_FEED) {
                        // This line ends with CR+LF.
                        currChar = nextChar();
                    }

                    return true;

                case Characters.LINE_FEED:
                    target.offset = offset;
                    target.length = charIterator.getIndex() - offset;
                    currChar = nextChar();

                    if (currChar == Characters.CARRIAGE_RETURN) {
                        // This line ends with LF+CR.
                        currChar = nextChar();
                    }

                    return true;

                default:
                    // Just ordinary character.
                    currChar = nextChar();
            }
        }
    }

    private char nextChar() {
        if (charsConsumed < maxChars) {
            ++charsConsumed;
            return charIterator.next();
        }

        return CharacterIterator.DONE;
    }

    public LineIterator reset(CharacterIterator charIterator) {
        return reset(charIterator, Integer.MAX_VALUE);
    }

    public LineIterator reset(CharacterIterator charIterator, int maxChars) {
        this.charIterator = charIterator;
        this.maxChars = maxChars;
        currChar = maxChars > 0 ? charIterator.current() : CharacterIterator.DONE;
        charsConsumed = 1;
        isDone = false;
        return this;
    }
}