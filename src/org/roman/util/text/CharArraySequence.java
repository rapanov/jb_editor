/**
 * Author: Roman Panov
 * Date:   12/5/12
 */

package org.roman.util.text;


import java.text.CharacterIterator;
import org.roman.util.Arrays;

final class CharArraySequence implements IterableCharSequence {

    private final char[] array;
    private final int offset;
    private final int length;

    @Override
    public char charAt(int index) {
        if (index < 0 || index >= length) {
            throw new StringIndexOutOfBoundsException(index);
        }

        return array[offset + index];
    }

    @Override
    public int length() {
        return length;
    }

    @Override
    public IterableCharSequence subSequence(int start, int end) {
        if (start < 0) {
            throw new StringIndexOutOfBoundsException(start);
        }

        if (end > length) {
            throw new StringIndexOutOfBoundsException(end);
        }

        final int subSequenceLength = end - start;

        if (subSequenceLength < 0) {
            throw new StringIndexOutOfBoundsException(subSequenceLength);
        }

        return ((start == 0 && end == length) ? this : new CharArraySequence(array, start, length));
    }

    @Override
    public String toString() {
        return new String(array, offset, length);
    }

    @Override
    public CharacterIterator iterator(CharacterIterator target) {
        return target instanceof CharArrayIterator ? ((CharArrayIterator)target).reset(array,
                                                                                       offset,
                                                                                       length)
                                                   : new CharArrayIterator(array, offset, length);
    }

    CharArraySequence(char[] array) {
        this(array, 0, array.length);
    }

    CharArraySequence(char[] array, int offset, int length) {
        Arrays.validateArraySegment(array.length, offset, length);
        this.array = array;
        this.offset = offset;
        this.length = length;
    }
}
