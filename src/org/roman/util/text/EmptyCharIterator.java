/**
 * Author: Roman Panov
 * Date:   2/10/13
 */

package org.roman.util.text;

import java.text.CharacterIterator;

public final class EmptyCharIterator implements CharacterIterator {
    private int index;

    public EmptyCharIterator() {
        reset();
    }

    @Override
    public Object clone() {
        return new EmptyCharIterator(index);
    }

    @Override
    public char current() {
        return DONE;
    }

    @Override
    public char first() {
        index = 0;
        return DONE;
    }

    @Override
    public int getBeginIndex() {
        return 0;
    }

    @Override
    public int getEndIndex() {
        return 0;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public char last() {
        index = 0;
        return DONE;
    }

    @Override
    public char next() {
        ++index;
        return DONE;
    }

    @Override
    public char previous() {
        --index;
        return DONE;
    }

    @Override
    public char setIndex(int index) {
        this.index = index;
        return DONE;
    }

    public EmptyCharIterator reset() {
        index = 0;
        return this;
    }

    private EmptyCharIterator(int index) {
        this.index = index;
    }
}
