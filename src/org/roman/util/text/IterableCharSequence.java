/**
 * Author: Roman Panov
 * Date:   12/1/12
 */

package org.roman.util.text;

import java.text.CharacterIterator;

public interface IterableCharSequence extends CharSequence {
    CharacterIterator iterator(CharacterIterator target);

    @Override
    IterableCharSequence subSequence(int start, int end);
}
