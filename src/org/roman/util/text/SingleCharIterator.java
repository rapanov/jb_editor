/**
 * Author: Roman Panov
 * Date:   2/10/13
 */

package org.roman.util.text;

import java.text.CharacterIterator;

public final class SingleCharIterator implements CharacterIterator {
    private char ch;
    private int index;

    public SingleCharIterator(char ch) {
        reset(ch);
    }

    @Override
    public Object clone() {
        return new SingleCharIterator(ch, index);
    }

    @Override
    public char current() {
        return index == 0 ? ch : DONE;
    }

    @Override
    public char first() {
        index = 0;
        return ch;
    }

    @Override
    public int getBeginIndex() {
        return 0;
    }

    @Override
    public int getEndIndex() {
        return 1;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public char last() {
        index = 0;
        return ch;
    }

    @Override
    public char next() {
        ++index;
        return index == 0 ? ch : DONE;
    }

    @Override
    public char previous() {
        --index;
        return index == 0 ? ch : DONE;
    }

    @Override
    public char setIndex(int index) {
        this.index = index;
        return index == 0 ? ch : DONE;
    }

    public SingleCharIterator reset(char ch) {
        this.ch = ch;
        index = 0;
        return this;
    }

    private SingleCharIterator(char ch, int index) {
        this.ch = ch;
        this.index = index;
    }
}
