/**
 * Author: Roman Panov
 * Date:   12/1/12
 */

package org.roman.util.text;

import java.text.CharacterIterator;
import org.roman.util.Arrays;

public final class CharArrayIterator implements CharacterIterator {
    private char[] array;
    private int beginIdx;
    private int endIdx;
    private int currIdx;

    public CharArrayIterator(char[] array) {
        reset(array);
    }

    public CharArrayIterator(char[] array, int offset, int length) {
        reset(array, offset, length);
    }

    @Override
    public Object clone() {
        return new CharArrayIterator(array, beginIdx, endIdx, currIdx);
    }

    @Override
    public char current() {
        return (currIdx < endIdx ? array[currIdx] : DONE);
    }

    @Override
    public char first() {
        currIdx = beginIdx;
        return current();
    }

    @Override
    public int getBeginIndex() {
        return beginIdx;
    }

    @Override
    public int getEndIndex() {
        return endIdx;
    }

    @Override
    public int getIndex() {
        return currIdx;
    }

    @Override
    public char last() {
        currIdx = endIdx;

        if (endIdx > beginIdx) {
            --currIdx;
            return array[currIdx];
        }

        return DONE;
    }

    @Override
    public char next() {
        ++currIdx;

        if (currIdx < endIdx) {
            return array[currIdx];
        }

        currIdx = endIdx;
        return DONE;
    }

    @Override
    public char previous() {
        if (currIdx > beginIdx) {
            --currIdx;
            return array[currIdx];
        }

        return DONE;
    }

    @Override
    public char setIndex(int index) {
        if (index < beginIdx || index > endIdx) {
            throw new IllegalArgumentException();
        }

        currIdx = index;
        return current();
    }

    public CharArrayIterator reset(char[] array) {
        return reset(array, 0, array.length);
    }

    public CharArrayIterator reset(char[] array, int offset, int length) {
        Arrays.validateArraySegment(array.length, offset, length);
        this.array = array;
        beginIdx = offset;
        endIdx = offset + length;
        currIdx = beginIdx;
        return this;
    }

    private CharArrayIterator(char[] array, int beginIdx, int endIdx, int currIdx) {
        this.array = array;
        this.beginIdx = beginIdx;
        this.endIdx = endIdx;
        this.currIdx = currIdx;
    }
}
