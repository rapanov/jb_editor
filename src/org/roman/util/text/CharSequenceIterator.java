/**
 * Author: Roman Panov
 * Date:   12/1/12
 */

package org.roman.util.text;

import java.text.CharacterIterator;
import org.roman.util.Arrays;

public final class CharSequenceIterator implements CharacterIterator {
    private CharSequence charSequence;
    private int beginIdx;
    private int endIdx;
    private int currIdx;

    public CharSequenceIterator(CharSequence charSequence) {
        reset(charSequence);
    }

    public CharSequenceIterator(CharSequence charSequence, int offset, int length) {
        reset(charSequence, offset, length);
    }

    @Override
    public Object clone() {
        return new CharSequenceIterator(charSequence, beginIdx, endIdx, currIdx);
    }

    @Override
    public char current() {
        return (currIdx < endIdx ? charSequence.charAt(currIdx) : DONE);
    }

    @Override
    public char first() {
        currIdx = beginIdx;
        return current();
    }

    @Override
    public int getBeginIndex() {
        return beginIdx;
    }

    @Override
    public int getEndIndex() {
        return endIdx;
    }

    @Override
    public int getIndex() {
        return currIdx;
    }

    @Override
    public char last() {
        currIdx = endIdx;

        if (endIdx > beginIdx) {
            --currIdx;
            return charSequence.charAt(currIdx);
        }

        return DONE;
    }

    @Override
    public char next() {
        ++currIdx;

        if (currIdx < endIdx) {
            return charSequence.charAt(currIdx);
        }

        currIdx = endIdx;
        return DONE;
    }

    @Override
    public char previous() {
        if (currIdx > beginIdx) {
            --currIdx;
            return charSequence.charAt(currIdx);
        }

        return DONE;
    }

    @Override
    public char setIndex(int index) {
        if (index < beginIdx || index > endIdx) {
            throw new IllegalArgumentException();
        }

        currIdx = index;
        return current();
    }

    public CharSequenceIterator reset(CharSequence charSequence) {
        return reset(charSequence, 0, charSequence.length());
    }

    public CharSequenceIterator reset(CharSequence charSequence, int offset, int length) {
        Arrays.validateArraySegment(charSequence.length(), offset, length);
        this.charSequence = charSequence;
        beginIdx = offset;
        endIdx = offset + length;
        currIdx = beginIdx;
        return this;
    }

    private CharSequenceIterator(CharSequence charSequence, int beginIdx, int endIdx, int currIdx) {
        this.charSequence = charSequence;
        this.beginIdx = beginIdx;
        this.endIdx = endIdx;
        this.currIdx = currIdx;
    }
}
