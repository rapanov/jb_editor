/**
 * Author: Roman Panov
 * Date:   2/17/13
 */

package org.roman.util;

public interface Mapper<K, V> {
    V map(K key);
}
