/**
 * Author: Roman Panov
 * Date:   1/29/13
 */

package org.roman.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Stack;

public final class Trees {

    static final int SIDE_UNKNOWN = 0;
    static final int SIDE_LEFT    = 1;
    static final int SIDE_RIGHT   = 2;

    private static final String NODE_PREFIX = "+-";

    public static int treeDepth(BinaryTree tree) {
        return treeDepth(tree.root());
    }

    public static int treeDepth(BinaryTree.Node root) {
        return subtreeDepth(root, 0);
    }

    public static void printTree(BinaryTree tree,
                                 Writer writer)
        throws IOException
    {
        printTree(tree.root(), writer);
    }

    public static void printTree(BinaryTree tree,
                                 OutputStream outputStream)
        throws IOException
    {
        printTree(tree, outputStream, Charset.defaultCharset());
    }

    public static void printTree(BinaryTree tree,
                                 OutputStream outputStream,
                                 Charset charset)
        throws IOException
    {
        printTree(tree, new OutputStreamWriter(outputStream, charset));
    }

    public static void printTree(BinaryTree.Node root,
                                 Writer writer)
        throws IOException
    {
        final Stack<Integer> sideStack = new Stack<Integer>();
        printTree(root, writer, 0, sideStack);
        writer.flush();
    }

    public static void printTree(BinaryTree.Node root,
                                 OutputStream outputStream)
        throws IOException
    {
        printTree(root, outputStream, Charset.defaultCharset());
    }

    public static void printTree(BinaryTree.Node root,
                                 OutputStream outputStream,
                                 Charset charset)
        throws IOException
    {
        printTree(root, new OutputStreamWriter(outputStream, charset));
    }

    private static int subtreeDepth(BinaryTree.Node node, int depth) {
        if (node == null) {
            return depth;
        }
        final int leftSubtreeDepth = subtreeDepth(node.leftChild(), depth + 1);
        final int rightSubtreeDepth = subtreeDepth(node.rightChild(), depth + 1);
        return Math.max(leftSubtreeDepth, rightSubtreeDepth);
    }

    private static void printTree(BinaryTree.Node root,
                                  Writer writer,
                                  int depth,
                                  Stack<Integer> sideStack)
        throws IOException
    {
        if (root != null)
        {
            final BinaryTree.Node rightSubtree = root.rightChild();
            final BinaryTree.Node leftSubtree = root.leftChild();
            final int depthPlusOne = depth + 1;

            // Print right subtree.
            sideStack.push(SIDE_RIGHT);
            printTree(rightSubtree, writer, depthPlusOne, sideStack);

            // Print root.
            final String nodeStr = root.toString();
            final int prefLen = NODE_PREFIX.length();
            final int spaceLen = prefLen * depth;
            final int longSpaceLen = spaceLen + prefLen;
            final StringBuilder nodeStrBld = new StringBuilder(longSpaceLen + nodeStr.length() + 2);
            StringBuilder rightShoulderStrBld = null;
            StringBuilder leftShoulderStrBld = null;

            for (int idx = 0; idx < spaceLen; ++idx)
            {
                nodeStrBld.append(' ');
            }

            nodeStrBld.append(NODE_PREFIX).append(nodeStr).append('\n');

            if (rightSubtree != null)
            {
                rightShoulderStrBld = new StringBuilder(longSpaceLen + 3);
                for (int idx = 0; idx < longSpaceLen; ++idx)
                {
                    rightShoulderStrBld.append(' ');
                }
                rightShoulderStrBld.append('|').append('\n');
            }

            if (leftSubtree != null)
            {
                leftShoulderStrBld = new StringBuilder(longSpaceLen + 3);
                for (int idx = 0; idx < longSpaceLen; ++idx)
                {
                    leftShoulderStrBld.append(' ');
                }
                leftShoulderStrBld.append('|').append('\n');
            }

            int prevSide = SIDE_UNKNOWN;
            int charIdx = 0;

            for (int side : sideStack)
            {
                if (prevSide != SIDE_UNKNOWN && side != prevSide)
                {
                    nodeStrBld.setCharAt(charIdx, '|');

                    if (leftShoulderStrBld != null)
                    {
                        leftShoulderStrBld.setCharAt(charIdx, '|');
                    }

                    if (rightShoulderStrBld != null)
                    {
                        rightShoulderStrBld.setCharAt(charIdx, '|');
                    }
                }

                prevSide = side;
                charIdx += prefLen;
            }

            if (rightShoulderStrBld != null)
            {
                if (!sideStack.isEmpty() && sideStack.peek() == SIDE_LEFT)
                {
                    rightShoulderStrBld.setCharAt(charIdx, '|');
                }

                writer.write(rightShoulderStrBld.toString());
            }

            writer.write(nodeStrBld.toString());

            if (leftShoulderStrBld != null)
            {
                if (!sideStack.isEmpty() && sideStack.peek() == SIDE_RIGHT)
                {
                    leftShoulderStrBld.setCharAt(charIdx, '|');
                }

                writer.write(leftShoulderStrBld.toString());
            }

            // Print left subtree.
            sideStack.push(SIDE_LEFT);
            printTree(leftSubtree, writer, depthPlusOne, sideStack);
        }

        if (!sideStack.isEmpty())
        {
            sideStack.pop();
        }
    }
}
